<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcademicDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('academic_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('app_no');
            $table->string('vocational');
            $table->string('syllabus')->nullable();
            $table->string('board_name');
            $table->integer('passing_year');
            $table->integer('fm1');
            $table->integer('fm2');
            $table->integer('fm3');
            $table->integer('fm4');
            $table->integer('fm5')->nullable();
            $table->integer('mrk1');
            $table->integer('mrk2');
            $table->integer('mrk3');
            $table->integer('mrk4');
            $table->integer('mrk5')->nullable();
            $table->integer('total_fullmarks');
            $table->integer('total_mark');
            $table->float('percentage');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('academic_details');
    }
}
