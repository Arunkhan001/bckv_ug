<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionTabTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_tab', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('app_no');
            $table->text('t_id')->nullable();
            $table->text('t_type')->nullable();
            $table->float('t_amt');
            $table->float('received_amt')->nullable();
            $table->text('t_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_tab');
    }
}
