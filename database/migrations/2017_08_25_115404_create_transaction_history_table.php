<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('app_no');
            $table->integer('invoice_no');
            $table->text('t_id')->nullable();
            $table->text('t_type');
            $table->text('t_amt')->nullable();
            $table->text('t_status')->nullable();
            $table->text('auth_status')->nullable();
            $table->text('bank_ref_no')->nullable();
            $table->text('bank_id')->nullable();
            $table->text('bank_marchant_id')->nullable();
            $table->text('error_descrip')->nullable();
            $table->text('item_code')->nullable();
            $table->text('tot_msg')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_history');
    }
}
