<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_details', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('app_no');
            $table->string('father_name');
            $table->string('mother_name');
            $table->string('guardian_name');
            $table->string('guardian_relationship');
            $table->bigInteger('guardian_mobile_number');
            $table->string('gender');
            $table->string('category');
            $table->string('religion');
            $table->string('blood_group')->nullable();
            $table->date('date_of_birth');
            $table->string('domicile');
            $table->string('residential_area_type');
            $table->string('nationality');
            $table->string('street_no');
            $table->string('village');
            $table->string('police_station');
            $table->string('post_office');
            $table->string('pincode');
            $table->string('district');
            $table->string('correspondence_street_no');
            $table->string('correspondence_village');
            $table->string('correspondence_police_station');
            $table->string('correspondence_post_office');
            $table->string('correspondence_pincode');
            $table->string('correspondence_district');
            $table->string('aadhaar');
            $table->string('student_aadhaar_number')->nullable();
            $table->string('guardian_aadhaar_number')->nullable();
            $table->string('guardian_aadhaar_name')->nullable();
            $table->string('guardian_relation')->nullable();
            $table->integer('img_flag')->default(0);
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_details');
    }
}
