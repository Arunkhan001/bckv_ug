<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppchargeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_charges', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category');
            $table->integer('amount');
            $table->text('in_word');
            $table->integer('total_amount');
            $table->text('total_in_word');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_charges');
    }
}
