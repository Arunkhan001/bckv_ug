<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:title" content="Online Admission"/>
        <meta property="og:site_name" content="Bidhan Chandra Krishi Viswavidyalaya Online Admission">
        <meta property="og:type" content="summary"/>
        <meta property="og:image" content="{{ url('images/bckv-logo.png') }}" />
        <meta property="og:url" content="{{ url('/') }}"/>
        <meta property="og:description" content="Bidhan Chandra Krishi Viswavidyalaya Online Admission for B.Sc.(Hons) Agril. & B.Sc(Hons) Hort. Courses for Mohanpur, Burdwan & Chanta(Susunia, Bankura) Campus for Academic session {{ env('ADMISSION_SESSION') }}. " />

        <meta name="keywords" content="online, BCKV, bckv,admission online,ug admission,agriculture, admission, bidhan chandra,krishi viswavidyalaya,agriculture admission, B.Sc.(Hons), mohanpur,Hort admission,Hort, mohanpur campus, burdwan, online admission,bidhan chandra krishi viswavidyalaya, Dos,dos, dos product, dos infotech product ">
        <meta name="robots" content="index, follow">
        <meta name="googlebot" content="index, follow">
        <meta name="msnbot" content="index, follow">

        <meta name="description" content="Bidhan Chandra Krishi Viswavidyalaya Online Admission for B.Sc.(Hons) Agril. & B.Sc(Hons) Hort. Courses for Mohanpur, Burdwan & Chanta(Susunia, Bankura) Campus for Academic session {{ env('ADMISSION_SESSION') }}">
        <meta name="author" content="Dos Infotech">
        <meta name="copyright" content="Dos Infotech">
        <meta name="document-distribution" content="Global">

        <meta name="theme-color" content="#d9edf7">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <link rel="icon" sizes="32x32" href="{{ url('images/bckv-logo.png') }}" type="image/png">


        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/default.css') }}" rel="stylesheet">
        <style>
            
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Open Sans' !important;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                right: 10px;
                top: 18px;
            }


            .title {
                font-size: 84px;
            }
            .link > a {
                letter-spacing: .1rem;
                text-transform: uppercase;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            @media only screen and (min-width:480px){
                .top-right {
                    position: absolute;
                }
            }
           
        </style>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116983977-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-116983977-1');
            function inIframe () {
                try {
                    return window.self !== window.top;
                } catch (e) {
                    return true;
                }
            }
        </script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    {{-- <a href="javascript:void(0);" class=" logo"> <img src="{{ asset('images/bckv-logo.png') }}" title="Bidhan Chandra Krishi Viswavidyalaya" /> </a> --}}
                    <div class="title text-center">
                        <h2>Bidhan Chandra Krishi Viswavidyalaya</h2>
                        <h5 style="text-transform: uppercase; text-align: center; font-size: 14px;">P.O. Krishiviswavidyalaya, MOHANPUR, DIST : NADIA,<br/>
                            West Bengal, PIN : 741252</h5>
                        <h5 style="text-align: center;">Advertisement No - {{ env('ADMISSION_ADVT_NO') }}</h5>
                    </div>
                </div>
            </div>
            <div class="row text-center" style="margin-top:5%">
                <svg height="100" width="100">
                    <polygon points="50,25 17,80 82,80" stroke-linejoin="round" style="fill:none;stroke:#ff8a00;stroke-width:8" />
                    <text x="42" y="74" fill="#ff8a00" font-family="sans-serif" font-weight="900" font-size="42px">!</text>
                </svg>
            </div>
            <div class="row text-center">
                <div class="col-md-12">
                    <div class="main-icon text-warning"><span class="uxicon uxicon-alert"></span></div>
                    <h1>Oops! That page couldn't be found.</h1>
                </div>
            </div>
            <div class="row text-center" style="margin-bottom: 10%">
                <div class="col-md-6 col-md-push-3">
                    <p class="lead">If you think what you're looking for should be here, please contact to the College Authority.</p>
                    <a class="btn btn-success" href="{{ url()->previous() }}">Go Back</a>
                </div>
            </div>
        </div>
        <div class="container-fluid" style="position: fixed;bottom: 0px; width: 100%;">
            <div class="row text-center" style="background: #C1C1C1;">
                <div class="col-md-12">
                    <p style="margin-top: 10px;
               font-size: 12px;
               font-weight: bold;
               margin-left: 10px;">Copyright <a href="http://www.bckv.edu.in/" target="_blank">BCKV</a> 2018-20. Designed, Developed & Maintained By <a href="http://dos-infotech.com/" target="_blank">DOS Infotech (House Of IT)</a></p>

                    <p  style="margin-top: 10px;margin-bottom:0px;
               font-size: 12px;
               font-weight: bold;
               margin-left: 10px;">For any assistant contact us at <span style="font-size: 14px; color: #00f;"> {{ env('CONTACT_LAND','000') }}</span> or Mail us at <span style="font-size: 14px; color: #00f;"><a href="mailto:bckvug.admission@gmail.com" target="_top">bckvug.admission@gmail.com</a></span> </p>
                    <!--p class="pull-left" style="
                       font-size: 12px;
                       font-weight: bold;
                       margin-left: 10px;">For any technical assistance please contact at <span style="font-size: 14px; color: #00f;">08013935142</span>
                        [ 11 AM - 8 PM ]
                    </p-->
                </div>
            </div>
        </div>
    </body>
    <script>
        if(inIframe ()) {
            document.body.innerHTML = '<h1>Please Open a new tab and paste this url : <a href="https://bckvadmn.in">https://bckvadmn.in</a> </h1>';
        }
    </script>
</html>
