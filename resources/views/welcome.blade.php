<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:title" content="BCKV Online Admission"/>
        <meta property="og:site_name" content="Bidhan Chandra Krishi Viswavidyalaya UG Online Admission">
        <meta property="og:type" content="summary"/>
        <meta property="og:image" content="{{ url('images/bckv-logo.png') }}" />
        <meta property="og:url" content="{{ url('/') }}"/>
        <meta property="og:description" content="Bidhan Chandra Krishi Viswavidyalaya Online Admission for B.Sc.(Hons) Agril. & B.Sc(Hons) Hort. Courses for Mohanpur, Burdwan & Chanta(Susunia, Bankura) Campus for Academic session {{ env('ADMISSION_SESSION') }}. " />
        <meta name="keywords" content="online, BCKV, bckv,admission online,ug admission,agriculture, admission, bidhan chandra,krishi viswavidyalaya,agriculture admission, B.Sc.(Hons), mohanpur,Hort admission,Hort, mohanpur campus, burdwan, online admission,bidhan chandra krishi viswavidyalaya, Dos,dos, dos product, dos infotech product ">
        <meta name="robots" content="index, follow">
        <meta name="googlebot" content="index, follow">
        <meta name="msnbot" content="index, follow">

        <meta name="description" content="Bidhan Chandra Krishi Viswavidyalaya Online Admission for B.Sc.(Hons) Agril. & B.Sc(Hons) Hort. Courses for Mohanpur, Burdwan & Chanta(Susunia, Bankura) Campus for Academic session {{ env('ADMISSION_SESSION') }}">
        <meta name="author" content="Dos Infotech">
        <meta name="copyright" content="Dos Infotech">
        <meta name="document-distribution" content="Global">

        <meta name="theme-color" content="#d9edf7">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <link rel="icon" sizes="32x32" href="{{ url('images/bckv-logo.png') }}" type="image/png">


        <!-- Fonts -->
        {{-- <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css"> --}}

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/default.css') }}" rel="stylesheet">
        <style>
            
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Open Sans' , monospace !important;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                right: 10px;
                top: 18px;
            }


            .title {
                font-size: 84px;
            }
            .link > a {
                letter-spacing: .1rem;
                text-transform: uppercase;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            @media only screen and (min-width:480px){
                .top-right {
                    position: absolute;
                }
            }
        </style>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116983977-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-116983977-1');
            function inIframe () {
				try {
					return window.self !== window.top;
				} catch (e) {
					return true;
				}
			}
        </script>
    </head>
    <body>
		<div class="container" style="display:none">
			<div class="row alert alert-success">
				<div class="col-sm-6"><a href="{{ url('/pgadmission/') }}" class="btn btn-primary" style="float:right" > PG Admission 2018</a></div>
				<div class="col-sm-6"><a href="{{ url('/phdadmission/') }}" class="btn btn-primary"> PH.D. Admission 2018</a></div>
			</div>
		</div>
        <div class="container">
            <div class="row">
                <div class="col-sm-10">
                    <a href="javascript:void(0);" class=" logo"> <img src="{{ asset('images/bckv-logo.png') }}" title="Bidhan Chandra Krishi Viswavidyalaya" /> </a>
                    <div class="title text-center">
                        <h2>Bidhan Chandra Krishi Viswavidyalaya</h2>
                        <h5 style="text-transform: uppercase; t5ext-align: center; font-size: 14px;">P.O. Krishiviswavidyalaya, MOHANPUR, DIST : NADIA,<br/>
                            West Bengal, PIN : 741252</h5>
                        <h5 style="text-align: center;">Notification No - {{ env('ADMISSION_ADVT_NO') }}</h5>
                    </div>
                </div>
                <div class="col-sm-2">
                    @if (Route::has('login'))
                        <div class="top-right link text-center">
                            @auth
                                <a href="{{ url('/home') }}">Home</a>
                            @else
                                @if(\Carbon\Carbon::parse(env('ADMISSION_START_DATE'))->isPast())
                                <a href="{{ url('student/login') }}" class="btn btn-info">STUDENT LOGIN SECTION</a>
                                {{--<a href="{{ route('register') }}">Register</a>--}}
                                @endif
                            @endauth
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 bg-info" style="padding-bottom: 20px">
                    <h2 class="section-title text-center">UG Online Application / Admission {{ env('ADMISSION_SESSION') }}</h2>
                    <ol style="text-align: justify;">
                                <li style="padding: 2px;font-weight: bold;">Download the Notification No 1 for Under Graduate(UG) admission {{ env('ADMISSION_SESSION') }} <a href="{{ asset('lib/200804_Adm_UG_AG-1(Part-IV)_L-146.pdf') }}">Download</a></li>
                                <li style="padding: 2px;font-weight: bold;">Download the Notification No 2 for Under Graduate(UG) admission {{ env('ADMISSION_SESSION') }} <a href="{{ asset('lib/200812_Adm_UG_AG-1(Part-IV)_L-160.pdf') }}">Download</a></li>
                                <li style="padding: 2px;font-weight: bold;">Application Procedure: 4 Simple Steps to be followed to apply online
                                    <ol type="I" class="form-fillup-guidline " style="margin-top: 6px;">
                                        <li style="padding: 5px;">Step 1 : Fill in the Online Application Form and note down system generated Application No.The candidate should provide all the details while filling the Online Application Form and also required to choose PASSWORD. After successful registration, Application No will be generated and it will be used to complete the remaining Steps of the Application Form and also required for all future correspondence. For subsequent logins, candidate will be able to login directly with his/her respective email-id and chosen Password.</li>
                                        <li style="padding: 5px;">Step 2 : Upload Scanned Images of <span style='color:red;'>Color Photograph</span>, Signature, Class 10 <sup>th</sup> Standard Marksheet (Both Side) separately, Class 12<sup>th</sup> Standard Marksheet (Both Side) separately, Birth Certificate / Certificate of Secondary Examination, Aadhaar Card :
                                            The scanned images should be in jpg/jpeg format only.
                                            <span style='color:red;'>
                                    Size of the Photo can be minimum 10 KB to maximum 150 KB.
                                    Size of the Signature can be minimum 5 KB to maximum 70 KB.
                                    Size of the following documents will be minimum 50 KB and maximum 300 KB.</span>
                                     <ol type='a' style="margin-left: 15px;margin-top: 5px;">
                                                <li style="padding: 3px;">Class 10 <sup>th</sup> Standard Marksheet - each side</li>
                                                <li style="padding: 3px;">Class 12 <sup>th</sup> Standard Marksheet - each side</li>
                                                <li style="padding: 3px;">Birth Certificate / Certificate of Secondary Examination</li>
                                                 <li style="padding: 3px;">Aadhaar Card</li>
                                            </ol>
                                    Dimension of Photograph image should be closest to [ 3.5CM X 4.5CM ] (Width X Height).
                                    Dimension of Signature image should be closest [ 3.5CM X 1.5CM ](Width X Height ).
                                    Dimension of Marksheet image and Birth Certificate / Certificate of Secondary Examination image should be closest to 500 px(width) x 800 px(height).
                                        </li>
                                        <li style="padding: 5px;">Step 3: Pay Application Fee by online mode (Debit Card/Credit Card/Net Banking):
                                            <ol type='a' style="margin-left: 15px;margin-top: 5px;">
                                               {{--  <li style="padding: 3px;">For Payment by offline mode : As soon as he/she selects the offline mode and click on payment option, an e-Challan will be generated containing specific details of the candidate along with amount to be paid. The candidate has to take a printout of the same and take it to the nearest branch of the United Bank of India(UBI) bank for making payment.It will take <u>72</u> hrs. for confirmation of the application fee. After the confirmation of deposited fee from Bank, the candidate will be able to take of print the Application Form.</li> --}}
                                                <li style="padding: 3px;">For Payment by online mode : The candidate has to select Online option to pay the application fee and follow the online instruction to complete the payment of fee. After successful payment, candidate will be able to print the Application Form.</li>
                                            </ol>
                                        </li>
                                        <li style="padding: 5px;">Step 4: Download the Filled Up Application Form:
                                            The Candidate should take print of Application Form and keep it for future reference.</li>
                                    </ol>
                                </li>
                                <li style="padding: 2px;font-weight: bold;">Application Steps flowchart :<br><br>
                                    <pre style="background-color:transparent;border:none;padding:0;font-size: 14px;">
    +-------------------+         +----------------------+         +----------------------+
    | START FORM FILLUP | +-----> | REGISTRATION / LOGIN | +-----> | PERSONAL INFORMATION |  +--------------+
    +-------------------+         +----------------------+         +----------------------+                 |
                                                                                                            |
    +-----------------+      +---------+         +-------------------+          +----------------------+    |
    |PRINT APPLICATION| <--+ | PAYMENT |  <---+  | DOCUMENTS UPLOAD  | <------+ | ACADEMIC INFORMATION | <--+
    +-----------------+      +---------+         +-------------------+          +----------------------+</pre>
                                </li>
                            </ol>
                    <div>
                        <ul  style="list-style: none;">
                            <li><span style="color:red;margin-left: -10px;font-weight: bold;">NOTE : </span>
                                <ol>
                                    <li style="padding: 5px;font-weight: bold;margin-left: 10px;"> Please use a <span style="color:#f00;">VALID email-id</span> for registration.</li>
                                    <li style="padding: 5px;font-weight: bold;margin-left: 10px;">We recommend you to use <b style="color: blue;"><a href="https://www.google.com/chrome/browser/features.html?brand=CHBD&gclid=Cj0KEQjwi7vIBRDpo9W8y7Ct6ZcBEiQA1CwV2I_szV9wdE6Hls1ZgF7qfYkreqSS2TvH-Hg0AU9eAkIaAoJm8P8HAQ&dclid=CN7eq-LT39MCFYupaAodIVEEKQ">Google Chrome</a></b> WEB Browser.</li>
                                    {{-- <li style="padding: 5px;font-weight: bold;margin-left: 10px;">Click <a href="followstep.php" target="_blank">here</a> to see STEP BY STEP PROCEDURE to fill the form.</li> --}}
                                    <li style="padding: 5px;font-weight: bold;margin-left: 10px;"><span style="color:#f00;">OPENING DATE OF ONLINE APPLICATION IS {{ env('ADMISSION_START_DATE') }}.</span></li>
                                    <li style="padding: 5px;font-weight: bold;margin-left: 10px;"><span style="color:#f00;">CLOSING DATE OF ONLINE APPLICATION IS {{ env('ADMISSION_END_DATE') }}.</span></li>
									<!-- <li style="padding: 5px;font-weight: bold;margin-left: 10px;"><span style="color:#f00;">LAST DATE OF ONLINE APPLICATION IS : Within 20 days from the date of publication of the result of the H.S.(10+2) Examination of West Bengal Council of Higher Secondary Education - 2018.</span></li>-->
                                   <li style="padding: 5px;font-weight: bold;margin-left: 10px;color:red;">LAST DATE OF ONLINE PAYMENT IS {{ env('PAYMENT_END_DATE') }}.</li>
                                   <li style="padding: 5px;font-weight: bold;margin-left: 10px;color:red;">OPENING DATE FOR CORRECTION AND MODIFICATION IS {{ env('RECTIFICATION_START_DATE') }}.</li>
                                   <li style="padding: 5px;font-weight: bold;margin-left: 10px;color:red;">CLOSING DATE FOR CORRECTION AND MODIFICATION IS {{ env('RECTIFICATION_END_DATE') }}.</li>
									<li style="padding: 5px;font-weight: bold;margin-left: 10px;">For any Technical Issues Mail us at  <span style="color:#f00;">bckvug.admission@gmail.com</span> </li> 
									<li style="padding: 5px;font-weight: bold;margin-left: 10px;">Technical Problem Help Line for online application :  {{ env('CONTACT_MOBILE') }}    <b style="color: #3097d1;">[ 11:00 a.m. to 8:00 p.m. ]</b></li>
                                    <li style="padding: 5px;font-weight: bold;margin-left: 10px;">Visit <a style="text-decoration: underline;" href="http://bckv.edu.in/">BCKV OFFICIAL WEB SITE</a> FOR PUBLISHING DATE OF MERIT LIST.</li>
                                    <li style="padding: 5px;font-weight: bold;margin-left: 10px;">
                                        It is advised that the entire Online Application process including Payment should be done through Desktop / Laptop Computer instead of Mobile Browser to avoid software compatibility problem
                                    </li>
                                </ol>
                            </li>
                        </ul>
                    </div>
                    <div class="text-center">
                        @if( \Carbon\Carbon::now()->between(\Carbon\Carbon::parse(env('ADMISSION_START_DATE')),\Carbon\Carbon::parse(env('ADMISSION_END_DATE'))) || \Carbon\Carbon::parse(env('ADMISSION_END_DATE'))->isToday())
                        <a href="{{ url('student/register') }}" class="btn btn-success">START FORM FILL-UP</a>
                        @elseif(\Carbon\Carbon::parse(env('ADMISSION_START_DATE'))->isFuture())
                        <a href="javascript:void(0)" class="btn btn-default" disabled>FORM FILL-UP START FROM {{ \Carbon\Carbon::parse(env('ADMISSION_START_DATE'))->format('d-M-Y h:i A')}}</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row" style="background: #C1C1C1;">
                <div class="col-md-12">
                    <p class="pull-right" style="margin-top: 10px;
               font-size: 12px;
               font-weight: bold;
               margin-left: 10px;">Copyright <a href="http://www.bckv.edu.in/" target="_blank">BCKV</a> 2020-21. Designed, Developed & Maintained By <a href="http://dos-infotech.com/" target="_blank">DOS Infotech (House Of IT)</a></p>

                    <p class="pull-left" style="margin-top: 10px;margin-bottom:0px;
               font-size: 12px;
               font-weight: bold;
               margin-left: 10px;">For any assistant contact us at <span style="font-size: 14px; color: #00f;">{{ env('CONTACT_MOBILE') }}</span> or Mail us at <span style="font-size: 14px; color: #00f;"><a href="mailto:bckvug.admission@gmail.com" target="_top">bckvug.admission@gmail.com</a></span> </p>
                    <!--p class="pull-left" style="
                       font-size: 12px;
                       font-weight: bold;
                       margin-left: 10px;">For any technical assistance please contact at <span style="font-size: 14px; color: #00f;">08013935142</span>
                        [ 11 AM - 8 PM ]
                    </p-->
                </div>
            </div>
        </div>
    </body>
</html>
