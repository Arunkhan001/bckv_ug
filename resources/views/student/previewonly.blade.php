@extends('student.layout.auth')

@section('content')
<div class="container">
    <div class="row">
        @if (session('alert'))
            <div class="alert alert-success">
                {{ session('alert') }}
            </div>
        @endif
        <p align="center" style="color: red"><b>** Please review your information carefully before submit application. This page is for view purpose only **</b></p>
        <div class="panel panel-default">
            <div id="heading" class="panel-heading">
                <h4>Personal Information Details<h4>
            </div>
            <div class="panel-body">
         		<div class="col-sm-2">
             		<img style="height:150px; width: 150px;" src="{{ url('student/image/'.$user->app_no.'/photo.jpg') }}">
             		<img style="height:50px; width: 150px;" src="{{ url('student/image/'.$user->app_no.'/signature.jpg') }}">
         		</div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <label class="col-sm-6">Candidate Full Name : </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12">{{$user->name}}</label>
                        </div>
                    </div>
                    @if($user->father_name)
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Father's Name : </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{$user->father_name}}</label>
                        </div>
                    </div>
                    @endif
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Mother's Name : </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{$user->mother_name}}</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Mobile Number : </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{$user->mobile_number}}</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Candidate's Email Id : </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{$user->email}}</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Guardian Name : </label>
                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{$user->guardian_name}}</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Guardian Mobile No : </label>
                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{$user->guardian_mobile_number}}</label>
                        </div>
                    </div>
                    @if($user->guardian_email)
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Guardian Email : </label>
                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{$user->guardian_email}}</label>
                        </div>
                    </div>
                    @endif
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Relation with Guardian : </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{$user->guardian_relationship}}</label>
                        </div>
                    </div>

                     <div class="form-group">
                        <label class="col-sm-6 control-label">Date of Birth : </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{ date('d/m/Y', strtotime($user->date_of_birth))}}</label>
                        </div>

                    </div>
                     @if($user->religion)
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Religion : </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{$user->religion}}</label>
                        </div>
                    </div>
                    @endif
                    @if($user->blood_group)
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Blood Group : </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{$user->blood_group}}</label>
                        </div>
                    </div>
                    @endif
                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Domicile : </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{$user->domicile}}</label>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Category : </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{$user->category}}</label>
                        </div>
                    </div>
                    @if($user->category == "SC" || $user->category == "ST" || $user->category == "OBC-A" || $user->category == "OBC-B")
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Caste Certificate Number : </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{$user->certificate_number}}</label>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Issuing Date : </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{ date('d/m/Y', strtotime($user->issuing_date))}}</label>
                        </div>
                    </div>
                    @endif

                    <div class="col-sm-12">
                            <label class="col-sm-6">Sport Quota : </label>

                            <div class="col-sm-6">
                                <label><b>{{$user->sport_quota}}</b></label>
                            </div>
                        </div>


                        @if($user->sport_quota == "Yes")
                        <div class="col-sm-12">
                            <label class="col-sm-6">Sport Certificate Number : </label>

                            <div class="col-sm-6">
                                <label>{{$user->sport_certificate_number}}</label>
                            </div>
                        </div>
                        
                        <div class="col-sm-12">
                            <label class="col-sm-6">Sport Certificate Issue Date : </label>

                            <div class="col-sm-6">
                                <label>{{ date('d/m/Y', strtotime($user->sport_issuing_date))}}</label>
                            </div>
                        </div>
                        @endif
                    
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Gender : </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{$user->gender}}</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Nationality : </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{$user->nationality}}</label>
                        </div>
                    </div>
                   
                    {{-- @if($user->aadhaar != NULL)
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Aadhaar : </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{$user->aadhaar}}</label>
                        </div>
                    </div>
                    @endif --}}

                    @if($user->student_aadhaar_number != NULL)
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Candidate Aadhaar No : </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{$user->student_aadhaar_number}}</label>
                        </div>
                    </div>
                    @endif

                   {{--  @if($user->guardian_aadhaar_number != NULL)
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Guardian Aadhaar No : </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{$user->guardian_aadhaar_number}}</label>
                        </div>
                    </div>
                    @endif  

                    @if($user->guardian_aadhaar_name != NULL)
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Guardian Name : </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{$user->guardian_aadhaar_name}}</label>
                        </div>
                    </div>
                    @endif  

                    @if($user->guardian_relation != NULL)
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Guardian Relation : </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{$user->guardian_relation}}</label>
                        </div>
                    </div>
                    @endif  --}}
                     @if($user->bank_account_details != NULL)
                    <div class="form-group">
                        <label class="col-sm-6 control-label" style="font-size: 13px;">Candidate S/B Account Details : </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{$user->bank_account_details}}</label>
                        </div>
                    </div>
                    @endif

                     @if($user->bank_ifsc_code != NULL)
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Bank IFS Code : </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{$user->bank_ifsc_code}}</label>
                        </div>
                    </div>
                    @endif                      
                </div>
        	</div>
    	</div>
    </div>
    <div class="row">
        <div class="panel panel-default">
            <div id="heading" class="panel-heading"><h4>Address Details<h4></div>
            <div class="panel-body">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Permanent Address : </label>

                        <div class="col-sm-12">
                            @if($user->father_name)
                                <label class="col-sm-12 control-label">C/O - {{$user->father_name}}</label>
                            @else
                                <label class="col-sm-12 control-label">C/O - {{$user->mother_name}}</label>
                            @endif
                            <label class="col-sm-12 control-label">{{$user->street_no}}</label>
                            <label class="col-sm-12 control-label">{{$user->area_type}} - {{$user->village}}</label>
                            <label class="col-sm-12 control-label">P.S - {{$user->police_station}}</label>
                            <label class="col-sm-12 control-label">P.O - {{$user->post_office}}</label>
                            <label class="col-sm-12 control-label">State - {{$user->state}}</label>
                            <label class="col-sm-12 control-label">District - {{$user->district}}</label>
                            <label class="col-sm-12 control-label">Pincode - {{$user->pincode}}</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Correspondence Address : </label>

                        <div class="col-sm-12">
                            @if($user->father_name)
                                <label class="col-sm-12 control-label">C/O - {{$user->father_name}}</label>
                            @else
                                <label class="col-sm-12 control-label">C/O - {{$user->mother_name}}</label>
                            @endif
                            <label class="col-sm-12 control-label">{{$user->correspondence_street_no}}</label>
                            <label class="col-sm-12 control-label">{{$user->area_type}} - {{$user->correspondence_village}}</label>
                            <label class="col-sm-12 control-label">P.S - {{$user->correspondence_police_station}}</label>
                            <label class="col-sm-12 control-label">P.O - {{$user->correspondence_post_office}}</label>
                            <label class="col-sm-12 control-label">State - {{$user->correspondence_state}}</label>
                            <label class="col-sm-12 control-label">District - {{$user->correspondence_district}}</label>
                            <label class="col-sm-12 control-label">Pincode - {{$user->correspondence_pincode}}</label>
                        </div>
                    </div>
                </div>              
            </div>
        </div>       
    </div>



    <div class="row">
        <div class="panel panel-default">
            <div id="heading" class="panel-heading"><h4>Academic Qualification (10<sup>th</sup> Standard)<h4></div>
            <div class="panel-body">

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-8 control-label">Board Name : </label>

                        <div class="col-sm-4">
                            <label class="col-sm-12 control-label">{{ $academic->board_name_classten}}</label>
                        </div>
                    </div>

                    @if($academic->specify_other_board != NULL)
                    <div class="form-group">
                        <label class="col-sm-8 control-label">Specified Other Board: </label>

                        <div class="col-sm-4">
                            <label class="col-sm-12 control-label">{{$academic->specify_other_board}}</label>
                        </div>
                    </div> 
                    @endif 

                    


                    <div class="form-group">
                        <label class="col-sm-8 control-label">Passing Year</label>

                        <div class="col-sm-4">
                            <label class="col-sm-12 control-label">{{$academic->passing_year_10}}</label>
                        </div>
                    </div>  


                       
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-8 control-label">Total Marks except Additional Subject : </label>

                        <div class="col-sm-4">
                            <label class="col-sm-12 control-label">{{ $academic->class10fm}}</label>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-8 control-label">Obtained Marks except Additional Subject : </label>

                        <div class="col-sm-4">
                            <label class="col-sm-12 control-label">{{ $academic->class10mrk}}</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-8 control-label">Percentage</label>

                        <div class="col-sm-4">
                            <label class="col-sm-12 control-label">{{$academic->classten_percentage}}</label>
                        </div>
                    </div> 
                       
                </div>
                           
            </div>
        </div>       
    </div>






    <div class="row">
        <div class="panel panel-default">
            <div id="heading" class="panel-heading"><h4>Academic Qualification (10+2 Standard)<h4></div>
            <div class="panel-body">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Vocational : </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{ $academic->vocational}}</label>
                        </div>
                    </div>
                    {{-- @if($academic->vocational != "No")
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Syllabus : </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{$academic->syllabus}}</label>
                        </div>
                    </div> 
                    @endif --}}       
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Board Name: </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{$academic->board_name}}</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Year of Passing: </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{$academic->passing_year}}</label>
                        </div>
                    </div> 
                </div>             
            </div>
        </div>       
    </div>
    @if($academic->vocational == 'No')
    <div class="row">
        <div class="panel panel-default">
            <div id="heading" class="panel-heading"><h4>Subject Wise Marks Details<h4></div>
              <div class="panel-body">
                 <div class="col-sm-12">
                    <div align="center" class="table-responsive">
                             <table cellpadding="0" cellspacing="0" class="qualification-table">
                                <tr>
                                    <th class="th">Subject</th>
                                    <th class="th">(A) Physics</th>
                                    <th class="th">(B) Chemistry</th>
                                    <th class="th">(C) Biology</th>
                                    <th class="th">(D) English</th>
                                    <th class="th">Total(A+B+C+D)</th>
                                    <th class="th">Percentage</th>
                                </tr>
                                <tr> 
                                    <td class="th">Full Marks</td>
                                    <td class="th">{{$academic->fm1}}</td>
                                    <td class="th">{{$academic->fm2}}</td>
                                    <td class="th">{{$academic->fm3}}</td>
                                    <td class="th">{{$academic->fm4}}</td>
                                    <td class="th">{{$academic->total_fullmarks}}</td>
                                    <td class="th">Out of 100</td>
                                    
                                </tr>
                               
                                <tr>
                                    <td class="th">Obained Marks</td>
                                    <td class="th">{{$academic->mrk1}}</td>
                                    <td class="th">{{$academic->mrk2}}</td>
                                    <td class="th">{{$academic->mrk3}}</td>
                                    <td class="th">{{$academic->mrk4}}</td>
                                    <td class="th">{{$academic->total_mark}}</td>
                                    <td class="th" rowspan="1">{{$academic->percentage}}</td>
                                    
                                </tr>
                            </table>
                        </div>    
                 </div>
            </div>
        </div>   
    </div>
    @endif

    @if($academic->vocational == 'Yes')
    <div class="row">
        <div class="panel panel-default">
            <div id="heading" class="panel-heading">
                <h4>Subject Wise Marks Details(Vocational)</h4>
                <h5><b>SUBJECT CODE : (PPFV/POFR/POVG/HNMG/FMAP/CNMG/SEPR/PHMG/POFC)</b></h5>
                </div>
              <div class="panel-body">
                 <div align="center" class="col-sm-12">
                    <div class="table-responsive">
                             <table cellpadding="0" cellspacing="0" class="qualification-table">
                                <tr>
                                    <th class="th">Subject</th>
                                    <th class="th">English<br/></th>
                                    <th class="th">Biology<br/></th>
                                    <th class="th">{{$academic->science}}<br/></th>
                                    <th class="th"><b>Subject Code</b><br>{{$academic->paper1}}</th>
                                    <th class="th"><b>Subject Code</b><br>{{$academic->paper2}}</th>
                                    <th class="th">Total</th>
                                    <th class="th">Percentage</th>
                                </tr>
                                <tr> 
                                    <td class="th">Full Marks</td>
                                    <td class="th">{{$academic->fm1}}</td>
                                    <td class="th">{{$academic->fm2}}</td>
                                    <td class="th">{{$academic->fm3}}</td>
                                    <td class="th">{{$academic->fm4}}</td>
                                    <td class="th">{{$academic->fm5}}</td>
                                    <td class="th">{{$academic->total_fullmarks}}</td>
                                    <td class="th">Out of 100</td>
                                </tr>
                                <tr>
                                    <td class="th">Obained Marks</td>
                                    <td class="th">{{$academic->mrk1}}</td>
                                    <td class="th">{{$academic->mrk2}}</td>
                                    <td class="th">{{$academic->mrk3}}</td>
                                    <td class="th">{{$academic->mrk4}}</td>
                                    <td class="th">{{$academic->mrk5}}</td>
                                    <td class="th">{{$academic->total_mark}}</td>
                                    <td class="th" rowspan="1">{{$academic->percentage}}</td>
                                    
                                </tr>
                            </table>
                        </div>    
                 </div>
            </div>
        </div>   
    </div>
    @endif
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" checked="checked" disabled> I HEREBY DECLARE THAT THE INFORMATION FURNISHED ABOVE ARE TRUE TO THE BEST OF MY KNOWLEDGE AND THAT I SHALL ABIDE BY THE RULES AND REGULATION OF THE VISWAVIDYALAYA
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-bottom: 30px">
        <div class="col-sm-10">
            <a type="button" class="btn btn-success" href="{{url('student/home')}}">Back to Dashboard</a>   
        </div>
        <div class="col-sm-2">
            <a type="button" class="btn btn-primary" href="{{url('student/payment')}}">Proceed for Payment</a> 
        </div>
    </div>
</div>
@endsection


@section('css')
    <style type="text/css">
       table {
            width: 100%;
        }
        table, td, th {
            border: 1px solid #d3e0e9;
            padding: 5px;
        }
        label.col-sm-6 {
            font-weight: 400 !important;
        }
        @media only screen and (max-width:640px){
            .form-group {
                border: 1px solid #f2f2f2;
            }
        }
    </style>
@endsection


