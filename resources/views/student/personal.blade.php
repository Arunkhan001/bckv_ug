@extends('student.layout.auth')

@section('content')
<div class="container-fluid">
    <form class="form-horizontal" method="POST" action="{{ url('student/personal') }}">
        {{ csrf_field() }}
        <div class="col-sm-12">
            <div class="row">
                <div class="panel panel-default">
                    <div id="heading" align="center" class="panel-heading"><h4>Personal Information Details</h4></div>
                    <div class="panel-body">
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label"><b>*&nbsp;</b>Candidate Full Name</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="@if(old('name', null)!= null){{ old('name') }}@elseif(!empty($user)){{$user->name}} @endif" required>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('father_name') ? ' has-error' : '' }}">
                                <label for="father_name" class="col-md-4 control-label">Father's Name</label>

                                <div class="col-md-6">
                                    <input id="father_name" type="text" class="form-control" name="father_name" value="@if(old('father_name', null)!= null){{ old('father_name') }}@elseif(!empty($user)){{$user->father_name}}@endif">

                                    @if ($errors->has('father_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('father_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('mother_name') ? ' has-error' : '' }}">
                                <label for="mother_name" class="col-md-4 control-label"><b>*&nbsp;</b>Mother's Name</label>

                                <div class="col-md-6">
                                    <input id="mother_name" type="text" class="form-control" name="mother_name" value="@if(old('mother_name',null)!= null){{old('mother_name')}}@elseif(!empty($user)){{$user->mother_name}}@endif" required>
                                    @if ($errors->has('mother_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('mother_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('guardian_name') ? ' has-error' : '' }}">
                                <label for="guardian_name" class="col-md-4 control-label"><b>*&nbsp;</b>Guardian Name</label>

                                <div class="col-md-6">
                                    <input id="guardian_name" type="text" class="form-control" name="guardian_name" value="@if(old('guardian_name', null)!= null){{ old('guardian_name') }}@elseif(!empty($user)){{$user->guardian_name}}@endif" required>
                                    @if ($errors->has('guardian_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('guardian_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('guardian_mobile_number') ? ' has-error' : '' }}">
                                <label for="guardian_mobile_number" class="col-md-4 control-label"><b>*&nbsp;</b>Guardian Mobile Number</label>

                                <div class="col-md-6">
                                    <input id="guardian_mobile_number" type="text" class="form-control" name="guardian_mobile_number" value="@if(old('guardian_mobile_number', null)!= null){{ old('guardian_mobile_number') }}@elseif(!empty($user)){{$user->guardian_mobile_number}}@endif" required>
                                    @if ($errors->has('guardian_mobile_number'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('guardian_mobile_number') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('guardian_email') ? ' has-error' : '' }}">
                                <label for="guardian_email" class="col-md-4 control-label">Guardian Email</label>

                                <div class="col-md-6">
                                    <input id="guardian_email" type="email" placeholder="Guardian Email" class="form-control" name="guardian_email" value="@if(old('guardian_email', null)!= null){{ old('guardian_email') }}@elseif(!empty($user)){{$user->guardian_email}}@endif" title="If Guardian has a email ID">
                                    @if ($errors->has('guardian_email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('guardian_email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('guardian_relationship') ? ' has-error' : '' }}">
                                <label for="guardian_relationship" class="col-md-4 control-label"><b>*&nbsp;</b>Relation with Guardian</label>
                                <div class="col-md-6">
                                    <input id="guardian_relationship" type="text" class="form-control" name="guardian_relationship" value="@if(old('guardian_relationship', null)!= null){{ old('guardian_relationship') }}@elseif(!empty($user)){{$user->guardian_relationship}}@endif" required>
                                    @if ($errors->has('guardian_relationship'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('guardian_relationship') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('date_of_birth') ? ' has-error' : '' }}">
                                <label for="datepicker" class="col-md-4 control-label"><b>*&nbsp;</b>Date of Birth</label>
                                <div class="col-md-6">
                                    <input id="datepicker" type="text" class="form-control" name="date_of_birth" value="@if(old('date_of_birth', null)!= null){{ old('date_of_birth') }}@elseif(!empty($user)){{$user->date_of_birth}}@endif" placeholder="yyyy-mm-dd" required autocomplete="off">
                                    @if ($errors->has('date_of_birth'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('date_of_birth') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label"><b>*&nbsp;</b>Gender</label>
                                <div class="col-md-6">
                                    <label class="radio-inline"><input type="radio" name="gender" value="Male" @if(old('gender')=='Male') checked @elseif(!empty($user) && $user->gender == 'Male') checked @endif required>Male</label>
                                    <label class="radio-inline"><input type="radio"  name="gender" value="Female" @if(old('gender')=='Female') checked @elseif(!empty($user) && $user->gender == 'Female') checked @endif>Female</label>
                                    <label class="radio-inline"><input type="radio"  name="gender" value="Other" @if(old('gender')=='Other') checked @elseif(!empty($user) && $user->gender == 'Other') checked @endif>Transgender</label>
                                    @if ($errors->has('gender'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('blood_group') ? ' has-error' : '' }}">
                                <label for="blood_group" class="col-md-4 control-label">Blood Group</label>
                                <div class="col-md-6">
                                    <select id="blood_group" name="blood_group" class="form-control">
                                        <option value="">Select</option>
                                        <option value="A+" @if(old('blood_group',$user->blood_group) == 'A+') selected @endif>A+</option>
                                        <option value="A-" @if(old('blood_group',$user->blood_group) == 'A-') selected @endif>A-</option>
                                        <option value="B+" @if(old('blood_group',$user->blood_group) == 'B+') selected @endif>B+</option>
                                        <option value="B-" @if(old('blood_group',$user->blood_group) == 'B-') selected @endif>B-</option>
                                        <option value="AB+" @if(old('blood_group',$user->blood_group) == 'AB+') selected @endif>AB+</option>
                                        <option value="AB-" @if(old('blood_group',$user->blood_group) == 'AB-') selected @endif>AB-</option>
                                        <option value="O+" @if(old('blood_group',$user->blood_group) == 'O+') selected @endif>O+</option>
                                        <option value="O-" @if(old('blood_group',$user->blood_group) == 'O-') selected @endif>O-</option>
                                    </select>
                                    @if ($errors->has('blood_group'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('blood_group') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            @if($user->status < 6) {{-- FOR CHANGE CATEGERY BEFORE PAYMENT --}}
                            <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                                <label for="category" class="col-md-4 control-label"><b>*&nbsp;</b>Category</label>

                                <div class="col-md-6">
                                    <select id="category" name="category" class="form-control" required>
                                        <option value="">Select</option>
                                        <option value="General" @if(old('category')=='General') selected @elseif(!empty($user) && $user->category == 'General') selected @endif>General (Unreserved)</option>
                                        <option value="SC" @if(old('category')=='SC') selected @elseif(!empty($user) && $user->category == 'SC') selected @endif>SC</option>
                                        <option value="ST" @if(old('category')=='ST') selected @elseif(!empty($user) && $user->category == 'ST') selected @endif>ST</option>
                                        <option value="OBC-A" @if(old('category')=='OBC-A') selected @elseif(!empty($user) && $user->category == 'OBC-A') selected @endif>OBC-A</option>
                                        <option value="OBC-B" @if(old('category')=='OBC-B') selected @elseif(!empty($user) && $user->category == 'OBC-B') selected @endif>OBC-B</option>
                                        <option value="Physically Challange" @if(old('category')=='Physically Challange') selected @elseif(!empty($user) && $user->category == 'Physically Challange') selected @endif>Physically Challange</option>
                                        
                                    </select>
                                    @if ($errors->has('category'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            @else
                             {{-- FOR NOT CHANGE CATEGERY AFTER PAYMENT --}}
                            <input type="hidden" name="category" value="{{ $user->category }}">
                            <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                                <label for="category" class="col-md-4 control-label"><b>*&nbsp;</b>Category</label>

                                <div class="col-md-6">
                                    <select id="category" name="category" class="form-control" disabled>
                                        <option value="">Select</option>
                                        <option value="General" @if(old('category')=='General') selected @elseif(!empty($user) && $user->category == 'General') selected @endif>General (Unreserved)</option>
                                        <option value="SC" @if(old('category')=='SC') selected @elseif(!empty($user) && $user->category == 'SC') selected @endif>SC</option>
                                        <option value="ST" @if(old('category')=='ST') selected @elseif(!empty($user) && $user->category == 'ST') selected @endif>ST</option>
                                        <option value="OBC-A" @if(old('category')=='OBC-A') selected @elseif(!empty($user) && $user->category == 'OBC-A') selected @endif>OBC-A</option>
                                        <option value="OBC-B" @if(old('category')=='OBC-B') selected @elseif(!empty($user) && $user->category == 'OBC-B') selected @endif>OBC-B</option>
                                        <option value="Physically Challange" @if(old('category')=='Physically Challange') selected @elseif(!empty($user) && $user->category == 'Physically Challange') selected @endif>Physically Challange</option>
                                        
                                    </select>
                                    
                                </div>
                            </div>
                            @endif
                            <div id="student_certificate" class="form-group{{ $errors->has('certificate_number') ? ' has-error' : '' }}">
                                <label for="certificate_number" class="col-md-4 control-label"><b class="mandetory hide">*&nbsp;</b>Certificate Number</label>
                                <div class="col-md-6">
                                    <input class="form-control" id="certificate_number" type="text" name="certificate_number" value="@if(old('certificate_number', null)!= null){{ old('certificate_number') }}@elseif(!empty($user)){{$user->certificate_number}}@endif" placeholder="Certificate number"/>
                                    @if ($errors->has('certificate_number'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('certificate_number') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div id="student_certificate_date" class="form-group{{ $errors->has('issuing_date') ? ' has-error' : '' }}">
                                <label for="issuing_date" class="col-md-4 control-label"><b class="mandetory hide">*&nbsp;</b>Certificate Issuing Date</label>
                                <div class="col-md-6">
                                    <input class="form-control" id="issuing_date" type="text" name="issuing_date" value="@if(old('issuing_date', null)!= null){{ old('issuing_date') }}@elseif(!empty($user)){{$user->issuing_date}}@endif" placeholder="yyyy-mm-dd" autocomplete="off"/>
                                    @if ($errors->has('issuing_date'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('issuing_date') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('sport_quota') ? ' has-error' : '' }}">
                                <label for="sport_quota" class="col-md-4 control-label"><b>*&nbsp;</b>Sport Quota</label>
                                <div class="col-md-6">
                                    <select id="sport_quota" name="sport_quota" class="form-control" required @if($user->sport_quota == "Yes" || $user->sport_quota == "No") disabled @endif>
                                        <option value="">Select</option>
                                        <option value="Yes" @if(old('sport_quota',$user->sport_quota) == 'Yes') selected @endif>Yes</option>
                                        <option value="No" @if(old('sport_quota',$user->sport_quota) == 'No') selected @endif>No</option>
                                    </select>
                                    @if ($errors->has('sport_quota'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('sport_quota') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                @if($user->sport_quota == "Yes" || $user->sport_quota == "No")
                                    <input type="hidden" name="sport_quota" value="{{ $user->sport_quota }}">
                                @endif
                            </div>

                            <div id="sport_certificate" class="form-group{{ $errors->has('sport_certificate_number') ? ' has-error' : '' }}">
                                <label for="sport_certificate_number" class="col-md-4 control-label"><b class="mandetory1 hide">*&nbsp;</b>Sport Certificate Number</label>
                                <div class="col-md-6">
                                    <input class="form-control" id="sport_certificate_number" type="text" name="sport_certificate_number" value="@if(old('sport_certificate_number', null)!= null){{ old('sport_certificate_number') }}@elseif(!empty($user)){{$user->sport_certificate_number}}@endif" placeholder="Certificate number"/>
                                    @if ($errors->has('sport_certificate_number'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('sport_certificate_number') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div id="sport_certificate_date" class="form-group{{ $errors->has('sport_issuing_date') ? ' has-error' : '' }}">
                                <label for="sport_issuing_date" class="col-md-4 control-label"><b class="mandetory1 hide">*&nbsp;</b>Sport Certificate Issuing Date</label>
                                <div class="col-md-6">
                                    <input class="form-control" id="sport_issuing_date" type="text" name="sport_issuing_date" value="@if(old('sport_issuing_date', null)!= null){{ old('sport_issuing_date') }}@elseif(!empty($user)){{$user->sport_issuing_date}}@endif" placeholder="yyyy-mm-dd" autocomplete="off"/>
                                    @if ($errors->has('sport_issuing_date'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('sport_issuing_date') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group{{ $errors->has('residential_area_type') ? ' has-error' : '' }}">
                                <label for="residential_area_type" class="col-md-4 control-label"><b>*&nbsp;</b>Residential Area Type</label>
                                <div class="col-md-6">
                                    <select id="residential_area_type" name="residential_area_type" class="form-control" required>
                                        <option value="">Select</option>
                                        <option value="Urban" @if(old('residential_area_type',$user->residential_area_type) == 'Urban') selected @endif>Urban</option>
                                        <option value="Rural" @if(old('residential_area_type',$user->residential_area_type) == 'Rural') selected @endif>Rural</option>
                                    </select>
                                    @if ($errors->has('residential_area_type'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('residential_area_type') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('domicile') ? ' has-error' : '' }}">
                                <label for="domicile" class="col-md-4 control-label"><b>*&nbsp;</b>Domicile</label>
                                <div class="col-md-6">
                                    <select id="domicile" name="domicile" class="form-control" required>
                                        <option value="west bengal" @if(old('domicile',$user->domicile) == 'West Bengal') selected @endif>West Bengal</option>
                                    </select>
                                    @if ($errors->has('domicile'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('domicile') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('nationality') ? ' has-error' : '' }}">
                                <label for="nationality" class="col-md-4 control-label"><b>*&nbsp;</b>Nationality</label>
                                <div class="col-md-6">
                                    <input id="nationality" type="text" class="form-control" name="nationality" @if(old('nationality',null)!=null) value="{{ old('nationality') }}" @elseif(empty($user->nationality)) value="INDIAN" @elseif(!empty($user)) value="{{$user->nationality}}" @endif required>
                                    @if ($errors->has('nationality'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('nationality') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('religion') ? ' has-error' : '' }}">
                                <label for="religion" class="col-md-4 control-label">Religion</label>
                                <div class="col-md-6">
                                    <select id="religion" name="religion" class="form-control">
                                        <option value="" >Select</option>
                                        <option value="Hinduism" @if(old('religion',$user->religion) == 'Hinduism') selected @endif>Hinduism</option>
                                        <option value="Muslim" @if(old('religion',$user->religion) == 'Muslim') selected @endif>Muslim</option>
                                        <option value="Christian" @if(old('religion',$user->religion) == 'Christian') selected @endif>Christian</option>
                                        <option value="Buddhism" @if(old('religion',$user->religion) == 'Buddhism') selected @endif>Buddhism</option>
                                        <option value="other" @if($user->religion==null)@elseif(old('religion',$user->religion) != 'Hinduism' && old('religion',$user->religion) != 'Muslim'  && old('religion',$user->religion) != 'Christian' && old('religion',$user->religion) != 'Buddhism') selected @endif>Other</option>
                                    </select>
                                    @if ($errors->has('religion'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('religion') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div id="student_religion" class="form-group{{ $errors->has('religion') ? ' has-error' : '' }}">
                                <label for="student_religion_other" class="col-md-4 control-label">Type Religion Name</label>
                                <div class="col-md-6">
                                    <input class="form-control" id="student_religion_other" type="text" name="religion" value="@if(old('religion', null)!= null){{ old('religion') }}@elseif(!empty($user)){{$user->religion}}@endif"/>
                                    @if ($errors->has('religion'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('religion') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            {{-- <div class="form-group{{ $errors->has('aadhaar') ? ' has-error' : '' }}">
                                <label for="aadhaar" class="col-md-4 control-label">Aadhaar</label>
                                <div class="col-md-6">
                                    <select id="aadhaar" name="aadhaar" class="form-control" required >
                                        <option value="">Select</option>
                                        <option value="Student Aadhaar" @if(old('aadhaar',$user->aadhaar)=="Student Aadhaar") selected @endif>If student have Aadhaar Number</option>
                                        <option value="Guardian Aadhaar" @if(old('aadhaar',$user->aadhaar) == "Guardian Aadhaar") selected @endif>If only guardian have Aadhaar Number
                                        </option>
                                        <option value="No" @if(old('aadhaar',$user->aadhaar)=="No") selected @endif>Do not have any of the Above
                                        </option>
                                    </select>
                                    @if ($errors->has('aadhaar'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('aadhaar') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div> --}}
                            <div id="student_aadhaar" class="form-group{{ $errors->has('student_aadhaar_number') ? ' has-error' : '' }}">
                                <label for="student_aadhaar_number" class="col-md-4 control-label">Candidate's Aadhaar Number</label>
                                <div class="col-md-6">
                                    <input class="form-control" id="student_aadhaar_number" type="text" name="student_aadhaar_number" value="@if(old('student_aadhaar_number', null)!= null){{ old('student_aadhaar_number') }}@elseif(!empty($user)){{$user->student_aadhaar_number}}@endif"/>
                                    @if ($errors->has('student_aadhaar_number'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('student_aadhaar_number') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            {{-- <div id="guardian_aadhaar" class="form-group{{ $errors->has('guardian_aadhaar_number') ? ' has-error' : '' }}">
                                <label for="guardian_aadhaar_number" class="col-md-4 control-label">Guardian's Aadhaar Number</label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" id="guardian_aadhaar_number" name="guardian_aadhaar_number" value="@if(old('guardian_aadhaar_number', null)!= null){{ old('guardian_aadhaar_number') }}@elseif(!empty($user)){{$user->guardian_aadhaar_number}}@endif"/>
                                    @if ($errors->has('guardian_aadhaar_number'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('guardian_aadhaar_number') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div id="guardian_aadhaar_name" class="form-group{{ $errors->has('guardian_aadhaar_name') ? ' has-error' : '' }}">
                                <label for="guardian_aadhaar_name_inp" class="col-md-4 control-label">Guardian's Name</label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" id="guardian_aadhaar_name_inp" name="guardian_aadhaar_name" value="@if(old('guardian_aadhaar_name', null)!= null){{ old('guardian_aadhaar_name') }}@elseif(!empty($user)){{$user->guardian_aadhaar_name}}@endif"/>
                                    @if ($errors->has('guardian_aadhaar_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('guardian_aadhaar_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div id="guardian_aadhaar_rel" class="form-group{{ $errors->has('guardian_relation') ? ' has-error' : '' }}">
                                <label for="guardian_relation" class="col-md-4 control-label">Guardian's Relation with Student</label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" id="guardian_relation" name="guardian_relation" value="@if(old('guardian_relation', null)!= null){{ old('guardian_relation') }}@elseif(!empty($user)){{$user->guardian_relation}}@endif"/>
                                    @if ($errors->has('guardian_relation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('guardian_relation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div> --}}

                            <div class="form-group{{ $errors->has('bank_account_details') ? ' has-error' : '' }}">
                                <label for="bank_account_details" class="col-md-4 control-label">Candidate's S/B Account NO</label>
                                <div class="col-md-6">
                                    <input id="bank_account_details" type="text" class="form-control" name="bank_account_details" @if(old('bank_account_details',null)!=null) value="{{ old('bank_account_details') }}" @elseif(!empty($user)) value="{{$user->bank_account_details}}" @endif>
                                    @if ($errors->has('bank_account_details'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('bank_account_details') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('bank_ifsc_code') ? ' has-error' : '' }}">
                                <label for="bank_ifsc_code" class="col-md-4 control-label">Bank IFS Code</label>
                                <div class="col-md-6">
                                    <input id="bank_ifsc_code" type="text" class="form-control" name="bank_ifsc_code" @if(old('bank_ifsc_code',null)!=null) value="{{ old('bank_ifsc_code') }}" @elseif(!empty($user)) value="{{$user->bank_ifsc_code}}" @endif>
                                    @if ($errors->has('bank_ifsc_code'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('bank_ifsc_code') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-md-6">
                            <h4>&nbsp;&nbsp;&nbsp;&nbsp;<b>*&nbsp;</b>Permanent Address</h4><br/>
                            <div class="form-group{{ $errors->has('street_no') ? ' has-error' : '' }}">
                                <label for="street_no" class="col-md-4 control-label">House No/Street</label>

                                <div class="col-md-6">
                                    <input id="street_no" type="text" class="form-control" name="street_no" value="@if(old('street_no', null)!= null){{ old('street_no') }}@elseif(!empty($user)){{$user->street_no}}@endif">

                                    @if ($errors->has('street_no'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('street_no') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('area_type') ? ' has-error' : '' }}">
                                <label for="area_type" class="col-md-4 control-label"><b>*&nbsp;</b>Area Type</label>
                                <div class="col-md-6">
                                    <select id="area_type" name="area_type" class="form-control" required >
                                        <option value="">Select</option>
                                        <option value="Village" @if(old('area_type',$user->area_type)=="Village") selected @endif>Village</option>
                                        <option value="Town" @if(old('area_type',$user->area_type)=="Town") selected @endif>Town</option>
                                       
                                    </select>
                                    @if ($errors->has('area_type'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('area_type') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('village') ? ' has-error' : '' }}">
                                <label for="village" class="col-md-4 control-label"><b>*&nbsp;</b>Village/Town Name</label>

                                <div class="col-md-6">
                                    <input id="village" type="text" class="form-control" name="village" value="@if(old('village', null)!= null){{ old('village') }}@elseif(!empty($user)){{$user->village}}@endif" required>

                                    @if ($errors->has('village'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('village') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('post_office') ? ' has-error' : '' }}">
                                <label for="post_office" class="col-md-4 control-label"><b>*&nbsp;</b>Post Office</label>

                                <div class="col-md-6">
                                    <input id="post_office" type="text" class="form-control" name="post_office" value="@if(old('post_office', null)!= null){{ old('post_office') }}@elseif(!empty($user)){{$user->post_office}}@endif" required>

                                    @if ($errors->has('post_office'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('post_office') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('police_station') ? ' has-error' : '' }}">
                                <label for="police_station" class="col-md-4 control-label"><b>*&nbsp;</b>Police Station</label>

                                <div class="col-md-6">
                                    <input id="police_station" type="text" class="form-control" name="police_station" value="@if(old('police_station', null)!= null){{ old('police_station') }}@elseif(!empty($user)){{$user->police_station}}@endif" required>

                                    @if ($errors->has('police_station'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('police_station') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                                <label for="state" class="col-md-4 control-label"><b>*&nbsp;</b>State</label>

                                <div class="col-md-6">
                                    <input id="state" type="text" class="form-control" name="state" value="@if(old('state', null)!= null){{ old('state') }}@elseif(!empty($user)){{$user->state}}@endif" required>

                                    @if ($errors->has('state'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('state') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('district') ? ' has-error' : '' }}">
                                <label for="district" class="col-md-4 control-label"><b>*&nbsp;</b>District</label>

                                <div class="col-md-6">
                                    <input id="district" type="text" class="form-control" name="district" value="@if(old('district', null)!= null){{ old('district') }}@elseif(!empty($user)){{$user->district}}@endif" required>

                                    @if ($errors->has('district'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('district') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('pincode') ? ' has-error' : '' }}">
                                <label for="pincode" class="col-md-4 control-label"><b>*&nbsp;</b>Pincode</label>

                                <div class="col-md-6">
                                    <input id="pincode" type="text" class="form-control" name="pincode" value="@if(old('pincode', null)!= null){{ old('pincode') }}@elseif(!empty($user)){{$user->pincode}}@endif" required>

                                    @if ($errors->has('pincode'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('pincode') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-md-6">
                            <h4>&nbsp;&nbsp;&nbsp;&nbsp;<b>*&nbsp;</b>Correspondence Address</h4><br>
                            <input type="checkbox" id="do"> (Tick if Same as Permanent Address)
                            <div class="form-group{{ $errors->has('correspondence_street_no') ? ' has-error' : '' }}">
                                <label for="correspondence_street_no" class="col-md-4 control-label">House No/Street</label>

                                <div class="col-md-6">
                                    <input id="correspondence_street_no" type="text" class="form-control" name="correspondence_street_no" value="@if(old('correspondence_street_no', null)!= null){{ old('correspondence_street_no') }}@elseif(!empty($user)){{$user->correspondence_street_no}}@endif">

                                    @if ($errors->has('correspondence_street_no'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('correspondence_street_no') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('correspondence_area_type') ? ' has-error' : '' }}">
                                <label for="correspondence_area_type" class="col-md-4 control-label"><b>*&nbsp;</b>Area Type</label>
                                <div class="col-md-6">
                                    <select id="correspondence_area_type" name="correspondence_area_type" class="form-control" required >
                                        <option value="">Select</option>
                                        <option value="Village" @if(old('correspondence_area_type',$user->correspondence_area_type)=="Village") selected @endif>Village</option>
                                        <option value="Town" @if(old('correspondence_area_type',$user->correspondence_area_type)=="Town") selected @endif>Town</option>
                                       
                                    </select>
                                    @if ($errors->has('correspondence_area_type'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('correspondence_area_type') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('correspondence_village') ? ' has-error' : '' }}">
                                <label for="correspondence_village" class="col-md-4 control-label"><b>*&nbsp;</b>Village/Town</label>

                                <div class="col-md-6">
                                    <input id="correspondence_village" type="text" class="form-control" name="correspondence_village" value="@if(old('correspondence_village', null)!= null){{ old('correspondence_village') }}@elseif(!empty($user)){{$user->correspondence_village}}@endif" required>

                                    @if ($errors->has('correspondence_village'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('correspondence_village') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('correspondence_post_office') ? ' has-error' : '' }}">
                                <label for="correspondence_post_office" class="col-md-4 control-label"><b>*&nbsp;</b>Post Office</label>

                                <div class="col-md-6">
                                    <input id="correspondence_post_office" type="text" class="form-control" name="correspondence_post_office" value="@if(old('correspondence_post_office', null)!= null){{ old('correspondence_post_office') }}@elseif(!empty($user)){{$user->correspondence_post_office}}@endif" required>

                                    @if ($errors->has('correspondence_post_office'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('correspondence_post_office') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('correspondence_police_station') ? ' has-error' : '' }}">
                                <label for="correspondence_police_station" class="col-md-4 control-label"><b>*&nbsp;</b>Police Station</label>

                                <div class="col-md-6">
                                    <input id="correspondence_police_station" type="text" class="form-control" name="correspondence_police_station" value="@if(old('correspondence_police_station', null)!= null){{ old('correspondence_police_station') }}@elseif(!empty($user)){{$user->correspondence_police_station}}@endif" required>

                                    @if ($errors->has('correspondence_police_station'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('correspondence_police_station') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('correspondence_state') ? ' has-error' : '' }}">
                                <label for="correspondence_state" class="col-md-4 control-label"><b>*&nbsp;</b>State</label>

                                <div class="col-md-6">
                                    <input id="correspondence_state" type="text" class="form-control" name="correspondence_state" value="@if(old('correspondence_state', null)!= null){{ old('correspondence_state') }}@elseif(!empty($user)){{$user->correspondence_state}}@endif" required>

                                    @if ($errors->has('correspondence_state'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('correspondence_state') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('correspondence_district') ? ' has-error' : '' }}">
                                <label for="correspondence_district" class="col-md-4 control-label"><b>*&nbsp;</b>District</label>

                                <div class="col-md-6">
                                    <input id="correspondence_district" type="text" class="form-control" name="correspondence_district" value="@if(old('correspondence_district', null)!= null){{ old('correspondence_district') }}@elseif(!empty($user)){{$user->correspondence_district}}@endif" required>

                                    @if ($errors->has('correspondence_district'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('correspondence_district') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('correspondence_pincode') ? ' has-error' : '' }}">
                                <label for="correspondence_pincode" class="col-md-4 control-label"><b>*&nbsp;</b>Pincode</label>

                                <div class="col-md-6">
                                    <input id="correspondence_pincode" type="text" class="form-control" name="correspondence_pincode" value="@if(old('correspondence_pincode', null)!= null){{ old('correspondence_pincode') }}@elseif(!empty($user)){{$user->correspondence_pincode}}@endif" required>

                                    @if ($errors->has('correspondence_pincode'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('correspondence_pincode') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom: 30px">
                <div class="col-sm-8">
                </div>
                <div class="col-sm-2">
                    <a class="btn btn-success" href="{{url('student/home')}}">Back to Dashboard</a>
                </div>
                <div class="col-sm-2">
                    <input type="submit" value="Save &amp; Proceed" class='btn btn-primary'>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@section('css')
<link href="{{ asset('css/jquery-ui.min.css') }}" rel="stylesheet">
<style>
    b {
        color: red;
        font-size: 16px;
    } 
    #student_religion,#student_certificate,#student_certificate_date {
        display: none;
    }
</style>
@endsection

@section('js')
    <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
    <script>
        $(document).ready(function(){
           $("#student_religion,#student_certificate,#student_certificate_date,#sport_certificate,#sport_certificate_date").hide();
           $("#datepicker").datepicker({
                dateFormat: "yy-mm-dd",
                changeMonth: true,
                changeYear: true,
                /*showButtonPanel: true,*/
                defaultDate: new Date(2000, 0, 1),
                maxDate: new Date(2004, 8, 31),
                /*maxDate: new Date(2001, 7, 31),
                minDate: new Date(1990, 1, 1)*/
                yearRange: "1990:2004",
            });
            $("#issuing_date").datepicker({
                dateFormat: "yy-mm-dd",
                changeMonth: true,
                changeYear: true,
                /*showButtonPanel: true,*/
                defaultDate: new Date(2004, 0, 1),
                maxDate: new Date(2020, 9, 3),
                yearRange: "2000:2020",
               /* minDate: new Date(1990, 1, 1)*/
            });
            $("#sport_issuing_date").datepicker({
                dateFormat: "yy-mm-dd",
                changeMonth: true,
                changeYear: true,
                /*showButtonPanel: true,*/
                defaultDate: new Date(2004, 0, 1),
                maxDate: new Date(2020, 9, 3),
                yearRange: "2000:2020",
               /* minDate: new Date(1990, 1, 1)*/
            });
            category();
            sport_quota();
            // changeAadharInput();
            if($("#religion").val() == 'other') {
                $("#student_religion").show('slow/400/fast');
            }

            $('#do').change(function () {
                if ($(this).is(':checked')) {
                    $('input[name=correspondence_street_no]').val($('input[name=street_no]').val());
                    $('input[name=correspondence_village]').val($('input[name=village]').val());
                    $('input[name=correspondence_police_station]').val($('input[name=police_station]').val());
                    $('input[name=correspondence_post_office]').val($('input[name=post_office]').val());
                    $('input[name=correspondence_pincode]').val($('input[name=pincode]').val());
                    $('input[name=correspondence_village]').val($('input[name=village]').val());
                    $('input[name=correspondence_district]').val($('input[name=district]').val());
                    $('input[name=correspondence_state]').val($('input[name=state]').val());
                }
                else {
                    $('input[name=correspondence_street_no]').val('');
                    $('input[name=correspondence_village]').val('');
                    $('input[name=correspondence_police_station]').val('');
                    $('input[name=correspondence_post_office]').val('');
                    $('input[name=correspondence_pincode]').val('');
                    $('input[name=correspondence_village]').val('');
                    $('input[name=correspondence_district]').val('');
                    $('input[name=correspondence_state]').val('');
                }
            });

        });
        function otherReligion() {
            if($("#religion").val() == 'other') {
                $("#student_religion_other").val('');
                $("#student_religion").show('slow/400/fast');
            }else{
                $("#student_religion_other").val($("#religion").val());
                $("#student_religion").hide('slow/400/fast');
            }
        }
        // function changeAadharInput() {

        //     if($("#aadhaar").val() == 'Student Aadhaar') {
        //         $('#guardian_aadhaar,#guardian_aadhaar_name,#guardian_aadhaar_rel').hide('slow/400/fast');
        //         $('#student_aadhaar').show('slow/400/fast');
        //         $("input[name=guardian_aadhaar_number],input[name=guardian_aadhaar_name],input[name=guardian_relation]").val('');
        //     }
        //     else if($("#aadhaar").val() == 'Guardian Aadhaar') {
        //         $('#student_aadhaar').hide('slow/400/fast');
        //         $('#guardian_aadhaar,#guardian_aadhaar_name,#guardian_aadhaar_rel').show('slow/400/fast');
        //         $("input[name=student_aadhaar_number]").val('');
        //     }
        //     else {
        //         $("#student_aadhaar,#guardian_aadhaar,#guardian_aadhaar_name,#guardian_aadhaar_rel").hide('slow/400/fast');
        //         $("input[name=guardian_aadhaar_number],input[name=guardian_aadhaar_name],input[name=guardian_relation],input[name=student_aadhaar_number]").val('');
        //     }
        // }
        function category () {

            if($("#category").val() != '' && $("#category").val() != 'General') {
                $('#certificate_number,#issuing_date').removeAttr('disabled');
                if($("#category").val() != 'Sport') {
                    $(".mandetory").removeClass('hide');
                    $('#certificate_number,#issuing_date').attr('required','required');
                }else{
                    $('#certificate_number,#issuing_date').removeAttr('required');
                    $(".mandetory").addClass('hide');
                }
                $('#student_certificate,#student_certificate_date').show('slow/400/fast');
            }else {
                $(".mandetory").addClass('hide');
                $('#certificate_number,#issuing_date').removeAttr('required');
                $('#certificate_number,#issuing_date').attr('disabled','disabled');
                $('#student_certificate,#student_certificate_date').hide('slow/400/fast');
            }
        }

        function sport_quota () {

            if($("#sport_quota").val() == 'Yes') {
                $('#sport_certificate_number,#sport_issuing_date').removeAttr('disabled');
                $(".mandetory1").removeClass('hide');
                $('#sport_certificate_number,#sport_issuing_date').attr('required','required');
                $('#sport_certificate,#sport_certificate_date').show('slow/400/fast');
            }else {
                $(".mandetory1").addClass('hide');
                $('#sport_certificate_number,#sport_issuing_date').removeAttr('required');
                $('#sport_certificate_number,#sport_issuing_date').attr('disabled','disabled');
                $('#sport_certificate,#sport_certificate_date').hide('slow/400/fast');
            }
        }

        // $(document).on('change', '#aadhaar', function () {
        //     changeAadharInput();
        // });
        $(document).on('change', '#religion', function () {
            otherReligion();
        });
        $(document).on('change', '#category', function() {
            category();
        })
        $(document).on('change', '#sport_quota', function() {
            sport_quota();
        })
        
    </script>
@endsection
