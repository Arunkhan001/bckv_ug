@extends('student.layout.auth')

@section('content')
<style>
    b{
        font-size: 15px;
    }
    th{
        text-align: center;
    }
</style>
    <div class="container">
        <form class="form-horizontal" method="POST" action="{{ url('student/academic') }}">
            {{ csrf_field() }}
            <div class="container">
                <div class="row">
                    @if (session('alert'))
                        <div class="alert alert-success">
                            {{ session('alert') }}
                        </div>
                    @endif
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if ($errors->has('percentage'))
                        <div class="alert alert-danger">
                            <strong>{{ $errors->first('percentage') }}</strong>
                        </div>
                    @endif


                    <div class="panel panel-default">
                        <div id="heading_tenth" align="center" class="panel-heading"><h4>Academic Information Details(10<sup>th</sup>)</h4></div>
                        <div class="panel-body">

                            <div class="col-md-6 form-group{{ $errors->has('board_name_classten') ? ' has-error' : '' }}">
                                <h4><label for="name" class="col-md-4 control-label"><b>*&nbsp;</b>Board</label></h4>
                                <div class="col-md-5">
                                    <select id="board_name_classten" name="board_name_classten" class="form-control" required>
                                        <option value="">Select</option>
                                        <option value="WBBSE" @if(old('board_name_classten')=='WBBSE') selected @elseif(!empty($user) && $user->board_name_classten == 'WBBSE') selected @endif>WBBSE</option>
                                        <option value="CBSE" @if(old('board_name_classten')=='CBSE') selected @elseif(!empty($user) && $user->board_name_classten == 'CBSE') selected @endif>CBSE</option>
                                        <option value="ICSE" @if(old('board_name_classten')=='ICSE') selected @elseif(!empty($user) && $user->board_name_classten == 'ICSE') selected @endif>ICSE</option>
                                        <option value="Other Board" @if(old('board_name_classten')=='Other Board') selected @elseif(!empty($user) && $user->board_name_classten == 'Other Board') selected @endif>Other Board</option>
                                    </select>
                                    @if ($errors->has('board_name_classten'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('board_name_classten') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div id="specify_other_board" style="display: none;" class="col-md-6 form-group{{ $errors->has('specify_other_board') ? ' has-error' : '' }}">
                                <h4><label for="name" class="col-md-6 control-label"><b>*&nbsp;</b>Specify Other Board</label></h4>
                                <div class="col-md-5">
                                    <input class="form-control"  type="text" name="specify_other_board" value="@if(old('specify_other_board', null)!= null){{ old('specify_other_board') }}@elseif(!empty($user)){{$user->specify_other_board}}@endif" />
                                    @if ($errors->has('specify_other_board'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('specify_other_board') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                        <div class="col-md-6">
                        <div class="table-responsive">
                            <table align="center">
                                
                                <tr>
                                    <td align="center"><b>*&nbsp;</b>Total Marks except Additional Subject &nbsp;&nbsp;&nbsp;</td>
                                    <td class="{{ $errors->has('class10fm') ? ' has-error' : '' }}"><input id="class10fm" class="form-control" maxlength="3" type="number" name="class10fm" value="@if(old('class10fm', null)!= null){{ old('class10fm') }}@elseif(!empty($user)){{$user->class10fm}}@endif" placeholder="Full Marks" required /></td>
                                    

                                </tr>

                                <tr>
                                    <td align="center"><b>*&nbsp;</b>Obtained Marks except Additional Subject &nbsp;&nbsp;&nbsp;</td>
                                    <td class="{{ $errors->has('class10mrk') ? ' has-error' : '' }}"><input id="class10mrk" class="form-control" maxlength="3" type="number" name="class10mrk" value="@if(old('class10mrk', null)!= null){{ old('class10mrk') }}@elseif(!empty($user)){{$user->class10mrk}}@endif" required /></td>
                                    
                                </tr>
                            </table>
                        </div>
                     </div>

                      <div class="col-md-6">

                     <div class="col-md-12 form-group{{ $errors->has('classtenpercantage') ? ' has-error' : '' }}">
                                <h4><label for="name" class="col-md-6 control-label"><b>*&nbsp;</b>Class 10 Percentage</label></h4>
                                <div class="col-md-5">
                                    <input id="classtenpercantage" class="form-control"  type="text" name="classtenpercantage" value="@if(old('classtenpercantage', null)!= null){{ old('classtenpercantage') }}@elseif(!empty($user)){{$user->classten_percentage}}@endif" />
                                    @if ($errors->has('classtenpercantage'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('classtenpercantage') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                            <div class="col-md-12 form-group{{ $errors->has('passing_year_10') ? ' has-error' : '' }}">
                                <h4><label for="name" class="col-md-6 control-label"><b>*&nbsp;</b>Class 10 Passing Year</label></h4>
                                <div class="col-md-5">
                                    <input class="form-control" maxlength="4" placeholder="YYYY"  type="text" name="passing_year_10" value="@if(old('passing_year_10', null)!= null){{ old('passing_year_10') }}@elseif(!empty($user)){{$user->passing_year_10}}@endif" />
                                    @if ($errors->has('passing_year_10'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('passing_year_10') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>
                    </div>





                    <div class="panel panel-default">
                        <div id="heading" align="center" class="panel-heading"><h4>Academic Information Details(10+2<sup>th</sup>)</h4></div>
                        <div class="panel-body">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('vocational') ? ' has-error' : '' }}">
                                    <h4><label for="name" class="col-md-4 control-label"><b>*&nbsp;</b>Vocational</label></h4>

                                    <div class="col-md-5">
                                        <select class="form-control" name="vocational" id="vocational">
                                            <option value="">Select</option>
                                            <option value="No" @if(old('vocational')=='No') selected @elseif(!empty($user) && $user->vocational == 'No') selected @endif>No</option>
                                            <option value="Yes" @if(old('vocational')=='Yes') selected @elseif(!empty($user) && $user->vocational == 'Yes') selected @endif>Yes</option>
                                        </select>

                                        @if ($errors->has('vocational'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('vocational') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="col-md-6 form-group{{ $errors->has('syllabus') ? ' has-error' : '' }}" id="syllabus_div">
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row" id="board">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-md-6 form-group{{ $errors->has('board_name') ? ' has-error' : '' }}">
                                <h4><label for="name" class="col-md-4 control-label"><b>*&nbsp;</b>Board</label></h4>
                                <div class="col-md-5">
                                    <select name="board_name" class="form-control">
                                        <option value="">Select</option>
                                        <option value="WBCHSE" @if(old('board_name')=='WBCHSE') selected @elseif(!empty($user) && $user->board_name == 'WBCHSE') selected @endif>WBCHSE</option>
                                        <option value="WBSCVE & T" @if(old('board_name')=='WBSCVE & T') selected @elseif(!empty($user) && $user->board_name == 'WBSCVE & T') selected @endif>WBSCVE &amp; T</option>
                                        <option value="Other Board" @if(old('board_name')=='Other Board') selected @elseif(!empty($user) && $user->board_name == 'Other Board') selected @endif>Other Board</option>
                                    </select>
                                    @if ($errors->has('board_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('board_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 form-group{{ $errors->has('passing_year') ? ' has-error' : '' }}">
                                <h4><label for="name" class="col-md-4 control-label"><b>*&nbsp;</b>Passing Year</label></h4>
                                <div class="col-md-5">
                                    <input class="form-control" maxlength="4" placeholder="YYYY"  type="text" name="passing_year" value="@if(old('passing_year', null)!= null){{ old('passing_year') }}@elseif(!empty($user)){{$user->passing_year}}@endif" />
                                    @if ($errors->has('passing_year'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('passing_year') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="education">
                </div>
                <div class="row" style="margin-bottom: 30px">
                    <div class="col-md-8">
                    </div>
                    <div class="col-md-2">
                        <a class="btn btn-success" href="{{url('student/home')}}">Back to Dashboard</a>
                    </div>
                    <div class="col-md-2">
                        <input type="submit" value="Save & Proceed" class='btn btn-primary'>
                    </div>
                </div>
            </div>
        </form>
    </div>


    <div id="tmpdata" class="hidden">
        {{-- Syllabus OLD or NEW --}}
        {{-- <div class="row" id="syllabus_row">
            <div class="form-group">
                <h4><label for="name" class="col-md-4 control-label"><b>*&nbsp;</b>Syllabus</label></h4>
                <div class="col-md-5">
                    <select class="form-control" name="syllabus" id="syllabus">
                        <option value="">Select</option>
                        <option value="new" @if(old('syllabus')=='new') selected @elseif(!empty($user) && $user->syllabus == 'new') selected @endif>New</option>
                        <option value="old" @if(old('syllabus')=='old') selected @elseif(!empty($user) && $user->syllabus == 'old') selected @endif>Old</option>
                    </select>

                    @if ($errors->has('syllabus'))
                        <span class="help-block">
                        <strong>{{ $errors->first('syllabus') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div> --}}
        {{-- NON VOCATIONAL SUBJECT WISE DETAILS --}}
        <div name="non_voc">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">
                        <h4>Subject Wise Marks Details</h4>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table align="center">
                                <tr>
                                    <th></th>
                                    <th>(A) Physics</th>
                                    <th>(B) Chemistry</th>
                                    <th>(C) Biology</th>
                                    <th>(D) English</th>
                                    <th>Total(A+B+C+D)</th>
                                    <th>Percentage</th>
                                </tr>
                                <tr>
                                    <td align="center"><b>*&nbsp;</b>Full Marks &nbsp;&nbsp;&nbsp;</td>
                                    <td class="{{ $errors->has('fm1') ? ' has-error' : '' }}"><input class="form-control" maxlength="3" type="number" name="fm1" value="@if(old('fm1', null)!= null){{ old('fm1') }}@elseif(!empty($user)){{$user->fm1}}@endif" placeholder="Full Marks" /></td>
                                    <td class="{{ $errors->has('fm2') ? ' has-error' : '' }}"><input class="form-control" maxlength="3" type="number" name="fm2" value="@if(old('fm2', null)!= null){{ old('fm2') }}@elseif(!empty($user)){{$user->fm2}}@endif" placeholder="Full Marks" /></td>
                                    <td class="{{ $errors->has('fm3') ? ' has-error' : '' }}"><input class="form-control" maxlength="3" type="number" name="fm3" value="@if(old('fm3', null)!= null){{ old('fm3') }}@elseif(!empty($user)){{$user->fm3}}@endif" placeholder="Full Marks" /></td>
                                    <td class="{{ $errors->has('fm4') ? ' has-error' : '' }}"><input class="form-control" maxlength="3" type="number" name="fm4" value="@if(old('fm4', null)!= null){{ old('fm4') }}@elseif(!empty($user)){{$user->fm4}}@endif" placeholder="Full Marks" /></td>
                                    <td class="{{ $errors->has('total_fullmarks') ? ' has-error' : '' }}"><input class="form-control" id="total"  maxlength="3" type="number" name="total_fullmarks" value="@if(old('total_fullmarks', null)!= null){{ old('total_fullmarks') }}@elseif(!empty($user)){{$user->total_fullmarks}}@endif" readonly/></td>
                                    <td><input class="form-control" type="text" value="Out of 100" readonly /></td>

                                </tr>

                                <tr>
                                    <td align="center"><b>*&nbsp;</b>Obained Marks &nbsp;&nbsp;&nbsp;</td>
                                    <td class="{{ $errors->has('mrk1') ? ' has-error' : '' }}"><input class="form-control" maxlength="3" type="number" name="mrk1" value="@if(old('mrk1', null)!= null){{ old('mrk1') }}@elseif(!empty($user)){{$user->mrk1}}@endif"  /></td>
                                    <td class="{{ $errors->has('mrk2') ? ' has-error' : '' }}"><input class="form-control" maxlength="3" type="number" name="mrk2" value="@if(old('mrk2', null)!= null){{ old('mrk2') }}@elseif(!empty($user)){{$user->mrk2}}@endif"  /></td>
                                    <td class="{{ $errors->has('mrk3') ? ' has-error' : '' }}"><input class="form-control" maxlength="3" type="number" name="mrk3" value="@if(old('mrk3', null)!= null){{ old('mrk3') }}@elseif(!empty($user)){{$user->mrk3}}@endif"  /></td>
                                    <td class="{{ $errors->has('mrk4') ? ' has-error' : '' }}"><input class="form-control" maxlength="3" type="number" name="mrk4" value="@if(old('mrk4', null)!= null){{ old('mrk4') }}@elseif(!empty($user)){{$user->mrk4}}@endif"  /></td>
                                    <td class="{{ $errors->has('total_mark') ? ' has-error' : '' }}"><input class="form-control" maxlength="3" type="number" name="total_mark" value="@if(old('total_mark', null)!= null){{ old('total_mark') }}@elseif(!empty($user)){{$user->total_mark}}@endif" readonly/></td>
                                    <td class="{{ $errors->has('percentage') ? ' has-error' : '' }}"><input class="form-control" type="text" name="percentage" maxlength="5" value="@if(old('percentage', null)!= null){{ old('percentage') }}@elseif(!empty($user)){{$user->percentage}}@endif" readonly  ></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- NEW VOCATIONAL SUBJECT WISE DETAILS --}}
        <div name="voc_new">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">
                        <h4>Subject Wise Marks Details(New Vocational)</h4>
                        <h5><b>Subject Code : (PPFV/POFR/POVG/HNMG/FMAP/CNMG/SEPR/PHMG/POFC)</b></h5>
                        <h5>(Full Marks must be 100 for every subject)</h5>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table align="center">
                                <tr>
                                    <th></th>
                                    <th>English<br/></th>
                                    <th>Biology<br/></th>
                                    <th>Physics/Chemistry
                                        <select class="form-control" name="science" id="science">
                                            <option value="">Select</option>
                                            <option value="Physics" @if(old('science')=='Physics') selected @elseif(!empty($user) && $user->science == 'Physics') selected @endif>Physics</option>
                                            <option value="Chemistry" @if(old('science')=='Chemistry') selected @elseif(!empty($user) && $user->science == 'Chemistry') selected @endif>Chemistry</option>
                                        </select>
                                        <div class="form-group{{ $errors->has('science') ? ' has-error' : '' }}">
                                        @if ($errors->has('science'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('science') }}</strong>
                                            </span>
                                        @endif
                                        </div>
                                    </th>
                                    <th>Subject Code
                                        <select class="form-control" name="paper1" id="paper1">
                                            <option value="">Select</option>
                                            <option value="PPFV" @if(old('paper1')=='PPFV') selected @elseif(!empty($user) && $user->paper1 == 'PPFV') selected @endif>PPFV</option>
                                            <option value="POFR" @if(old('paper1')=='POFR') selected @elseif(!empty($user) && $user->paper1 == 'POFR') selected @endif>POFR</option>
                                            <option value="POVG" @if(old('paper1')=='POVG') selected @elseif(!empty($user) && $user->paper1 == 'POVG') selected @endif>POVG</option>
                                            <option value="HNMG" @if(old('paper1')=='HNMG') selected @elseif(!empty($user) && $user->paper1 == 'HNMG') selected @endif>HNMG</option>
                                            <option value="FMAP" @if(old('paper1')=='FMAP') selected @elseif(!empty($user) && $user->paper1 == 'FMAP') selected @endif>FMAP</option>
                                            <option value="CNMG" @if(old('paper1')=='CNMG') selected @elseif(!empty($user) && $user->paper1 == 'CNMG') selected @endif>CNMG</option>
                                            <option value="SEPR" @if(old('paper1')=='SEPR') selected @elseif(!empty($user) && $user->paper1 == 'SEPR') selected @endif>SEPR</option>
                                            <option value="PHMG" @if(old('paper1')=='PHMG') selected @elseif(!empty($user) && $user->paper1 == 'PHMG') selected @endif>PHMG</option>
                                            <option value="POFC" @if(old('paper1')=='POFC') selected @elseif(!empty($user) && $user->paper1 == 'POFC') selected @endif>POFC</option>

                                        </select>
                                        <div class="form-group{{ $errors->has('paper1') ? ' has-error' : '' }}">
                                        @if ($errors->has('paper1'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('paper1') }}</strong>
                                            </span>
                                        @endif
                                        </div>
                                    </th>
                                    <th>Subject Code
                                        <select class="form-control" name="paper2" id="paper2">
                                            <option value="">Select</option>
                                            <option value="PPFV" @if(old('paper2')=='PPFV') selected @elseif(!empty($user) && $user->paper2 == 'PPFV') selected @endif>PPFV</option>
                                            <option value="POFR" @if(old('paper2')=='POFR') selected @elseif(!empty($user) && $user->paper2 == 'POFR') selected @endif>POFR</option>
                                            <option value="POVG" @if(old('paper2')=='POVG') selected @elseif(!empty($user) && $user->paper2 == 'POVG') selected @endif>POVG</option>
                                            <option value="HNMG" @if(old('paper2')=='HNMG') selected @elseif(!empty($user) && $user->paper2 == 'HNMG') selected @endif>HNMG</option>
                                            <option value="FMAP" @if(old('paper2')=='FMAP') selected @elseif(!empty($user) && $user->paper2 == 'FMAP') selected @endif>FMAP</option>
                                            <option value="CNMG" @if(old('paper2')=='CNMG') selected @elseif(!empty($user) && $user->paper2 == 'CNMG') selected @endif>CNMG</option>
                                            <option value="SEPR" @if(old('paper2')=='SEPR') selected @elseif(!empty($user) && $user->paper2 == 'SEPR') selected @endif>SEPR</option>
                                            <option value="PHMG" @if(old('paper2')=='PHMG') selected @elseif(!empty($user) && $user->paper2 == 'PHMG') selected @endif>PHMG</option>
                                            <option value="POFC" @if(old('paper2')=='POFC') selected @elseif(!empty($user) && $user->paper2 == 'POFC') selected @endif>POFC</option>
                                        </select>
                                        <div class="form-group{{ $errors->has('paper2') ? ' has-error' : '' }}">
                                        @if ($errors->has('paper2'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('paper2') }}</strong>
                                            </span>
                                        @endif
                                        </div>
                                    </th>
                                    
                                    <th>Total</th>
                                    <th>Percentage</th>
                                </tr>
                                <tr>
                                    <td align="center"><b>*&nbsp;</b>Full Marks&nbsp;&nbsp;&nbsp;</td>
                                    <td class="{{ $errors->has('fm1') ? ' has-error' : '' }}"><input class="form-control" maxlength="3" type="number" name="fm1" value="@if(old('fm1', null)!= null){{ old('fm1') }}@elseif(!empty($user)){{$user->fm1}}@endif"  placeholder="Full Marks"/></td>
                                    <td class="{{ $errors->has('fm2') ? ' has-error' : '' }}"><input class="form-control" maxlength="3" type="number" name="fm2" value="@if(old('fm2', null)!= null){{ old('fm2') }}@elseif(!empty($user)){{$user->fm2}}@endif"  placeholder="Full Marks"/></td>
                                    <td class="{{ $errors->has('fm3') ? ' has-error' : '' }}"><input class="form-control" maxlength="3" type="number" name="fm3" value="@if(old('fm3', null)!= null){{ old('fm3') }}@elseif(!empty($user)){{$user->fm3}}@endif"  placeholder="Full Marks"/></td>
                                    <td class="{{ $errors->has('fm4') ? ' has-error' : '' }}"><input class="form-control" maxlength="3" type="number" name="fm4" value="@if(old('fm4', null)!= null){{ old('fm4') }}@elseif(!empty($user)){{$user->fm4}}@endif"  placeholder="Full Marks"/></td>
                                    <td><input class="form-control" maxlength="3" type="number" name="fm5" value="@if(!empty($user)){{$user->fm5}}@endif"  placeholder="Full Marks"/></td>
                                    <td><input class="form-control" id="total"  maxlength="3" type="number" name="total_fullmarks" value="@if(!empty($user)){{$user->total_fullmarks}}@endif" readonly/></td>
                                    <td><input class="form-control" type="text" value="Out of 100" readonly /></td>

                                </tr>

                                <tr>
                                    <td align="center"><b>*&nbsp;</b>Obtained Marks&nbsp;&nbsp;&nbsp;</td>
                                    <td><input class="form-control" maxlength="3" type="number" name="mrk1" value="@if(!empty($user)){{$user->mrk1}}@endif"  /></td>
                                    <td><input class="form-control" maxlength="3" type="number" name="mrk2" value="@if(!empty($user)){{$user->mrk2}}@endif"  /></td>
                                    <td><input class="form-control" maxlength="3" type="number" name="mrk3" value="@if(!empty($user)){{$user->mrk3}}@endif"  /></td>
                                    <td><input class="form-control" maxlength="3" type="number" name="mrk4" value="@if(!empty($user)){{$user->mrk4}}@endif"  /></td>
                                    <td><input class="form-control" maxlength="3" type="number" name="mrk5" value="@if(!empty($user)){{$user->mrk5}}@endif"  /></td>
                                    <td><input class="form-control" maxlength="3" type="number" name="total_mark" value="@if(!empty($user)){{$user->total_mark}}@endif" readonly /></td>
                                    <td rowspan="1"><input class="form-control" type="text" name="percentage" maxlength="5" value="@if(!empty($user)){{$user->percentage}}@endif" readonly  ></td>

                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- OLD VOCATIONAL SUBJECT WISE DETAILS --}}
        <div name="voc_old">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">
                        <h4>Subject Wise Marks Details(Old Vocational)</h4>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table align="center">
                                <tr>
                                    <th></th>
                                    <th>CEN2<br/>(Theo+Prac)</th>
                                    <th>AHP2<br/>(Theo+Prac)</th>
                                    <th>AIEM<br/>(Theo+Prac)</th>
                                    <th>VOC. Paper-I<br/>Any one of<br>PPFV/PLPT/HNMG/CAVC<br/>(Theo+Prac)</th>
                                    <th>VOC. Paper-II<br/>Any one of<br/>CFAV/MRCL/MAAP/FRCL<br/>(Theo+Prac)</th>
                                    <th>Total</th>
                                    <th>Percentage</th>
                                </tr>
                                <tr>
                                    <td align="center"><b>*&nbsp;</b>Full Marks&nbsp;&nbsp;&nbsp;</td>
                                    <td><input class="form-control" maxlength="3" type="number" name="fm1" value="@if(!empty($user)){{$user->fm1}}@endif" placeholder="Full Marks" /></td>
                                    <td><input class="form-control" maxlength="3" type="number" name="fm2" value="@if(!empty($user)){{$user->fm2}}@endif" placeholder="Full Marks" /></td>
                                    <td><input class="form-control" maxlength="3" type="number" name="fm3" value="@if(!empty($user)){{$user->fm3}}@endif" placeholder="Full Marks" /></td>
                                    <td><input class="form-control" maxlength="3" type="number" name="fm4" value="@if(!empty($user)){{$user->fm4}}@endif" placeholder="Full Marks" /></td>
                                    <td><input class="form-control" maxlength="3" type="number" name="fm5" value="@if(!empty($user)){{$user->fm5}}@endif" placeholder="Full Marks" /></td>
                                    <td><input class="form-control" id="total"  maxlength="3" type="number" name="total_fullmarks" value="@if(!empty($user)){{$user->total_fullmarks}}@endif" readonly /></td>
                                    <td><input class="form-control" type="text" value="Out of 100" readonly /></td>

                                </tr>

                                <tr>
                                    <td align="center"><b>*&nbsp;</b>Obained Marks&nbsp;&nbsp;&nbsp;</td>
                                    <td><input class="form-control" maxlength="3" type="number" name="mrk1" value="@if(!empty($user)){{$user->mrk1}}@endif"  /></td>
                                    <td><input class="form-control" maxlength="3" type="number" name="mrk2" value="@if(!empty($user)){{$user->mrk2}}@endif"  /></td>
                                    <td><input class="form-control" maxlength="3" type="number" name="mrk3" value="@if(!empty($user)){{$user->mrk3}}@endif"  /></td>
                                    <td><input class="form-control" maxlength="3" type="number" name="mrk4" value="@if(!empty($user)){{$user->mrk4}}@endif"  /></td>
                                    <td><input class="form-control" maxlength="3" type="number" name="mrk5" value="@if(!empty($user)){{$user->mrk5}}@endif"  /></td>
                                    <td><input class="form-control" maxlength="3" type="number" name="total_mark" value="@if(!empty($user)){{$user->total_mark}}@endif"  readonly/></td>
                                    <td rowspan="1"><input class="form-control" type="text" name="percentage" maxlength="5" value="@if(!empty($user)){{$user->percentage}}@endif" readonly  ></td>

                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <style type="text/css">
        table, td, th {
            border: 1px solid #d3e0e9;
            padding: 5px;
        }
        b {
            color: red;
            font-size: 16px;
        }
    </style>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function(){
            autochangeVocational();
            autochangeClassTenOtherboard();
            
        });
        
        $('#vocational').on('change',function(){
            manualchangeVocational();
        });

        $('#board_name_classten').on('change',function(){
            if($('#board_name_classten').val() == 'Other Board') {
                 $('#specify_other_board').show('slow/400/fast');
                
            }else {
                $('#specify_other_board').hide('slow/400/fast');
            }
        });

        function autochangeClassTenOtherboard(){
            if($('#board_name_classten').val() == 'Other Board') {
                $('#specify_other_board').show('slow/400/fast');
            }else {
                $('#specify_other_board').hide('slow/400/fast');
            }
        }

        function autochangeVocational(){
            if($('#vocational').val() == 'Yes') {
                $('#education').empty();
                $('#education').html($('#tmpdata div[name=voc_new] > div').html());
                $('#education,#board').show('slow/400/fast');


                //$('#education,#board').hide('slow/400/fast');
                // $('#syllabus_div').html($('#tmpdata div#syllabus_row > div').html());
                // $('#syllabus_div').show('slow/400/fast');  
                $("select[name='board_name'] option").show();
                $("select[name='board_name'] option[value='WBCHSE']").hide();
                $("select[name='board_name'] option[value='Other Board']").hide();
            }else if($('#vocational').val()=='No') {

               // $('#syllabus_div').empty($('#tmpdata div#syllabus_row > div').html());
                $('#education').empty();
                $('#education').html($('#tmpdata div[name=non_voc] > div').html());
                $('#education,#board').show('slow/400/fast');
                $("select[name='board_name'] option").show();
                $("select[name='board_name'] option[value='WBSCVE & T']").hide();
            }else {
                $('#education,#board').hide('slow/400/fast');
            }
        }

        function manualchangeVocational(){
                $("select[name='board_name'] option[value='WBCHSE']").remove();
                $("select[name='board_name'] option[value='Other Board']").remove();
                $("select[name='board_name'] option[value='WBSCVE & T']").remove();

                $("input[name='fm1']").removeAttr('value');
                $("input[name='fm2']").removeAttr('value');
                $("input[name='fm3']").removeAttr('value');
                $("input[name='fm4']").removeAttr('value');
                $("input[name='fm5']").removeAttr('value');
                $("input[name='mrk1']").removeAttr('value');
                $("input[name='mrk2']").removeAttr('value');
                $("input[name='mrk3']").removeAttr('value');
                $("input[name='mrk4']").removeAttr('value');
                $("input[name='mrk5']").removeAttr('value');
                $("input[name='total_fullmarks']").removeAttr('value');
                $("input[name='total_mark']").removeAttr('value');
                $("input[name='percentage']").removeAttr('value');

            if($('#vocational').val() == 'Yes') {
                $('#education').empty();
                $('#education').html($('#tmpdata div[name=voc_new] > div').html());
                $('#education,#board').show('slow/400/fast');


                //$('#education,#board').hide('slow/400/fast');
                // $('#syllabus_div').html($('#tmpdata div#syllabus_row > div').html());
                // $('#syllabus_div').show('slow/400/fast');  
                $("select[name='board_name']").append(new Option("WBSCVE & T", "WBSCVE & T"));
                
            }else if($('#vocational').val()=='No') {

                //$('#syllabus_div').empty($('#tmpdata div#syllabus_row > div').html());
                $('#education').empty();
                $('#education').html($('#tmpdata div[name=non_voc] > div').html());
                $('#education,#board').show('slow/400/fast');
                $("select[name='board_name'] option").show();
                $("select[name='board_name']").append(new Option("WBCHSE", "WBCHSE"));
                $("select[name='board_name']").append(new Option("Other Board", "Other Board"));
            }else {
                $('#education,#board').hide('slow/400/fast');
            }
        }





        $('body').on('keyup', '#class10fm', function (e) {
            var class10_fm = $('#class10fm').val();
            var class10_mrk = $('#class10mrk').val();



            var percent = ((class10_mrk * 100)/class10_fm).toFixed(3);

            console.log(percent);


            values=percent.toString().split('.');
            one=values[0];
            two=values[1];

        if (two != " ") {
           var subStr = two.substring(0, 2);
             console.log(one);
             console.log(subStr);

            var res = one+ '.' +subStr;
        }

        else {
            var res = one;
        }
   

            $('#classtenpercantage').val(res);
             

        });

        $('body').on('keyup', '#class10mrk', function (e) {
           var class10_fm = $('#class10fm').val();
            var class10_mrk = $('#class10mrk').val();
            //var percent = (class10_mrk/class10_fm*100);



            var percent = ((class10_mrk * 100)/class10_fm).toFixed(3);
            console.log(percent);


           values=percent.toString().split('.');
            one=values[0];
            two=values[1];

        if (two != " ") {
           var subStr = two.substring(0, 2);
             console.log(one);
             console.log(subStr);

            var res = one+ '.' +subStr;
        }

        else {
            var res = one;
        }

            $('#classtenpercantage').val(res);


        });






        $('body').on('keyup', 'form input[name^=fm]', function (e) {
            var total_fullmarks = 0, total_fullmarks;
            $('form input[name^=fm]').each(function () {
                total_fullmarks += Number(this.value);
            });
            $('form input[name=total_fullmarks]').val(total_fullmarks);


        });

        $('body').on('keyup', 'form input[name^=mrk]', function (e) {
            var total_mark = 0, total_mark;
            $('form input[name^=mrk]').each(function () {
                total_mark += Number(this.value);
            });
            $('form input[name=total_mark]').val(total_mark);


        });

        $('body').on('keyup', 'form input[name^=mrk]', function (e) {
            var total_mark = $('form input[name=total_mark]').val();
            var total_fullmarks = $('form input[name=total_fullmarks]').val();
            var percent = Number(total_mark/total_fullmarks*100).toFixed(2);
            $('form input[name=percentage]').val(percent);

        });

        $('body').on('keyup', 'form input[name^=fm]', function (e) {
            var total_mark = $('form input[name=total_mark]').val();
            var total_fullmarks = $('form input[name=total_fullmarks]').val();
            var percent = Number(total_mark/total_fullmarks*100).toFixed(2);
            $('form input[name=percentage]').val(percent);

        });
        /*for blur also*/
        $('body').on('blur', 'form input[name^=fm]', function (e) {
            var total_fullmarks = 0, total_fullmarks;
            $('form input[name^=fm]').each(function () {
                total_fullmarks += Number(this.value);
            });
            $('form input[name=total_fullmarks]').val(total_fullmarks);


        });

        $('body').on('blur', 'form input[name^=mrk]', function (e) {
            var total_mark = 0, total_mark;
            $('form input[name^=mrk]').each(function () {
                total_mark += Number(this.value);
            });
            $('form input[name=total_mark]').val(total_mark);


        });

        $('body').on('blur', 'form input[name^=mrk]', function (e) {
            var total_mark = $('form input[name=total_mark]').val();
            var total_fullmarks = $('form input[name=total_fullmarks]').val();
            var percent = Number(total_mark/total_fullmarks*100).toFixed(2);
            $('form input[name=percentage]').val(percent);

        });

        $('body').on('blur', 'form input[name^=fm]', function (e) {
            var total_mark = $('form input[name=total_mark]').val();
            var total_fullmarks = $('form input[name=total_fullmarks]').val();
            var percent = Number(total_mark/total_fullmarks*100).toFixed(2);
            $('form input[name=percentage]').val(percent);

        });


        /*
        if User=> vocational, trigger change function and value set to Yes
        if User=> non-vocational, trigger change function and value set to NO
        */

        /*
        if User=> SYLLABUS=>NEW, trigger change function and value set to NEW
        if User=> SYLLABUS=>OLD, trigger change function and value set to OLD
        */
        

    </script>

@endsection
