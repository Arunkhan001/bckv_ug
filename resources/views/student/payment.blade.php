@extends('student.layout.auth')

@section('content')
<form class="form-horizontal" role="form" method="POST" action="{{ url('/student/payment') }}">
     {{ csrf_field() }}
     <input type="hidden" name="t_amount" value="{{ $user->amount }}">
<div class="container">
    <div class="row">
        @if (session('alert'))
            <div class="alert alert-success">
                {{ session('alert') }}
            </div>
        @endif
        <div style="min-height: 400px;" class="panel panel-default">
            <div id="heading" align="center" class="panel-heading"><h4>Make Payment</h4></div>
            <div class="panel-body">
                <div class="col-md-12">
                    <div class="form-group{{ $errors->has('payment_mode') ? ' has-error' : '' }}">
                        <label for="payment_mode" class="col-md-4 control-label"><b>*&nbsp;</b>Payment Mode : </label>
                        @if(Auth::user()->isActive())
                        <div class="col-md-6">
                            <label class="radio-inline"><input id="payment_mode" type="radio" name="payment_mode" value="Online" checked>Online</label>
                            {{-- <label class="radio-inline"><input id="payment_mode" type="radio"  name="payment_mode" value="Offline">Offline</label> --}}
                            @if ($errors->has('payment_mode'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('payment_mode') }}</strong>
                                </span>
                            @endif
                        </div>
                        @elseif(Auth::user()->isPayment())
                        <div class="col-md-6">
                            <label class="radio-inline"><input id="payment_mode" type="radio"  name="payment_mode" value="Offline" checked>Offline</label>
                            @if ($errors->has('payment_mode'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('payment_mode') }}</strong>
                                </span>
                            @endif
                        </div>
                        @endif
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-4 control-label"><b>*&nbsp;</b>Amount : </label>
                        <div class="col-md-6">
                            <label class="radio-inline">₹{{ $user->amount }} + Transaction Fees</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-4 control-label"><b>*&nbsp;</b>Payment Status : </label>
                        <div class="col-md-6">
                            <label class="radio-inline">PENDING
                            {{-- @if($user->status == 6)
                            {{ 'SUCCESS' }}
                            @else
                            {{ 'PENDING' }}
                            @endif --}}
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-4 control-label"></label>
                        <div class="col-md-6">
                            <label class="radio-inline">
                           <input type="submit" value="Make Payment" class='btn btn-danger'> 
                            </label>
                        </div>
                    </div>
                </div>
                @if (session('error'))
                <div class="col-md-12">
                    <div class="alert alert-danger">
                        <h4>ERROR</h4>
                        <h4>PAYMENT IS NOT SUCCESSFUL</h4>
                        {{ session('error') }}
                    </div>
                </div>
                @endif
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-10">
        </div>
        <div class="col-md-2">
            <a type="button" class="btn btn-success" href="{{url('student/home')}}">Back to Dashboard</a>
        </div>
    </div>
</div>
</form>
@endsection
