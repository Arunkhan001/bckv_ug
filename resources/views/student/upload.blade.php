@extends('student.layout.auth')

@section('content')
<div class="container">
    <div align="center" class="row">

        @if (session('alert'))
            <div class="alert alert-success">
                {{ session('alert') }}
            </div>
        @endif
        
        <div class="col-md-12">
            <div class="panel panel-default">
                <div id="heading" class="panel-heading">
                @if($data->category != 'General')
                <h4>Upload Scan Copy of Candidate's Photo, Signature, Self Attested Birth Certificate, Self Attested Marksheet,Self Attested Aadhaar, Self Attested {{ $data->category=='Physically Challange'?'Disability': $data->category }} Certificate <br> [<b> Image file only</b> ]</h4>
                @else
                <h4>Upload Scan Copy of Candidate's Photo, Signature,Self Attested Birth Certificate,Self Attested Marksheet,Self Attested Aadhaar, [<b> Image file only</b> ]</h4>
                @endif
                </div>

                <div style="min-height: 400px;" class="panel-body">
                <form enctype="multipart/form-data" method="post" action="{{ url('student/upload') }}" role="from">
                            {{ csrf_field() }}
                    <div align="center" class="row">
                        <div class="col-md-8">
                            <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label"><b>*&nbsp;</b>Upload Photo</label>
                                <div class="col-md-6">
                                    <p id="red">Photo Size must be between 10-150Kb</p>
                                    <input class="form-control" name="photo" type="file" id="photo"  accept="image/x-png,image/jpeg,image/jpg">
                                    @if ($errors->has('photo'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('photo') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <span style="height:200px; width: 200px;" id="preview_photo">
                            @if($img_flag == 1)
                                <img style="height:200px; width: 200px;" src="{{ url('student/image/'.$app_no.'/photo.jpg') }}">
                            @endif
                            </span>
                        </div>
                    </div><hr/>
                    <div align="center" class="row">
                        <div class="col-md-8">
                             <div class="form-group{{ $errors->has('signature') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label"><b>*&nbsp;</b>Upload Signature</label>
                                <div class="col-md-6">
                                <p id="red">Signature Size must be between 5-70Kb</p>
                                <input class="form-control" name="signature" type="file" id="signature"  accept="image/x-png,image/jpeg,image/jpg">
                                @if ($errors->has('signature'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('signature') }}</strong>
                                    </span>
                                @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <span style="height:300px; width: 200px;" id="preview_signature">
                                @if($img_flag == 1)
                                <img style="height:50px; width: 200px;" src="{{ url('student/image/'.$app_no.'/signature.jpg') }}">
                            @endif
                            </span>
                        </div>
                    </div><hr/>
                    <div align="center" class="row">
                        <div class="col-md-8">
                             <div class="form-group{{ $errors->has('birth_certificate') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label"><b>*&nbsp;</b>Upload Birth Certificate/Certificate of Secondary Examination<b> [ Self Attested ]</b></label>
                                <div class="col-md-6">
                                <p id="red">Birth Certificate Size must be less than 300Kb</p>
                                <input class="form-control" name="birth_certificate" type="file" id="birth_certificate"  accept="image/x-png,image/jpeg,image/jpg">
                                @if ($errors->has('birth_certificate'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('birth_certificate') }}</strong>
                                    </span>
                                @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <span style="height:300px; width: 200px;" id="preview_birth_certificate">
                                @if($img_flag == 1)
                                <img style="height:200px; width: 200px;" src="{{ url('student/image/'.$app_no.'/birth_certificate.jpg') }}">
                            @endif
                            </span>
                        </div>
                    </div><hr/>


                    <div align="center" class="row">
                        <div class="col-md-8">
                             <div class="form-group{{ $errors->has('marksheetclasstenfont') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label"><b>*&nbsp;</b>Upload 10<sup>th</sup> Board Marksheet Front Side<br/><b> [ Self Attested ]</b></label>
                                <div class="col-md-6">
                                <p id="red">Marksheet Front side Size must be less than 300kb</p>
                                <input class="form-control" name="marksheetclasstenfont" type="file" id="marksheetclasstenfont" accept="image/x-png,image/jpeg,image/jpg">
                                @if ($errors->has('marksheetclasstenfont'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('marksheetclasstenfont') }}</strong>
                                    </span>
                                @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <span style="height:300px; width: 200px;" id="preview_marksheet_ten_font">
                                @if($img_flag == 1)
                                <img style="height:200px; width: 200px;" src="{{ url('student/image/'.$app_no.'/10th_marksheet_font.jpg') }}">
                            @endif
                            </span>
                        </div>
                    </div><hr/>


                    <div align="center" class="row">
                        <div class="col-md-8">
                             <div class="form-group{{ $errors->has('marksheetclasstenback') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label"><b>*&nbsp;</b>Upload 10<sup>th</sup> Board Marksheet Back Side<br/><b> [ Self Attested ]</b></label>
                                <div class="col-md-6">
                                <p id="red">Marksheet Back side Size must be less than 300kb</p>
                                <input class="form-control" name="marksheetclasstenback" type="file" id="marksheetclasstenback" accept="image/x-png,image/jpeg,image/jpg">
                                @if ($errors->has('marksheetclasstenback'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('marksheetclasstenback') }}</strong>
                                    </span>
                                @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <span style="height:300px; width: 200px;" id="preview_marksheet_ten_back">
                                @if($img_flag == 1)
                                <img style="height:200px; width: 200px;" src="{{ url('student/image/'.$app_no.'/10th_marksheet_back.jpg') }}">
                            @endif
                            </span>
                        </div>
                    </div><hr/>







                    <div align="center" class="row">
                        <div class="col-md-8">
                             <div class="form-group{{ $errors->has('marksheet') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label"><b>*&nbsp;</b>Upload 10+2<sup>th</sup> Board Marksheet Front Side<br/><b> [ Self Attested ]</b></label>
                                <div class="col-md-6">
                                <p id="red">Marksheet Front side Size must be less than 300kb</p>
                                <input class="form-control" name="marksheet" type="file" id="marksheet" accept="image/x-png,image/jpeg,image/jpg">
                                @if ($errors->has('marksheet'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('marksheet') }}</strong>
                                    </span>
                                @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <span style="height:300px; width: 200px;" id="preview_marksheet">
                                @if($img_flag == 1)
                                <img style="height:200px; width: 200px;" src="{{ url('student/image/'.$app_no.'/marksheet.jpg') }}">
                            @endif
                            </span>
                        </div>
                    </div><hr/>




                    <div align="center" class="row">
                        <div class="col-md-8">
                             <div class="form-group{{ $errors->has('marksheet_back') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label"><b>*&nbsp;</b>Upload 10+2<sup>th</sup> Board Marksheet Back Side<br/><b> [ Self Attested ]</b></label>
                                <div class="col-md-6">
                                <p id="red">Marksheet Back side Size must be less than 300kb</p>
                                <input class="form-control" name="marksheet_back" type="file" id="marksheet_back" accept="image/x-png,image/jpeg,image/jpg">
                                @if ($errors->has('marksheet_back'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('marksheet_back') }}</strong>
                                    </span>
                                @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <span style="height:300px; width: 200px;" id="preview_marksheet_back">
                                @if($img_flag == 1)
                                <img style="height:200px; width: 200px;" src="{{ url('student/image/'.$app_no.'/marksheet_back.jpg') }}">
                            @endif
                            </span>
                        </div>
                    </div><hr/>

                    @if($data->student_aadhaar_number != NULL)
                    <div align="center" class="row">
                        <div class="col-md-8">
                             <div class="form-group{{ $errors->has('aadhaar') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label"><b>*&nbsp;</b>Upload Aadhaar Card<br/><b> [ Self Attested ]</b></label>
                                <div class="col-md-6">
                                <p id="red">Aadhaar Card Size must be less than 300kb</p>
                                <input class="form-control" name="aadhaar" type="file" id="aadhaar" accept="image/x-png,image/jpeg,image/jpg">
                                @if ($errors->has('aadhaar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('aadhaar') }}</strong>
                                    </span>
                                @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <span style="height:300px; width: 200px;" id="preview_aadhaar">
                                @if($img_flag == 1)
                                <img style="height:200px; width: 200px;" src="{{ url('student/image/'.$app_no.'/aadhaar.jpg') }}">
                            @endif
                            </span>
                        </div>
                    </div><hr/>
                    @endif
                    
                    @if($data->category != 'General')
                    <div align="center" class="row">
                        <div class="col-md-8">
                             <div class="form-group{{ $errors->has('certificate') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label"><b>*&nbsp;</b>Upload {{ $data->category=='Physically Challange'?'Disability': $data->category }} Certificate <br/><b> [ Self Attested ]</b></label>
                                <div class="col-md-6">
                                <p id="red">{{ $data->category=='Physically Challange'?'Disability': $data->category }} Certificate Size must be less than 300kb</p>
                                <input class="form-control" name="certificate" type="file" id="certificate"  accept="image/x-png,image/jpeg,image/jpg">
                                @if ($errors->has('certificate'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('certificate') }}</strong>
                                    </span>
                                @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <span style="height:300px; width: 200px;" id="preview_certificate">
							@if($img_flag == 1)
                                <img style="height:200px; width: 200px;" src="{{ url('student/image/'.$app_no.'/certificate.jpg') }}">
							@endif
                            </span>
                        </div>
                    </div>
                    @endif

                    {{-- FOR SPORT QUOATA --}}

                    @if($data->sport_quota == 'Yes')
                    <div align="center" class="row">
                        <div class="col-md-8">
                             <div class="form-group{{ $errors->has('sport_certificate') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label"><b>*&nbsp;</b>Upload Sport Certificate <br/><b> [ Self Attested ]</b></label>
                                <div class="col-md-6">
                                <p id="red">Sport Certificate Size must be less than 300kb</p>
                                <input class="form-control" name="sport_certificate" type="file" id="sport_certificate"  accept="image/x-png,image/jpeg,image/jpg">
                                @if ($errors->has('sport_certificate'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sport_certificate') }}</strong>
                                    </span>
                                @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <span style="height:300px; width: 200px;" id="preview_sport_certificate">
                            @if($img_flag == 1)
                                <img style="height:200px; width: 200px;" src="{{ url('student/image/'.$app_no.'/sport_certificate.jpg') }}">
                            @endif
                            </span>
                        </div>
                    </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-bottom: 30px">
        <div class="col-md-8">  
        </div>
        <div class="col-md-2">
            <a type="button" class="btn btn-success" href="{{url('student/home')}}">Back to Dashboard</a>   
        </div>
        <div class="col-md-2">
            <input type="submit" value="Save &amp; Proceed" class='btn btn-primary'> 
        </div>
    </div>
    </form>
</div>
@endsection


@section('js')
<script src="{{ asset('js/validation.js') }}"></script>
@endsection

@section('css')
    <style type="text/css">
        b {
            color: red;
            font-size: 16px;
        }
    </style>
@endsection
