<body onload="submitForm()">
	<form method="POST" action="https://pgi.billdesk.com/pgidsk/PGIMerchantPayment" name="Form">
	     {{ csrf_field() }}
	     <input type="hidden" name="msg" value="{{ $msg }}" />
	</form>
</body>
<script>
  function submitForm() {
    var Form = document.forms.Form;
    Form.submit();
  }
</script>
