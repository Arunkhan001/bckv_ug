@extends('student.layout.auth')
@section('css')
<style type="text/css">
	.challan {
	width: 100%;
}
.challan, .challan td {
	border: 1px solid #000;
}
.challan td {
	font-size: 12px;
	padding: 2px 5px;
}
.challan td > div {
	min-height: 60px;
}
.challan td > div >span {
	float: right;
	margin-top: 47px;
	font-size: 9px;
	text-transform: uppercase;
}
.challan .row-high-light {
	background: rgba(204, 186, 186, 0.28);
	font-weight: bold;
}
.challan td:first-child {
	width: 40%;
	text-transform: uppercase;
}
@media print {
.container-fluid {
	padding: 0;
}
.col-xs-6 {
	padding: 0 8px;
}
.main-wrapper {
	padding: 10px;
}
.no-print {
	display: none;
}
}

</style>
@endsection
@section('content')
<div class="container">
	<div class="row no-print">
        <div class="col-xs-12 text-center">
            <b style="color:red">Please make offline payment to your nearest {{ $bank_details->bank_name }} branch using this challan</b>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <table class="challan" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="2" style="text-align:right;text-transform:uppercase;">Bank Copy</td>
                </tr>
                <tr>
                    <td colspan="2"  style="text-align:center;font-weight:bold;font-size:14px;">{{ $bank_details->bf_name }}</td>
                </tr>
                <tr>
                    <td colspan="2"  style="text-align:center;font-weight:bold;">Bank Challan</td>
                </tr>
                <tr>
                    <td colspan="2"  style="text-align:center;text-transform:uppercase;">{{ $bank_details->bank_name }}</td>
                </tr>
                <tr>
                    <td colspan="2">Invoke Finacle Menu<br/> Option "PAYFEE" & Module -> BCKV_626</td>             
                </tr>
                <tr class="row-high-light">
                    <td>A/C No. :</td>
                    <td>{{ $bank_details->acc_no }}</td>
                </tr>
                <tr class="row-high-light">
                    <td>Invoice No. :</td>
                    <td>{{ $invoice->tid }}</td>
                </tr>
                <tr class="row-high-light">
                    <td>Application No. :</td>
                    <td>{{ $user->application_no }}</td>
                </tr>
                <tr>
                    <td>Applicant's Name :</td>
                    <td style="text-transform: uppercase;">{{ $user->name }}</td>
                </tr>
                <tr>
                    <td>Mobile No. :</td>
                    <td>{{ $user->mobile_number }}</td>
                </tr>
                <tr>
                    <td>Amount (in Rs.) :</td>
                    <td>{{ $user->amount }}</td>
                </tr>
                <tr>
                    <td>Service Charge :</td>
                    <td>46</td>
                </tr>
                <tr>
                    <td>Total Amount :</td>
                    <td>{{ $user->total_amount }}</td>
                </tr>
                <tr>
                    <td>Total amount in words :</td>
                    <td style="text-transform: uppercase;">{{ $user->total_in_word }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:left;font-weight:bold;text-transform:uppercase;">Office Use :</td>
                </tr>
                <tr>
                    <td>Transaction ID :</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Transaction Date :</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Branch SOL ID :</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Branch Name. :</td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="2"><div><span>Signature of the Depositor</span></div></td>
                </tr>
                <tr>
                    <td><div><span>Seal/Date</span></div></td>
                    <td><div><span>Authorised Signatory</span></div></td>
                </tr>
            </table>
        </div>
        <div class="col-xs-6">
            <table class="challan" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="2"  style="text-align:right;text-transform:uppercase;">Student Copy</td>
                </tr>
                <tr>
                    <td colspan="2"  style="text-align:center;font-weight:bold;font-size:14px;">{{ $bank_details->bf_name }}</td>
                </tr>
                <tr>
                    <td colspan="2"  style="text-align:center;font-weight:bold;">Bank Challan</td>
                </tr>
                <tr>
                    <td colspan="2"  style="text-align:center;text-transform:uppercase;">{{ $bank_details->bank_name }}</td>
                </tr>
                <tr>
                    <td colspan="2">Invoke Finacle Menu<br/> Option "PAYFEE" & Module -> BCKV_626</td>             
                </tr>
                <tr class="row-high-light">
                    <td>A/C No. :</td>
                    <td>{{ $bank_details->acc_no }}</td>
                </tr>
                <tr class="row-high-light">
                    <td>Invoice No. :</td>
                    <td>{{ $invoice->tid }}</td>
                </tr>
                <tr class="row-high-light">
                    <td>Application No. :</td>
                    <td>{{ $user->application_no }}</td>
                </tr>
                <tr>
                    <td>Applicant's Name :</td>
                    <td style="text-transform: uppercase;">{{ $user->name }}</td>
                </tr>
                <tr>
                    <td>Mobile No. :</td>
                    <td>{{ $user->mobile_number }}</td>
                </tr>
                <tr>
                    <td>Amount (in Rs.) :</td>
                    <td>{{ $user->amount }}</td>
                </tr>
                <tr>
                    <td>Service Charge :</td>
                    <td>46</td>
                </tr>
                <tr>
                    <td>Total Amount :</td>
                    <td>{{ $user->total_amount }}</td>
                </tr>
                <tr>
                    <td>Total amount in words :</td>
                    <td style="text-transform: uppercase;">{{ $user->total_in_word }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:left;font-weight:bold;text-transform:uppercase;">Office Use :</td>
                </tr>
                <tr>
                    <td>Transaction ID :</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Transaction Date :</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Branch SOL ID :</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Branch Name. :</td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="2"><div><span>Signature of the Depositor</span></div></td>
                </tr>
                <tr>
                    <td><div><span>Seal/Date</span></div></td>
                    <td><div><span>Authorised Signatory</span></div></td>
                </tr>
            </table>
        </div>
    </div>
 	<br>
    <div class="row no-print">
    	<div class="col-md-6">
    	</div>
        <div class="col-md-6">
        	<button style="float: right; margin-left: 10px;" class="btn btn-primary" onclick="myFunction()">Print Challan</button>
        	<a style="float: right;" type="button" class="btn btn-success" href="{{url('student/home')}}">Back to Dashboard</a>
        </div>
    </div>
    <br>
</div>
<script>
function myFunction() {
    window.print();
}
</script>
@endsection