@extends('student.layout.auth')

@section('content')
<div class="container">
    @if (session('alert'))
        <div class="alert alert-success">
            {{ session('alert') }}
        </div>
    @endif
    <div style="min-height: 500px;" align="center" class="row">
    @if(empty($status) && Auth::user()->isActive() )
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <div class="panel panel-default" style="margin-top: 10%">
                <div class="panel-body">
                    <h4>Welcome to {{ config('app.name', 'BCKV') }} </h4>
                    <h3>Application No. U{{ Auth::user()->id }}</h3>
                    <h5>Registered Email: {{ Auth::user()->email }} </h5>
                    <div class="list-group">
                        <br>
                        <a href="{{ url('student/personal') }}" class="btn btn-success">
                            START FILLUP FORM</a>
                    </div>
                </div>
                <div></div>
            </div>
        </div>
        <div class="col-sm-2"></div>
    @elseif($status >= 4 && Auth::user()->isRectification() )
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <div class="panel panel-default" style="">
                <div class="panel-body">
                    @if($status>=4 && $status < 6)
                    <h4 class="text-danger">Please Make Payment to Submit Application</h4>
                    @if($status==5 && $transaction_tab->t_type == 'Offline')
                    <h5>If you paid using offline method [ Using Challan ] please wait for confirmation </h5>
                    @endif
                    <hr/>
                    @elseif($status==6)
                    <h4 class="text-success">Application Submitted Successfully</h4>
                    @endif
                    <h4>You Can Still Modify Your Information Details</h4>
                    <h3>Application No. U{{ Auth::user()->id }}</h3>
                    <h5>Registered Email: {{ Auth::user()->email }} </h5>
                    <hr/>
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <div class="list-group">
                                <a href="{{ url('student/personal') }}" class="list-group-item list-group-item-success">
                                    <img src="{{ asset('images/tick.png') }}">
                                    Modify Personal Information
                                </a>
                            </div>
                            <div class="list-group">
                                <a href="{{ url('student/academic') }}" class="list-group-item list-group-item-success">
                                    <img src="{{ asset('images/tick.png') }}">
                                    Modify Academic Information
                                </a>
                            </div>
                            <div class="list-group">
                                <a href="{{ url('student/upload') }}" class="list-group-item list-group-item-success">
                                    <img src="{{ asset('images/tick.png') }}">
                                    Change Uploaded Images
                                </a>
                            </div>
                            @if($status>=4 && $status <6)
                                <div class="list-group">
                                    <a href="{{ url('student/previewonly') }}" class="list-group-item list-group-item-info">
                                        Preview Application</a>
                                </div>
                            @endif
                           
                            @if($status==4 || $status==5)
                                <div class="list-group">
                                    <a href="{{ url('student/payment') }}" class="list-group-item list-group-item-info">
                                        Make Payment</a>
                                </div>
                            @endif
                            

                            @if ($status == 5 && $transaction_tab->t_type == 'Offline')
                                <div class="list-group">
                                    <a href="{{ url('student/challan') }}" class="list-group-item list-group-item-info">
                                        Print Challan</a>
                                </div>
                            @endif
                            @if ($status<6)
                                <div class="list-group">
                                    <a class="list-group-item disabled">Print Application</a>
                                </div>
                            @elseif($status==6)
                                <div class="list-group">
                                    <a href="{{ url('student/transaction') }}" class="list-group-item list-group-item-info">
                                        Transaction Details</a>
                                </div>
                                <div class="list-group">
                                    <a href="{{ url('student/print') }}" class="list-group-item list-group-item-info">
                                        Print Application</a>
                                </div>
                            {{-- @elseif($status==6 && $category == "Physically Challange")
                                
                                <div class="list-group">
                                    <a href="{{ url('student/print') }}" class="list-group-item list-group-item-info">
                                        Print Application</a>
                                </div> --}}
                            @else
                                
                                <h3 style="color: red">Something went wrong in your payment process.</h3>
                                <br>
                                <h5>Please contact BCKV Admission Office.</h5>
                                <h5> OR Call +91-9163137309 </h5>
                                <div class="list-group">
                                    <a href="{{ url('student/previewonly') }}" class="list-group-item list-group-item-info">
                                        Preview Application</a>
                                </div>
                            @endif
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-2">
        </div>
    @elseif(Auth::user()->isActive() || ($status > 3 && Auth::user()->isPayment() ))
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            {{-- start personal info --}}
            <div class="panel panel-default" style="">
                <div class="panel-body">
                    <h3>Application No. U{{ Auth::user()->id }}</h3>
                    <h5>Registered Email: {{ Auth::user()->email }} </h5>
                    <hr/>
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            @if($status < 4)
                            @if ($status>0 && $status<4)
                                <div class="list-group">
                                    <a href="{{ url('student/personal') }}" class="list-group-item list-group-item-success">
                                        <img src="{{ asset('images/tick.png') }}">
                                        Personal Information
                                    </a>
                                </div>
                            @elseif($status>3)
                                <div class="list-group">
                                    <a class="list-group-item disabled">
                                        Personal Information</a>
                                </div>
                            @elseif(empty($status))
                                <div class="list-group">
                                    <a href="{{ url('student/personal') }}" class="list-group-item list-group-item-info">
                                        Personal Information</a>
                                </div>
                            @endif
                            
                            {{-- finish personal info --}}
                            {{-- start Academic Information --}}
                            @if ($status>0 && $status<4)
                                <div class="list-group">
                                    @if ($status>1)
                                    <a href="{{ url('student/academic') }}" class="list-group-item list-group-item-success">
                                        <img src="{{ asset('images/tick.png') }}">
                                        Academic Information</a>
                                    @else
                                        <a href="{{ url('student/academic') }}" class="list-group-item list-group-item-info">
                                            Academic Information</a>
                                    @endif

                                </div>
                            @else
                                <div class="list-group">
                                    <a class="list-group-item disabled">
                                        Academic Information</a>
                                </div>
                            @endif
                            {{-- finish Academic Information --}}
                            {{-- start Upload Image --}}
                            @if ($status>1 && $status<4)
                                <div class="list-group">
                                    @if ($status>2)
                                    <a href="{{ url('student/upload') }}" class="list-group-item list-group-item-success">
                                        <img src="{{ asset('images/tick.png') }}">
                                        Upload Image
                                    </a>
                                    @else
                                        <a href="{{ url('student/upload') }}" class="list-group-item list-group-item-info">
                                            Upload Image
                                        </a>
                                    @endif

                                </div>
                            @else
                                <div class="list-group">
                                    <a class="list-group-item disabled">
                                        Upload Image</a>
                                </div>
                            @endif
                            @endif
                            {{-- finish Upload Image --}}
                            {{-- start Preview Application & Submit Application --}}
                            @if ($status<3)
                                <div class="list-group">
                                    <a class="list-group-item disabled">
                                        Preview  &amp; Submit Application</a>
                                </div>
                            @elseif($status==3)
                                <div class="list-group">
                                    <a href="{{ url('student/preview') }}" class="list-group-item list-group-item-info">
                                        Preview  &amp; Submit Application</a>
                                </div>
                            @elseif($status>=4 && $status <6)
                                <div class="list-group">
                                    <a href="{{ url('student/previewonly') }}" class="list-group-item list-group-item-info">
                                        Preview Application</a>
                                </div>
                            @endif
                            {{-- finish Preview Application --}}
                            {{-- start Make Payment --}}
                            @if ($status<4)
                                <div class="list-group">
                                    <a class="list-group-item disabled">Make Payment</a>
                                </div>
                            @elseif($status==4 || $status==5)
                                <div class="list-group">
                                    <a href="{{ url('student/payment') }}" class="list-group-item list-group-item-info">
                                        Make Payment</a>
                                </div>
                            @endif
                            {{-- finish Make Payment --}}
                            {{-- start Print Challan if tried before --}}
                            @if ($status == 5 && $transaction_tab->t_type == 'Offline')
                                <div class="list-group">
                                    <a href="{{ url('student/challan') }}" class="list-group-item list-group-item-info">
                                        Print Challan</a>
                                </div>
                            @endif
                            {{-- finish Print Challan --}}
                            {{-- start Print Application --}}
                            @if ($status<6)
                                <div class="list-group">
                                    <a class="list-group-item disabled">Print Application</a>
                                </div>
                            @elseif($status==6)
                                <div class="list-group">
                                    <a href="{{ url('student/transaction') }}" class="list-group-item list-group-item-info">
                                        Transaction Details</a>
                                </div>
                                <div class="list-group">
                                    <a href="{{ url('student/print') }}" class="list-group-item list-group-item-info">
                                        Print Application</a>
                                </div>
                            {{-- @elseif($status==6 && $category == "Physically Challange")
                                
                                <div class="list-group">
                                    <a href="{{ url('student/print') }}" class="list-group-item list-group-item-info">
                                        Print Application</a>
                                </div> --}}
                            @else
                                <h3 style="color: red">Something went wrong in your payment process.</h3>
                                <br>
                                <h5>Please contact BCKV Admission Office.</h5>
                                <h5>OR Call +91-9163137309 </h5>
                            @endif
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
        </div>
    @else
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <div class="panel panel-default" style="margin-top: 10%">
                <div class="panel-body">
                    <h4>Hello, {{Auth::user()->name}}</h4>
                    <h2>Application NO. U{{ Auth::user()->id }}</h2>
                    <h5>Registered Email: {{ Auth::user()->email }} </h5>
                    <h3 class="text-danger">UG admission closed for {{ env('ADMISSION_SESSION') }} academic session.</h3><p></p>
                    <h4>Please visit BCKV official <a href="https://www.bckv.edu.in/"> website</a> for more information</h4>
                    @if($status > 4 && $status < 6)
                    <h5>If you paid using offline method [ Using Challan ] please wait for confirmation </h5>
                    @endif
                    <div class="list-group">
                        
                        @if($status==6 && $transaction_tab->t_status == 'SUCCESS')
                        <br>
                        <div class="list-group">
                            <a href="{{ url('student/print') }}" class="btn btn-primary">
                                Print Your Application Form</a>
                        </div>
                        @elseif($status==6 && $transaction_tab->t_status != 'SUCCESS')
                        <h3 style="text-center">BUT </h3>
                        <h3 style="color: red">Something went wrong in your payment process.</h3>
                        <h5>Please contact BCKV Admission Office.</h5>
                        <h5> OR Call<b> +91-9163137309 </b></h5>
                        @endif
                        <a href="{{ url('/student/logout') }}" class="btn btn-success"
                            onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ url('/student/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
                <div></div>
            </div>
        </div>
        <div class="col-sm-2"></div>
    @endif
    </div>
</div>
@endsection

@section('css')
<style type="text/css">
    .list-group {
        font-size: 1.2em;
    } 
    .list-group img {
        float: left;
    }
    hr {
        margin-top: 15px;
        margin-bottom: 15px;
    }
</style>     
@endsection
