@extends('student.layout.auth')

@section('content')
<div class="container page-header">
    <div class="row">
        <div class="col-md-12"> 
            <a href="javascript:void(0);" class="logo"> <img src="{{ asset('images/bckv-logo.png') }}" title="Bidhan Chandra Krishi Viswavidyalaya" /> </a>
            <div class="title">
                <h2>Bidhan Chandra Krishi Viswavidyalaya</h2>
                <h5 style="text-transform: uppercase; text-align: center; font-size: 14px;">P.O. Krishiviswavidyalaya, MOHANPUR, DIST : NADIA,<br/>
                    West Bengal, PIN : 741252</h5>
                <h5 style="text-align: center;">Notification No - {{ env('ADMISSION_ADVT_NO') }}</h5>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
        </div>
        <div class="col-sm-6">
            <div style="min-height: 400px;" class="panel panel-default">
                <div id="heading" align="center" class="panel-heading"><h4>UG Admission Transaction Details<h4></div>
                <div class="panel-body">
                   <table class="table table-bordered">
                    <tbody>
                      <tr>
                        <td>Transaction Mode</td>
                        <td>{{ $invoice->t_type }}</td>
                      </tr>
                      <tr>
                        <td>Application Number</td>
                        <td>{{ $invoice->application_no }}</td>
                      </tr>
                      <tr>
                        <td>Applicant Name</td>
                        <td>{{ $invoice->name }}</td>
                      </tr>
                      <tr>
                        <td>Applicant Email</td>
                        <td>{{ $invoice->email }}</td>
                      </tr>
                      <tr>
                        <td>Category</td>
                        <td>{{ $invoice->category }}</td>
                      </tr>
                      <tr>
                        <td>Total Amount</td>
                        <td>₹{{ $invoice->t_amt }} + Transaction Fees</td>
                      </tr>
                      <tr>
                        <td>Status</td>
                        <td>{{ $invoice->t_status }}</td>
                      </tr>
                      <tr>
                        <td>Invoice ID</td>
                        <td>{{ $invoice->invoice }}</td>
                      </tr>
                      <tr>
                        <td>Transaction ID</td>
                        <td>{{ $invoice->t_id }}</td>
                      </tr>
                      <tr>
                        <td>Payment Date</td>
                        <td>{{ $invoice->date }}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
        </div>
    </div>
   <div class="row no-print">
        <div class="col-md-7">
        <a  type="button" style="margin-left: 10px; float: right;" class="btn btn-success " href="{{ url('student/home') }}">Back to Dashboard</a>
        </div>
        <div class="col-md-5">
          <input type="button" value="Print Transaction Details" class="btn btn-primary print-tab"  onclick="window.print();">
        </div>
    </div>
</div>

@endsection

@section('css')
<style type="text/css">
  .page-header { 
        display: none 
  }
  @media print{
      .no-print {
          display: none;
      }
 
    .page-header {
        display: block;
        border-bottom: none;
        margin: 0px auto !important;
        page-break-before: always !important;
    }
    .logo {
        float: left;
        margin-right: 5px;
        margin: 4px 0px;
    }
    .logo img {
        height: 100px;
    }
    .title {
        float: left;
        margin: 0px;
        margin-left: 15px;
        height: 120px;
        padding: 12px 0;
    }
    .title h2 {
        margin-top: 0px !important;
    }
    .address label {
        margin-bottom: 0px;
        line-height: 1.4;
    }
  }
</style>
@endsection

