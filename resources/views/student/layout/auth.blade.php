<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="icon" href="{{ url('images/bckv-logo.png') }}" type="image/png" sizes="16x16">
    <link rel="icon" href="{{ url('images/bckv-logo.png') }}" type="image/png" sizes="32x32">

    <!-- Styles -->
    {{-- <link href="/css/app.css" rel="stylesheet"> --}}
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
     @yield('css')
    <style>
        * , body {
            font-family: 'Open Sans', Monospace !important;
        }
        textarea {
            resize:none
        }
        @media only screen and (max-width:480px){
            .navbar-brand {
                width: 82%;
                font-weight: bold;
                font-size: 12px !important;
                line-height: 17px !important;
                padding: 7px 15px !important;
            }   
        }
        @media only screen and (max-width:640px){
            .asistance, .print-tab {
                display:none;
            }
            .btn-success {
                margin-bottom: 10px;
            }
           
        }
        @media  print {
			.no-print {
				display:none;
			}
		}
    </style>

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116983977-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-116983977-1');
        function inIframe () {
            try {
                return window.self !== window.top;
            } catch (e) {
                return true;
            }
        }
        function inIframe () {
            try {
                return window.self !== window.top;
            } catch (e) {
                return true;
            }
        }
    </script>
</head>
<body>
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Bidhan Chandra Krishi Viswavidyalaya') }} {{ env('ADMISSION_SESSION') }}
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">            
                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/student/login') }}">Login</a></li>
                        @if(\Carbon\Carbon::parse(env('ADMISSION_END_DATE'))->isFuture() || \Carbon\Carbon::parse(env('ADMISSION_END_DATE'))->isToday())
                        <li><a href="{{ url('/student/register') }}">Register</a></li>
                        @endif
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ url('/student/logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/student/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')
    
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <div style="width: 100%;height: 50px"></div>
    @yield('js')
    <div class="container-fluid no-print" style="position: fixed;bottom: 0px;width: 100%">
        <div class="row" style="background: #C1C1C1;">
            <div class="col-md-12">
                <p class="pull-right" style="margin-top: 10px;
            font-size: 12px;
            font-weight: bold;
            margin-left: 10px;">Copyright <a href="http://www.bckv.edu.in/" target="_blank">BCKV</a> 2020-21. Designed, Developed &amp; Maintained By <a href="http://dos-infotech.com/" target="_blank">DOS Infotech (House Of IT)</a></p>

                <p class="pull-left asistance" style="margin-top: 10px;margin-bottom:0px;
            font-size: 12px;
            font-weight: bold;
            margin-left: 10px;">For any assistant contact us at <span style="font-size: 14px; color: #00f;">{{ env('CONTACT_MOBILE') }} </span> or Mail us at <span style="font-size: 14px; color: #00f;"><a href="mailto:bckvug.admission@gmail.com" target="_top">bckvug.admission@gmail.com</a></span> </p>
                <!--p class="pull-left" style="
                    font-size: 12px;
                    font-weight: bold;
                    margin-left: 10px;">For any technical assistance please contact at <span style="font-size: 14px; color: #00f;">08013935142</span>
                    [ 11 AM - 8 PM ]
                </p-->
            </div>
        </div>
    </div>
</body>
<script>
    if(inIframe ()) {
        document.body.innerHTML = '<h1>Please Open a new tab and paste this url : <a href="https://bckvadmn.in">https://bckvadmn.in</a> </h1>';
    }
</script>
</html>
