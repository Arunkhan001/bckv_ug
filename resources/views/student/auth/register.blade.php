@extends('student.layout.auth')

@section('content')
<div class="container">
    <div class="row">
        @if( \Carbon\Carbon::now()->between(\Carbon\Carbon::parse(env('ADMISSION_START_DATE')),\Carbon\Carbon::parse(env('ADMISSION_END_DATE'))) || \Carbon\Carbon::parse(env('ADMISSION_END_DATE'))->isToday())
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Please Provide Necessary Information</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/student/register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Full Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" required class="form-control" name="name" value="{{ old('name') }}" autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" required class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('mobile_number') ? ' has-error' : '' }}">
                            <label for="mobile_number" class="col-md-4 control-label">Mobile Number</label>

                            <div class="col-md-6">
                                <input id="mobile_number" type="text" required class="form-control" name="mobile_number" value="{{ old('mobile_number') }}">

                                @if ($errors->has('mobile_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" required class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" required class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Continue Form Fill-Up
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        @elseif(\Carbon\Carbon::parse(env('ADMISSION_START_DATE'))->isFuture())
        <div class="col-md-8 col-md-offset-2" style=" margin-top: 8%">
            <div class="panel panel-default text-center">
                <div class="panel-body">
                <h3 class="text-success"> UG Admission for {{ env('ADMISSION_SESSION')}} will be start from {{ \Carbon\Carbon::parse(env('ADMISSION_START_DATE'))->format('d-M-Y')}}</h3>
                <h4>Please visit BCKV official <a href="https://www.bckv.edu.in/"> website</a> for more information</h4>
                </div>
            </div>
        </div>
        @elseif(\Carbon\Carbon::parse(env('ADMISSION_END_DATE'))->isPast())
        <div class="col-md-8 col-md-offset-2" style=" margin-top: 8%">
            <div class="panel panel-default">
                <div class="panel-body text-center">
                <h3 class="text-danger"> UG admission closed for {{ env('ADMISSION_SESSION')}} academic session.</h3>
                <h4>Please visit BCKV official <a href="https://www.bckv.edu.in/"> website</a> for more information</h4>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@endsection
