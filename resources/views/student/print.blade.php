@extends('student.layout.auth')

@section('content')
@foreach ($user as $user)
<div class="container page-header">
    <div class="row">
        <div class="col-md-12"> 
            <a href="javascript:void(0);" class="logo"> <img src="{{ asset('images/bckv-logo.png') }}" title="Bidhan Chandra Krishi Viswavidyalaya" /> </a>
            <div class="title">
                <h2>Bidhan Chandra Krishi Viswavidyalaya</h2>
                <h5 style="text-transform: uppercase; text-align: center; font-size: 13px;">P.O. Krishiviswavidyalaya, MOHANPUR, DIST : NADIA, WB, PIN : 741252</h5>
                <h5 style="text-align: center;">Notification No - {{ env('ADMISSION_ADVT_NO') }}</h5>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-10" style="padding-left: 0px !important; padding-right: 0px !important">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Application No. : {{ $user->application_no }}</h4>
                </div>
                <div class="panel-body">
                    <p class="clz-addr" style="text-align: justify;">Application for Admission to <b>B. Sc.(Hons.) Ag. &amp; B. Sc.(Hons.) Hort. Courses</b> for Mohanpur Campus, Burdwan &amp; Bankura (Extended Campus of BCKV) for the Academic Session {{ env('ADMISSION_SESSION')}} <b>(WBCHSE / WBSCVE&amp;T / OTHER BOARD)</b></p>
                </div>
            </div>
        </div>
        <div class="col-sm-2 text-center">
            <img src="{{ url('student/image/'.$user->app_no.'/photo.jpg') }}" class="profile" alt="Profile Picture">
        </div>
    </div>

    <div class="row" id="Personal">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Personal Information of the Candidate :</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="col-sm-12">
                            <label class="col-sm-6">Candidate Full Name : </label>

                            <div class="col-sm-6">
                                <label><b>{{$user->name}}</b></label>
                            </div>
                        </div>
                        @if($user->father_name)
                        <div class="col-sm-12">
                            <label class="col-sm-6">Father's Name : </label>

                            <div class="col-sm-6">
                                <label>{{$user->father_name}}</label>
                            </div>
                        </div>
                        @endif
                        <div class="col-sm-12">
                            <label class="col-sm-6">Mother's Name : </label>

                            <div class="col-sm-6">
                                <label>{{$user->mother_name}}</label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <label class="col-sm-6">Candidate Mobile No. : </label>

                            <div class="col-sm-6">
                                <label><b>{{$user->mobile_number}}</b></label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <label class="col-sm-6">Email Address : </label>

                            <div class="col-sm-6">
                                <label>{{$user->email}}</label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <label class="col-sm-6">Guardian Name : </label>
                            <div class="col-sm-6">
                                <label>{{$user->guardian_name}}</label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <label class="col-sm-6">Guardian Mobile No : </label>
                            <div class="col-sm-6">
                                <label>{{$user->guardian_mobile_number}}</label>
                            </div>
                        </div>
                        @if($user->guardian_email)
                        <div class="col-sm-12">
                            <label class="col-sm-6">Guardian Email Address : </label>
                            <div class="col-sm-6">
                                <label>{{$user->guardian_email}}</label>
                            </div>
                        </div>
                        @endif
                        <div class="col-sm-12">
                            <label class="col-sm-6">Relation with Guardian : </label>

                            <div class="col-sm-6">
                                <label>{{$user->guardian_relationship}}</label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <label class="col-sm-6">Date of Birth : </label>

                            <div class="col-sm-6">
                                <label><b>{{ date('d/m/Y', strtotime($user->date_of_birth))}}</b></label>
                            </div>
                        </div>
                        @if($user->religion)
                        <div class="col-sm-12">
                            <label class="col-sm-6">Religion : </label>

                            <div class="col-sm-6">
                                <label>{{$user->religion}}</label>
                            </div>
                        </div>
                        @endif
                       
                        @if($user->blood_group)
                        <div class="col-sm-12">
                            <label class="col-sm-6">Blood Group : </label>

                            <div class="col-sm-6">
                                <label>{{$user->blood_group}}</label>
                            </div>
                        </div>
                        @endif
                        
                    </div>
                    <div class="col-sm-6">
                        <div class="col-sm-12">
                            <label class="col-sm-6">Residential Area Type : </label>

                            <div class="col-sm-6">
                                <label>{{$user->residential_area_type}}</label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <label class="col-sm-6">Domicile : </label>

                            <div class="col-sm-6">
                                <label>{{ucwords($user->domicile)}}</label>
                            </div>
                        </div>
                         <div class="col-sm-12">
                            <label class="col-sm-6">Category : </label>

                            <div class="col-sm-6">
                                <label><b>{{$user->category}}</b></label>
                            </div>
                        </div>
                        
                        
                        @if($user->category == "SC" || $user->category == "ST" || $user->category == "OBC-A" || $user->category == "OBC-B")
                        <div class="col-sm-12">
                            <label class="col-sm-6">Caste Certificate Number : </label>

                            <div class="col-sm-6">
                                <label>{{$user->certificate_number}}</label>
                            </div>
                        </div>
                        
                        <div class="col-sm-12">
                            <label class="col-sm-6">Issuing Date : </label>

                            <div class="col-sm-6">
                                <label>{{ date('d/m/Y', strtotime($user->issuing_date))}}</label>
                            </div>
                        </div>
                        @endif
                        <div class="col-sm-12">
                            <label class="col-sm-6">Sport Quota : </label>

                            <div class="col-sm-6">
                                <label><b>{{$user->sport_quota}}</b></label>
                            </div>
                        </div>


                        @if($user->sport_quota == "Yes")
                        <div class="col-sm-12">
                            <label class="col-sm-6">Sport Certificate Number : </label>

                            <div class="col-sm-6">
                                <label>{{$user->sport_certificate_number}}</label>
                            </div>
                        </div>
                        
                        <div class="col-sm-12">
                            <label class="col-sm-6">Sport Certificate Issue Date : </label>

                            <div class="col-sm-6">
                                <label>{{ date('d/m/Y', strtotime($user->sport_issuing_date))}}</label>
                            </div>
                        </div>
                        @endif


                        <div class="col-sm-12">
                            <label class="col-sm-6">Gender : </label>
                            <div class="col-sm-6">
                                <label><b>{{$user->gender}}</b></label>
                            </div>
                        </div>
                        
                        <div class="col-sm-12">
                            <label class="col-sm-6">Nationality : </label>

                            <div class="col-sm-6">
                                <label>{{$user->nationality}}</label>
                            </div>
                        </div>
                        
                        {{-- @if($user->aadhaar != NULL && $user->student_aadhaar_number == NULL)
                        <div class="col-sm-12">
                            <label class="col-sm-6">Aadhaar : </label>

                            <div class="col-sm-6">
                                <label>{{$user->aadhaar}}</label>
                            </div>
                        </div>
                        @endif --}}

                        @if($user->student_aadhaar_number != NULL)
                        <div class="col-sm-12">
                            <label class="col-sm-6">Candidate Aadhaar No : </label>

                            <div class="col-sm-6">
                                <label><b>{{$user->student_aadhaar_number}}</b></label>
                            </div>
                        </div>
                        @endif

                       {{--  @if($user->guardian_aadhaar_number != NULL)
                        <div class="col-sm-12">
                            <label class="col-sm-6">Guardian Aadhaar No : </label>

                            <div class="col-sm-6">
                                <label>{{$user->guardian_aadhaar_number}}</label>
                            </div>
                        </div>
                        @endif  

                        @if($user->guardian_aadhaar_name != NULL)
                        <div class="col-sm-12">
                            <label class="col-sm-6">Guardian Name : </label>

                            <div class="col-sm-6">
                                <label>{{$user->guardian_aadhaar_name}}</label>
                            </div>
                        </div>
                        @endif  

                        @if($user->guardian_relation != NULL)
                        <div class="col-sm-12">
                            <label class="col-sm-6">Guardian Relation : </label>

                            <div class="col-sm-6">
                                <label>{{$user->guardian_relation}}</label>
                            </div>
                        </div>
                        @endif --}}  


                        @if($user->bank_account_details != NULL)

                        <div class="col-sm-12">
                            <label class="col-sm-6">Candidate S/B Account No :</label>

                            <div class="col-sm-6">
                                <label><b>{{$user->bank_account_details}}</b></label>
                            </div>
                        </div>
                    {{-- <div class="form-group">
                        <label class="col-sm-6 control-label" style="font-size: 13px;">Candidate S/B Account Details : </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{$user->bank_account_details}}</label>
                        </div>
                    </div> --}}
                    @endif

                     @if($user->bank_ifsc_code != NULL)

                     <div class="col-sm-12">
                            <label class="col-sm-6">Bank IFS Code : </label>

                            <div class="col-sm-6">
                                <label><b>{{$user->bank_ifsc_code}}</b></label>
                            </div>
                        </div>
                   {{--  <div class="form-group">
                        <label class="col-sm-6 control-label">Bank IFS Code : </label>

                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">{{$user->bank_ifsc_code}}</label>
                        </div>
                    </div> --}}
                    @endif                      
                    </div> 
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="col-sm-12">
                            <label class="col-sm-12" style="border-bottom: 1px solid #000"><b>Permanent Address : </b></label>
                        </div>    
                        <div class="col-sm-12 address">
                            @if($user->father_name)
                                <label class="col-sm-12 control-label">C/O - {{$user->father_name}} <b>,</b></label>
                            @else
                                <label class="col-sm-12 control-label">C/O - {{$user->mother_name}} <b>,</b></label>
                            @endif
                            <label class="col-sm-12">@if($user->street_no){{$user->street_no}} <b>,@endif</b> {{$user->area_type}} - {{$user->village}} <b>,</b></label>
                            <label class="col-sm-12">P.O. - {{$user->post_office}} 
                                <b>,</b> P.S. - {{$user->police_station}} <b>,</b></label>
                            <label class="col-sm-12">District - {{$user->district}}  <b>,</b> State - {{$user->state}} <b>,</b></label>
                            <label class="col-sm-12">Pincode - {{$user->pincode}}
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="col-sm-12">
                            <label class="col-sm-12" style="border-bottom: 1px solid #000"><b>Correspondence Address :</b></label>
                        </div>
                        <div class="col-sm-12 address">
                            @if($user->father_name)
                                <label class="col-sm-12 control-label">C/O - {{$user->father_name}} <b>,</b></label>
                            @else
                                <label class="col-sm-12 control-label">C/O - {{$user->mother_name}} <b>,</b></label>
                            @endif
                            <label class="col-sm-12"> @if($user->correspondence_street_no){{$user->correspondence_street_no}} <b>,@endif</b> {{$user->area_type}} - {{$user->correspondence_village}} <b>,</b></label>
                            <label class="col-sm-12">P.O. - {{$user->correspondence_post_office}} 
                                <b>,</b> P.S. - {{$user->correspondence_police_station}} <b>,</b></label>
                            <label class="col-sm-12">District - {{$user->correspondence_district}}  <b>,</b> State - {{$user->correspondence_state}} <b>,</b></label>
                            <label class="col-sm-12">Pincode - {{$user->correspondence_pincode}}</label>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="panel panel-default">
            <div id="heading" class="panel-heading"><h4>Academic Qualification (10<sup>th</sup> Standard)<h4></div>
            <div class="panel-body">

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-8 control-label">Board Name : </label>

                        <div class="col-sm-4">
                            <label class="col-sm-12 control-label">{{ $user->board_name_classten}}</label>
                        </div>
                    </div>

                    @if($user->specify_other_board != NULL)
                    <div class="form-group">
                        <label class="col-sm-8 control-label">Specified Other Board: </label>

                        <div class="col-sm-4">
                            <label class="col-sm-12 control-label">{{$user->specify_other_board}}</label>
                        </div>
                    </div> 
                    @endif 

                     

                    <div class="form-group">
                        <label class="col-sm-8 control-label">Passing Year</label>

                        <div class="col-sm-4">
                            <label class="col-sm-12 control-label">{{$user->passing_year_10}}</label>
                        </div>
                    </div> 


                       
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-9 control-label">Total Marks except Additional Subject : </label>

                        <div class="col-sm-3">
                            <label class="col-sm-12 control-label">{{ $user->class10fm}}</label>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-9 control-label">Obtained Marks except Additional Subject : </label>

                        <div class="col-sm-3">
                            <label class="col-sm-12 control-label">{{ $user->class10mrk}}</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-9 control-label">Percentage</label>

                        <div class="col-sm-3">
                            <label class="col-sm-12 control-label" >{{$user->classten_percentage}} %</label>
                        </div>
                    </div> 
                       
                </div>
                           
            </div>
        </div>       
    </div>

    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading"><h4>Academic Qualification ( 10+2 Standard ) :</h4></div>
            <div class="panel-body education">
                <div class="row" style="border-bottom: 1px solid #000">
                    <div class="col-sm-12">
                        <label class="col-sm-3">Vocational :  <b>{{ $user->vocational}}</b></label>
                        {{-- @if($user->vocational != "No")
                        <label class="col-sm-2"> 
                        Syllabus : <b>{{ucfirst($user->syllabus)}}   </b>
                        </label>
                        @endif --}}
                        <label class="col-sm-3">Year of Passing :  <b>{{$user->passing_year}}</b></label>
                        <label class="col-sm-4">Board Name :  <b>{{$user->board_name}}</b> </label>
                    </div>
                </div>
                <div class="row">
                    @if($user->vocational == 'No')
                    <div class="col-sm-12">
                        <div align="center">
                            <table cellpadding="0" cellspacing="0" class="qualification-table">
                                <!-- <caption>Subject Wise Marks Details</caption> -->
                                <tr>
                                    <td>Subject</td>
                                    <td>(A) Physics</td>
                                    <td>(B) Chemistry</td>
                                    <td>(C) Biology</td>
                                    <td>(D) English</td>
                                    <td>Total(A+B+C+D)</td>
                                    <td rowspan="2">Percentage</td>
                                </tr>
                                <tr> 
                                    <td>Full Marks</td>
                                    <td>{{$user->fm1}}</td>
                                    <td>{{$user->fm2}}</td>
                                    <td>{{$user->fm3}}</td>
                                    <td>{{$user->fm4}}</td>
                                    <td>{{$user->total_fullmarks}}</td>
                                    {{-- <td>Out of 100</td> --}}
                                </tr> 
                                <tr>
                                    <td>Obained Marks</td>
                                    <td>{{$user->mrk1}}</td>
                                    <td>{{$user->mrk2}}</td>
                                    <td>{{$user->mrk3}}</td>
                                    <td>{{$user->mrk4}}</td>
                                    <td><b>{{$user->total_mark}}</b></td>
                                    <td ><b>{{$user->percentage}} %</b></td>    
                                </tr>
                            </table>
                        </div>    
                    </div>
                    
                    @elseif($user->vocational == 'Yes')
                    <div class="col-sm-12">
                        <div align="center" >
                            {{-- <h5><b>Subject Codes : (PPFV/POFR/POVG/HNMG/FMAP/CNMG/SEPR/PHMG/POFC)</b></h5> --}}
                            <table cellpadding="0" cellspacing="0" class="qualification-table">
                                <!-- <caption>Subject Wise Marks Details(Vocational New Syllabus)</caption> -->
                                <tr>
                                    <td>Subject</td>
                                    <td>English<br/></td>
                                    <td>Biology<br/></td>
                                    <td>{{$user->science}}<br/></td>
                                    <td>(Subject Code)<br>{{$user->paper1}}</td>
                                    <td>(Subject Code)<br>{{$user->paper2}}</td>
                                    <td>Total</td>
                                    <td rowspan="2">Percentage</td>
                                </tr>
                                <tr> 
                                    <td>Full Marks</td>
                                    <td>{{$user->fm1}}</td>
                                    <td>{{$user->fm2}}</td>
                                    <td>{{$user->fm3}}</td>
                                    <td>{{$user->fm4}}</td>
                                    <td>{{$user->fm5}}</td>
                                    <td>{{$user->total_fullmarks}}</td>
                                    {{-- <td>Out of 100</td> --}}
                                </tr>
                                <tr>
                                    <td>Obained Marks</td>
                                    <td>{{$user->mrk1}}</td>
                                    <td>{{$user->mrk2}}</td>
                                    <td>{{$user->mrk3}}</td>
                                    <td>{{$user->mrk4}}</td>
                                    <td>{{$user->mrk5}}</td>
                                    <td><b>{{$user->total_mark}}</b></td>
                                    <td ><b>{{$user->percentage}} %</b></td> 
                                </tr>
                            </table>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <label class="col-sm-12 dec">
                        <input type="checkbox" checked="checked" disabled /> <span>I HEREBY DECLARE THAT THE INFORMATION FURNISHED ABOVE ARE TRUE TO THE BEST OF MY KNOWLEDGE AND THAT I SHALL ABIDE BY THE RULES AND REGULATION OF THE VISWAVIDYALAYA</span>
                    </label>
                </div>
                <div class="row">
                    <div class="col-sm-offset-8 col-sm-4 signature" >
                        <img src="{{url('student/image/'.$user->app_no.'/signature.jpg')}}" alt="SIGNATURE OF THE CANDIDATE" style="width: 3.5cm; height: 1cm;">
                        <figcaption class="figure-caption text-center">SIGNATURE OF THE CANDIDATE</figcaption>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    <div class="row office">
        <div class="col-sm-12 text-center" style="border-bottom: 1px solid #3cc3d9; margin-top: 5px;margin-bottom: 5px; ;padding: 5px;">
            <h4>FOR USE OF THE MEMBERS OF THE JOINT ADMISSION COMMITTEE</h4>
        </div>
        <div class="col-sm-6 text-center">
            <span style="float: left" class="col-sm-12">ACCEPTED / REJECTED <br/>MEMBER'S COMMENTS OF THE ADMISSION COMMITTEE</span>
            
        </div>
        <div class="col-sm-6 text-center">
            <span style="float: right" class="col-sm-12">ADMITTED / NOT ADMITTED <br/>(REASONS FOR NON ADMISSION MAY BE RECORDED)</span>
            
        </div>
    </div>
    <div class="row office"  style="padding-top: 33px;">
        <div class="col-sm-6 text-center">
            <span  class="col-sm-12">SIGNATURE WITH DATE<br/>
            MEMBER OF THE ADMISSION COMMITTEE</span>
        </div>
        <div class="col-sm-6 text-center">
            <span  class="col-sm-12">SIGNATURE WITH DATE
                <br/>MEMBER OF THE ADMISSION COMMITTEE</span>
        </div>
    </div>  
</div>
@endforeach
<div class="container">
    <div class="row no-print" style="margin-bottom: 30px;">
        <div class="col-sm-12">
            <input type="button" value="Print Application" class="btn btn-primary" style="margin-left: 10px; float: right;" onclick="window.print();">
            <a style="float:right;" type="button" class="btn btn-success" href="{{url('student/home')}}">Back to Dashboard</a>  

        </div>
    </div>
</div>
@endsection

@section('css')
<style type="text/css">
    @media only screen and (max-width:640px){
        .form-group {
            border: 1px solid #f2f2f2;
        }
    }
    h4 {
        margin-top: 0px;
        margin-bottom: 5px;
    }
    .profile {
        overflow: hidden;
        width: 100%;
        max-width: 3.5cm;
        height: 3.5cm; 
        border: 1px solid #000; 
        padding: 0px;
    }
    label {
        font-weight: 400 !important;
    }
    .page-header { 
        display: none 
    }
    table {
        margin-top: 10px;
        width: 100%;
        border:1px solid #000;
    }
    table, td, th {
        text-align: center;
        padding: 5px;
    }
    td{
        border: 1px solid #000;
    }
    .signature img{
        display: block;
        margin-left: auto;
        margin-right: auto;
    }
    .office {
        display: none;
    }
    @media print {
        body {
            font-size: 10px !important;
            line-height: 1.2 !important;
        }
        h4 {
            font-size: 16px !important;
            margin-bottom: 0px;
        }
        h5 {
            margin-top: 5px !important;
            margin-bottom: 5px !important;
        }
        p {
            margin: 0px !important;
        }
        .panel {
            margin-bottom : 8px !important;
        }
        .panel-default {
            border-color: #000;
        }
        .panel-heading {
            padding: 5px 10px !important;
            border-bottom-color: #000 !important;
        }
        .panel-body {
            padding: 5px 10px !important;
        }
        .col-sm-2,.col-sm-4,.col-sm-6,.col-sm-8,.col-sm-12 {
            padding-left: 5px !important;
            padding-right: 5px !important;
        }
        .profile {
            padding: 0px;
            margin-top: -20px;
            max-width: 3cm !important;
            height: 3cm !important; 
        }
        .page-header {
            display: block;
            border-bottom: none;
            margin: 0px auto !important;
            padding-bottom: 0px !important;
            page-break-before: always !important;
        }
        .logo {
            float: left;
            margin-right: 5px;
            margin: 4px 0px;
        }
        .logo img {
            height: 100px;
        }
        .title {
            float: left;
            margin: 0px;
            margin-left: 15px;
            height: 120px;
            padding: 12px 0;
        }
        .title h2 {
            margin-top: 0px !important;
        }
        .address label {
            margin-bottom: 0px;
            line-height: 1.4;
        }
        table {
            margin-top: 0px;
            border-style: hidden !important;
        }
        .education {
            padding-bottom: 0px !important;
        }
        .education caption {
            padding-left: 10px;
        }
        .signature img {
            margin-top: -10px;
        }
        .dec {
            font-size: 8px !important;
            padding-left: 15px !important;
            padding-right: 15px !important;
        }
        input {
          width: 13px;
          height: 13px;
          padding: 0;
          margin:0;
          vertical-align: bottom;
          position: relative;
          top: 0px;
          overflow: hidden;
        }
        figcaption {
            font-size: 8px !important;
        }
        .office {
            display: block;
            font-size: 9px !important;
        }
        .no-print {
            display: none !important;
        }


    }
</style>
 <link rel="stylesheet" href="{{ asset('css/print.css') }}" type="text/css"/>
 {{-- <link rel="stylesheet" href="{{ asset('css/formpreview.css') }}" type="text/css"/> --}}
@endsection