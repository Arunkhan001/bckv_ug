@extends('management.layout.auth')
@section('content')

<div class="container">
    <div align="center" class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                <h4>Upload CSV FILE of Challan Offline Payment</h4>
                </div>

                <div style="min-height: 300px;" class="panel-body">

                    {{-- after csv upload to show messge --}}
                    @if (session('alert'))
                        <div class="alert alert-success">
                            {{ session('alert') }}
                        </div>
                    @endif
                    @if($error_array == null && $success_array == null)
                    <form enctype="multipart/form-data" method="post" action="{{url('management/csvupload') }}">
                        {{ csrf_field() }}
                       <div align="center" class="row">
                            <div class="col-md-8">
                                <div class="form-group{{ $errors->has('csv') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label"><b>*&nbsp;</b>Upload CSV File : </label>
                                    <div class="col-md-6">
                                    <input class="form-control" name="csv" type="file" required>
                                    @if ($errors->has('csv'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('csv') }}</strong>
                                        </span>
                                    @endif
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="submit" value="Upload CSV" class='btn btn-primary'>
                                    </div> 
                                </div> 
                            </div>
                       </div>
                    </form>
                    @else
                        @if($success_array != null)
                        <form method="post" action="{{url('management/offlinedata') }}">
                            {{ csrf_field() }}
                            <table class="table table-bordered" align="center" style="font-weight: 600 !important;">
                                <tr>
                                    <th>Invoice No</th>
                                    <th>Application No</th>
                                    <th>Amount</th>
                                    <th>Trans_ID</th>
                                    <th>DB_Amount</th>
                                </tr>
                                @foreach($success_array as $row)
                                <tr>
                                    <td><input class="form-control" type="text" name="invoice_no[]" value="{{ $row[0] }}" readonly /></td>
                                    <td><input class="form-control" type="text" name="app_no[]" value="{{ $row[1] }}" readonly/></td>
                                    <td><input class="form-control" type="text" name="amount[]" value="{{ $row[5] }}" readonly/></td>
                                    <td><input class="form-control" type="text" name="t_id[]" value="{{ $row[6] }}" readonly/></td>
                                    <td><input class="form-control" type="text" value="{{ $row[10] }}" readonly/></td>
                                </tr>
                                @endforeach
                            </table>
                            <br>
                            <input type="submit" value="Save & Proceed" class='btn btn-primary'>
                        </form>
                        <br>
                        @endif
                        @if($error_array != null)
                        <table class="table table-bordered">
                            <tr>
                                <th>Invoice No</th>
                                <th>Application No</th>
                                <th>Trans_ID</th>
                                <th>DB_Status</th>
                                <th>Amount</th>
                                <th>DB_Amount</th>
                            </tr>
                            @foreach($error_array as $row)
                            <tr class="danger">
                                <td>{{ $row[0] }}</td>
                                <td>{{ $row[1] }}</td>
                                <td>{{ $row[6] }}</td>
                                <td>{{ $row[9] }}</td>
                                <td>{{ $row[5] }}</td>
                                <td>{{ $row[10] }}</td>
                            </tr>
                            @endforeach
                        </table>
                        @endif
                    @endif
                </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">  
        </div>
        <div class="col-md-2">
            <a type="button" class="btn btn-success" href="{{url('management/home')}}">Back to Dashboard</a>   
        </div>
        <div class="col-md-2">
            
        </div>
    </div>
</div>
<script src="{{ asset('js/validation.js') }}"></script>
@endsection
