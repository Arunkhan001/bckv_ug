@extends('management.layout.auth')

@section('content')
<div class="hidden-print" style="text-align: center; margin-bottom: 30px;">
    <p style="color: red;">Kindly Choose Scale as 98</p>
    <button class="btn btn-primary" onclick="myPrint()">Print this page</button>
</div>

@if(count($student_array) != 0)


	
		@foreach ($student_array as $students_id)
		
			<h1>Application ID - {{ $students_id }}</h1>
			
		
		@endforeach
	



	@else
<h1 style="text-align: center;">NO RECORD FOUND! </h1>
@endif

<script>
    function myPrint() {
        window.print();
    }
</script>
<style>
@media print {
	.page-break { display: block; page-break-before: always; }
	img {
	    max-width:100%;
	    max-height:900px;
	}
	h1{
		text-align:center;
	}
}
</style>
@endsection