@extends('management.layout.auth')

@section('content')
<div class="hidden-print" style="text-align: center; margin-bottom: 30px;">
    <p style="color: red;">Kindly Choose Scale as 98</p>
    <button class="btn btn-primary" onclick="myPrint()">Print this page</button>
</div>
@if(count($students_array) != 0)
	@if($case == "caste")
		@foreach ($students_array as $students_id)
		<div class="maxheight">
			<h1>Application ID - U{{ $students_id }}</h1>
			<img src=" {{ url('management/image/') }}/{{ $students_id }}/certificate.jpg" width="100%">
		</div>
		<div class="page-break"></div>
		@endforeach
	@endif

	@if($case == "sport")
		@foreach ($students_array as $students_id)
		<div class="maxheight">
			<h1>Application ID - U{{ $students_id }}</h1>
			<img src=" {{ url('management/image/') }}/{{ $students_id }}/sport_certificate.jpg" width="100%">
		</div>
		<div class="page-break"></div>
		@endforeach
	@endif

	@if($case == "marksheet")
		@foreach ($students_array as $students_id)
		<div class="maxheight">
			<h1>Application ID - U{{ $students_id }}</h1>
			<img src=" {{ url('management/image/') }}/{{ $students_id }}/marksheet.jpg" width="100%">
		</div>
		<div class="page-break"></div>
		@endforeach
	@endif

	@if($case == "10thmarksheet")
		@foreach ($students_array as $students_id)
		<div class="maxheight">
			<h1>Application ID - U{{ $students_id }}</h1>
			<img src=" {{ url('management/image/') }}/{{ $students_id }}/10th_marksheet_font.jpg" width="100%">
		</div>
		<div class="page-break"></div>
		@endforeach
	@endif

	@if($case == "birth_certificate")
		@foreach ($students_array as $students_id)
		<div class="maxheight">
			<h1>Application ID - U{{ $students_id }}</h1>
			<img src=" {{ url('management/image/') }}/{{ $students_id }}/birth_certificate.jpg" width="100%">
		</div>
		<div class="page-break"></div>
		@endforeach
	@endif

	@if($case == "aadhaar")
		@foreach ($students_array as $students_id)
		<div class="maxheight">
			<h1>Application ID - U{{ $students_id }}</h1>
			<img src=" {{ url('management/image/') }}/{{ $students_id }}/aadhaar.jpg" width="100%">
		</div>
		<div class="page-break"></div>
		@endforeach
	@endif
@else
<h1 style="text-align: center;">NO RECORD FOUND! </h1>
@endif

<script>
    function myPrint() {
        window.print();
    }
</script>
<style>
@media print {
	.page-break { display: block; page-break-before: always; }
	img {
	    max-width:100%;
	    max-height:900px;
	}
	h1{
		text-align:center;
	}
}
</style>
@endsection