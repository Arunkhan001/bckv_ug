@extends('management.layout.auth')

@section('content')
<div class="container">
    <div class="row text-center">
        <div class="col-sm-3">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title pull-left">Total Registration<h4>
                </div>
                <div class="panel-body">
                    <b>{{ $register }}</b>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h6 class="panel-title pull-left">Personal   |  10th & 10+2<h5>
                </div>
                <div class="panel-body">
                    <b class="pull-left">{{ $step1 }}</b> 
                    <b class="pull-right"> {{ $step2 }}</b>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title pull-left">Image Uploaded<h4>
                </div>
                <div class="panel-body">
                    <b>{{ $step3 }}</b>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title pull-left">Form Submited<h4>
                </div>
                <div class="panel-body">
                    <b>{{ $step4 }}</b>
                </div>
            </div>
        </div>
        {{-- <div class="col-sm-2">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h4 class="panel-title pull-left">Offline Due<h4>
                </div>
                <div class="panel-body">
                    <b>{{ $ofline }}</b>
                </div>
            </div>
        </div> --}}
        <div class="col-sm-3">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h4 class="panel-title pull-left">Successfull<h4>
                </div>
                <div class="panel-body">
                    <b>{{ $step6 }}</b>
                </div>
            </div>
        </div>
		<div class="col-sm-4">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h4 class="panel-title pull-left">SUCCESSFUL ONLINE PAYMENT<h4>
                </div>
                <div class="panel-body">
                    <b>₹ {{ $onlinepayment }}</b>
                </div>
            </div>
        </div>
    </div>
</div>
<hr/>
<div style="min-height: 400px;" align="center" class="row">
    <div class="col-md-2">
    </div>
    <div class="col-md-6">
        <div class="list-group">
            <a href="{{ url('/management/meritlist') }}" class="list-group-item">
            <h4>Check Merit-List</h4></a>
        </div>
       {{--  <div class="list-group">
            <a href="{{ url('/management/csvupload') }}" class="list-group-item">
            <h4>Challan Upload</h4></a>
        </div> --}}
        


        {{-- <div class="list-group">
            <a href="{{ url('/management/meritlistWBCHSEOTHERS') }}" class="list-group-item">
            <h4>Combined Merit List for WBCHSE / OTHERS Board</h4></a>
        </div> --}}
        <div class="list-group">
            <a href="{{ url('/management/printmeritlist') }}" class="list-group-item">
            <h4>Print Merit List (for Publication)</h4></a>
        </div>


        

        <div class="list-group">
            <a href="{{ url('/management/checkstatus') }}" class="list-group-item">
            <h4>Check Application Status</h4></a>
        </div>



    </div>
    <div class="col-md-4">

        <div class="list-group">
            <a href="{{ url('/management/counselling_bulk') }}" class="list-group-item">
            <h4>Counselling Bulk</h4></a>
        </div>

        {{-- <div class="list-group">
            <a href="{{ url('/management/bulksms') }}" class="list-group-item">
            <h4>Bulk SMS Send</h4></a>
        </div> --}}

    </div>
    
</div>



<script type="text/javascript">
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
    if(exist){
      alert(msg);
    }
  </script>
@endsection