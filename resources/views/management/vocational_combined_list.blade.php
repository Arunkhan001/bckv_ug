@extends('management.layout.auth')
@section('content')
<style>
  #app_table_box td{
        border: 1px solid #000;
       
        text-align: center;
        }
        #app_table_box th{
        border: 1px solid #000;
        
        text-align: center;
        }
    @media print {
        * {
        -webkit-print-color-adjust: exact !important;   /* Chrome, Safari */
        color-adjust: exact !important;                 /*Firefox*/
        }

        #app_table_box td{
        border: 1px solid #000;
        font-size: 35px;
        text-align: center;
        }
        #app_table_box th{
        border: 1px solid #000;
        font-size: 25px;
        text-align: center;
        }
        body{
            background-color: #fff !important;
            margin: 0px;
        }
       
        .remarks{
            min-width: 300px;
        }

    body
    {
      margin-left: 155px !important;
    }
    h2{
      font-size: 70px;
    }

    h3{
      font-size: 45px;
    }
    }
</style>
<div class="container-fluid hidden-print">
    <div class="row ">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left" style="padding-top: 15px;">Generate Merit-List<h4>
                <div class="btn-group pull-right">
                    <a type="button" class="btn btn-success float-right" href="{{ url('management/printmeritlist') }}">Back to Dashboard</a>
                </div>
            </div>
            <div class="panel-body">
                <form id="meritListInput" method="POST">
                {{ csrf_field() }}
                    <div class="col-md-2">
                        <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <select id="category" name="category" class="form-control" required>
                                    <option value="">--Select Category--</option>
                                    <option value="All" >All</option>
                                    <option value="General" >General</option>
                                    <option value="Physically Challange">Physically Challange</option>
                                    
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group{{ $errors->has('board') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <select name="board" class="form-control" required id="board">
                                    <option value="">--Select Board--</option>
                                    <option  value="WBSCVE & T">WBSCVE & T</option>
                                    
                                </select>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group{{ $errors->has('sport') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <select name="sport" class="form-control" required id="sport">
                                    <option value="">--Sport Quota--</option>
                                    <option value="All">All</option>
                                    <option value="Yes">Yes</option>
                                    <option  value="No">No</option>                
                                   
                                </select>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="col-md-12">
                                <input id="name" type="number" min="1" class="form-control" name="from_limit" placeholder="Limit From" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="col-md-12">
                                <input id="name" type="number" class="form-control" name="to_limit" placeholder="Limit To" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <div class="col-md-12">
                                <input id="submit" type="submit" class="btn btn-primary" value="Submit">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
       
    </div>


</div>

<table style="width: 100%;">
        <thead>
            <th>
                <div class="row header" style="font-weight: 600 !important;">
                    
                       <div class="title text-center">
                    <div class="col-md-12"> 
                       <h2 style="text-align:center;font-family: 'Algerian';">BIDHAN CHANDRA KRISHI VISWAVIDYALAYA</h2>

                      <h3 id="heading" style="text-align: center;"></h3>
                    </div>
                  </div>
                  
                </div>
            </th>
        </thead>
        <tbody>
            <tr><td>
                <div class="row">
        <div  class="col-md-12">
        <div class="table-responsive">
            <table class="table" id="app_table_box">
              <thead>
                
                 <tr>
                  <th>1</th>
                  <th >2</th>
                  <th>3</th>
                  <th >4</th>
                  <th >5</th>
                  <th >6</th>
                  <th>7</th>
                  <th>8</th>
                  <th>9</th>
                  <th>10</th>
                  <th>11</th>
                  
                  <th >12</th>
                  <th >13</th>
                  <th>14</th>
                  <th >15</th>
                  <th>16</th>
                  <th>17</th>
                  
                  

                </tr>
                <tr>
                  
                  <th rowspan="2">&nbsp;<span id="category_selected"></span> Rank</th>
                  
                  <th rowspan="2">Application No.</th>
                  <th rowspan="2">Candidate's Name</th>
                  <th rowspan="2">Guardian's Name</th>
                  <th rowspan="2">Category</th>
                  <th rowspan="2">DOB</th>

                  <th colspan="5">H.S. Subjects</th>
                  
                  <th rowspan="2">% of Five Subjects(H.S.)</th>
                  <th rowspan="2">Marks Obtained in Secondary Examination</th>
                  <th rowspan="2">% of Marks of Secondary Examination</th>

                  <th rowspan="2">80% on Column No. 12</th>
                  <th rowspan="2">20% on Column No. 14</th>
                  <th rowspan="2"><b>Marks Index (15+16)</b></th>
                  
                  
                  
                  

                </tr>
                <tr>

                  
                  <th>ENG</th>
                  <th>BIO</th>
                  <th>PHY/CHEM</th>
                  <th>VOC.PAPER-I</th>
                  <th>VOC.PAPER-II</th>
                  
                  

                </tr>
              </thead>
              <tbody id="append_data">
              </tbody>
            </table>
        </div>
    </div>
</div>
</td>
            </tr>
        </tbody>
        <tfoot>
            {{-- <tr>
                <td style="text-align:center; font-size:30px;">
                    <div class="row" id="break">
                      Sl. No. 
                    </div>
                 </td>
             </tr> --}}
        </tfoot>
    </table>
{{-- <div class="row hidden-print" style="margin-top: 20px">
    <div class="col-md-10">
    </div>
    <div class="col-md-2">
        
    </div>
</div> --}}
<script type="text/javascript">

$(document).ready(function(){
   

    $('#meritListInput').on('submit',function(e) {
    e.preventDefault();
    


    $.ajax({
        url: '{!! url('management/meritlist') !!}',
        type: 'POST',
        dataType: 'json',
        data: $(this).serialize() ,
        // beforeSend: function() {
        //     $("#loading").show();
        //     $(".app").hide();
        // },
    })
    .done(function(data) {

      category = $("#category").val();
      heading_board = $("#board").val();

      
    if (category == "All") {
        heading_category = "COMBINED List";
        heading = 'Provisional Merit List For Admission to B.Sc.(Hons.) Agriculture and Horticulture Course:2020-21 '+ heading_board+' Board '+heading_category+' (Category:- General, SC,ST,OBC-A & OBC-B)';
    }

    else {

      heading_category = category;
        heading = 'Provisional Merit List For Admission to B.Sc.(Hons.) Agriculture and Horticulture Course:2020-21 '+ heading_board +' Board ('+ heading_category+' Merit List)';

    }
    
    

    // heading = 'Provetional Merit List For Admission to B.Sc.(Hons.) Agriculture and Horticulture Course:2020-21 '+ heading_category +' (Category:- General, SC,ST,OBC-A & OBC-B) of '+ heading_board+' Board';
    $("#heading").text(heading)
        // $("#loading").hide();
        // $(".app").show();
        console.log(data.user);
        td_data = "";
        $.each(data.user, function(index, value) {

          td_data += "<tr>";
          td_data += "<td>"+ value.rank +"</td>";
          td_data += "<td>"+ value.application_no +"</td>";
          td_data += "<td>"+ value.name +"</td>";
          td_data += "<td>"+ value.guardian_name +"</td>";
          td_data += "<td>"+ value.category +"</td>";
          td_data += "<td>"+ value.date_birth +"</td>";
          td_data += "<td>"+ value.mrk1 +"</td>";
          td_data += "<td>"+ value.mrk2 +"</td>";
          td_data += "<td>"+ value.mrk3 +"</td>";
          td_data += "<td>"+ value.mrk4 +"</td>";
          td_data += "<td>"+ value.mrk5 +"</td>";
          td_data += "<td>"+ value.percentage +"</td>";
          td_data += "<td>"+ value.class10mrk +"</td>";
          td_data += "<td>"+ value.classten_percentage +"</td>";
          td_data += "<td>"+ value.classtwelve_weightage_final +"</td>";
          td_data += "<td>"+ value.classten_weightage_final +"</td>";

          td_data += "<td><b>"+ value.mark_index +"</b></td>";

          td_data += "</tr>"
          $("#append_data").html(td_data)
        });
        
    })
    .fail(function(xhr) {
        // if(xhr.status===403 || xhr.status===401){
        //     alert('Your Session Expire');
        //     window.location.reload();
        // }else
        //     alert(xhr.responseText);
        console.log(xhr);
    })
});

});



</script>



<script>
    function myPrint() {
        window.print();
    }
</script>

@endsection
