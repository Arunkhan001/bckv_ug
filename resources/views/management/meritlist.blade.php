@extends('management.layout.auth')
@section('content')
<style>
    td{
        color: #3b67af;
        font-weight: 600;
        
    }
    th{
        color: #663300;
    }
</style>
<div class="container-fluid hidden-print">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left" style="padding-top: 15px;">Generate Merit-List<h4>
                <div class="btn-group pull-right">
                    <a type="button" class="btn btn-success float-right" href="{{ url('management/home') }}">Back to Dashboard</a>
                </div>
            </div>
            <div class="panel-body">
                <form id="meritListInput" method="POST">
                {{ csrf_field() }}
                    <div class="col-md-2">
                        <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <select id="category" name="category" class="form-control" required>
                                    <option value="">--Select Category--</option>
                                    <option value="All" >All</option>
                                    <option value="General" >General</option>
                                    <option value="SC">SC</option>
                                    <option value="ST">ST</option>
                                    <option value="OBC-A">OBC-A</option>
                                    <option value="OBC-B">OBC-B</option>
                                    <option value="Physically Challange">Physically Challange</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group{{ $errors->has('board') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <select name="board" class="form-control" required>
                                    <option value="">--Select Board--</option>
                                    <option value="All">All</option>
                                    <option  value="WBCHSE">WBCHSE</option>                
                                    <option  value="Other Board">Other Board</option>
                                    <option  value="WBSCVE & T">WBSCVE & T</option>
                                </select>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group{{ $errors->has('sport') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <select name="sport" class="form-control" required>
                                    <option value="">--Sport Quota--</option>
                                    <option value="All">All</option>
                                    <option value="Yes">Yes</option>
                                    <option  value="No">No</option>                
                                   
                                </select>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="col-md-12">
                                <input id="name" type="number" min="1" class="form-control" name="from_limit" placeholder="Limit From" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="col-md-12">
                                <input id="name" type="number" class="form-control" name="to_limit" placeholder="Limit To" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <div class="col-md-12">
                                <input id="submit" type="submit" class="btn btn-primary" value="Submit">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
        <form id="bulkprint" action="{{ url('management/bulkprint') }}" method="POST" target="_blank">
                {{ csrf_field() }}
            <div class="col-md-2">
                <div class="form-group">
                    <input class="students_id" type="hidden" name="students_id">
                    <input id="submit" style="margin-left: 5px" type="submit" value="Print Application Form" class="btn btn-primary">
                </div>
            </div>
        </form>
        <form class="certificates" action="{{ url('management/certificate') }}" method="POST" target="_blank">
                {{ csrf_field() }}
            <div class="col-md-2">
                <div class="form-group">
                    <input type="hidden" name="case" value="caste">
                    <input class="students_id" type="hidden" name="students_id">
                    <input id="submit" style="margin-left: 5px" type="submit" value="Print Caste Certificate" class="btn btn-primary">
                </div>
            </div>
        </form>
        <form class="certificates" action="{{ url('management/certificate') }}" method="POST" target="_blank">
                {{ csrf_field() }}
            <div class="col-md-2">
                <div class="form-group">
                    <input type="hidden" name="case" value="sport">
                    <input class="students_id" type="hidden" name="students_id">
                    <input id="submit" style="margin-left: 5px" type="submit" value="Print Sport Certificate" class="btn btn-primary">
                </div>
            </div>
        </form>
        <form class="certificates" action="{{ url('management/certificate') }}" method="POST" target="_blank">
                {{ csrf_field() }}
            <div class="col-md-2">
                <div class="form-group">
                    <input type="hidden" name="case" value="10thmarksheet">
                    <input class="students_id" type="hidden" name="students_id">
                    <input id="submit" style="margin-left: 5px" type="submit" value="Print 10th Marksheet" class="btn btn-primary">
                </div>
            </div>
        </form>

        <form class="certificates" action="{{ url('management/certificate') }}" method="POST" target="_blank">
                {{ csrf_field() }}
            <div class="col-md-2">
                <div class="form-group">
                    <input type="hidden" name="case" value="marksheet">
                    <input class="students_id" type="hidden" name="students_id">
                    <input id="submit" style="margin-left: 5px" type="submit" value="Print 12th Marksheet" class="btn btn-primary">
                </div>
            </div>
        </form>
        <form class="certificates" action="{{ url('management/certificate') }}" method="POST" target="_blank">
                {{ csrf_field() }}
            <div class="col-md-2">
                <div class="form-group">
                    <input type="hidden" name="case" value="birth_certificate">
                    <input class="students_id" type="hidden" name="students_id">
                    <input id="submit" style="margin-left: 5px" type="submit" value="Print Birth Cert." class="btn btn-primary">
                </div>
            </div>
        </form>

        <form class="certificates" action="{{ url('management/certificate') }}" method="POST" target="_blank">
                {{ csrf_field() }}
            <div class="col-md-2">
                <div class="form-group">
                    <input type="hidden" name="case" value="aadhaar">
                    <input class="students_id" type="hidden" name="students_id">
                    <input id="submit" style="margin-left: 5px" type="submit" value="Print Aadhaar Card" class="btn btn-primary">
                </div>
            </div>
        </form>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div style="overflow-x:auto;">
                
                <table id="box_table" class="table table-striped table-bordered">
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row hidden-print" style="margin-top: 20px">
    <div class="col-md-10">
    </div>
    <div class="col-md-2">
        
    </div>
</div>
<script type="text/javascript">

$(document).ready(function(){
    $("#bulkprint").hide();
    $(".certificates").hide();

    $('#meritListInput').on('submit',function(e) {
    e.preventDefault();
    $('#bulkprint').show('slow/400/fast');
    $('.certificates').show('slow/400/fast');


    $.ajax({
        url: '{!! url('management/meritlist') !!}',
        type: 'POST',
        dataType: 'json',
        data: $(this).serialize() ,
        beforeSend: function() {
            $("#loading").show();
            $(".app").hide();
        },
    })
    .done(function(data) {
        $("#loading").hide();
        $(".app").show();
        dataTableInitialization(data ,"all");
        console.log(data['user']);
        $('.students_id').val(data['students_id']);
        
    })
    .fail(function(xhr) {
        // if(xhr.status===403 || xhr.status===401){
        //     alert('Your Session Expire');
        //     window.location.reload();
        // }else
        //     alert(xhr.responseText);
        console.log(xhr);
    })
});

});


    function dataTableInitialization(data) 
    {
        if ( $.fn.DataTable.isDataTable('#box_table') ) {
        $('#box_table').DataTable().destroy();
    }

    var btn_admit =  '<button class="btn btn-success btn-sm"  data-toggle="tooltip">Birth Certificate</button>';
     var btn_marksheet_ten =  '<button class="btn btn-danger btn-sm"  data-toggle="tooltip">10th Marksheet</button>';
    var btn_marksheet =  '<button class="btn btn-primary btn-sm"  data-toggle="tooltip">12th Marksheet</button>';
    var btn_certificate = '--';
    if(data['category']!="General") {
        btn_certificate = '<button class="btn btn-default btn-sm"  data-toggle="tooltip">Cast Cert.</button>';
    }
    var btn_sport_quota =  '<button class="btn btn-warning btn-sm"  data-toggle="tooltip">Sport Cert.</button>';
    var board = data['board'];
    var category = data['category'];
    if(board == 'all' || board == 'WBCHSE' || board == 'Other Board' || board == 'All')
    {
        if(category == 'SC' || category == 'ST' || category == 'OBC-A' || category == 'OBC-B')
        {
            tab = "<thead><tr><th>Rank</th><th>Application No.</th><th>Name</th><th>Guardian Name</th><th>General Rank</th><th>Category</th><th>Sport</th><th>DOB</th><th>English_FM</th><th>Physics_FM</th><th>Chemistry_FM</th><th>Biology_FM</th><th>English</th><th>Physics</th><th>Chemistry</th><th>Biology</th><th>% in HS</th><th>PCB </th><th>HS Total</th><th>10th Total</th><th>Obtained in Secondary</th><th>% in Secondary</th><th>10th Weightage</th><th>12th Weightage</th><th>Marks Index</th><th class='text-center no-export'>Birth Certificate</th><th class='text-center no-export'>10th Marksheet</th><th class='text-center no-export'>Marksheet</th><th class='text-center no-export'>Certificate</th><th class='text-center no-export'>Sport Certificate</th></tr></thead>";
            data_user = data['user'];
            data_value = [
                    {"data": "rank"},
                    {"data": "application_no"},
                    {"data": "name"},
                    {"data": "guardian_name"},
                    {"data": "general_rank"},
                    {"data": "category"},
                    {"data": "sport_quota"},
                    {"data": "date_birth"},
                    
                    {"data": "fm4"},
                    {"data": "fm1"},
                    {"data": "fm2"},
                    {"data": "fm3"},
                    {"data": "mrk4"},
                    {"data": "mrk1"},
                    {"data": "mrk2"},
                    {"data": "mrk3"},
                    {"data": "percentage"},
                    {"data": "average"},
                    {"data": "total_mark"},
                    {"data": "class10fm"},
                    {"data": "class10mrk"},
                    {"data": "classten_percentage"},
                    {"data": "classten_weightage_final"},
                    {"data": "classtwelve_weightage_final"},
                    {"data": "mark_index"},
                    
                    {
                        "orderable": false,
                        "data": null,
                        "defaultContent" : btn_admit,
                    },
                    {
                        "orderable": false,
                        "data": null,
                        "defaultContent" : btn_marksheet_ten,
                    },
                    {
                        "orderable": false,
                        "data": null,
                        "defaultContent" : btn_marksheet,
                    },{
                        "orderable": false,
                        "data": null,
                        "render" : function (data, type, row) {
                            if(data['category'] =='SC' || data['category'] =='ST' || data['category'] =='OBC-A' || data['category'] =='OBC-B' || data['category'] =='Physically Challange'){
                                return btn_certificate;
                            }else {
                                return "--";
                            }
                        }
                    },{
                        "orderable": false,
                        "data": null,
                        "render" : function (data_user, type, row) {
                            if(data_user['sport_quota'] == "Yes"){
                                return btn_sport_quota;
                            }else {
                                return "--";
                            }
                        }
                    }
                ];
        }
        else
        {
            /*if(category == 'SC' || category == 'ST' || category == 'OBC-A' || category == 'OBC-B')
            {
                tab = "<thead><tr><th>Rank</th><th>Application No.</th><th>Name</th><th>Guardian Name</th><th>General Rank</th><th>Category</th><th>DOB</th><th>Physics</th><th>Chemistry</th><th>Biology</th><th>English</th><th>Total</th><th>PCB </th><th>Marks Index</th><th class='text-center no-export'>Admit</th><th class='text-center no-export'>Marksheet</th><th class='text-center no-export'>Certificate</th></tr></thead>";

                data_value = [
                        {"data": "rank"},
                        {"data": "application_no"},
                        {"data": "name"},
                        {"data": "guardian_name"},
                        {"data": "general_rank"},
                        {"data": "category"},
                        {"data": "date_birth"},
                        {"data": "mrk1"},
                        {"data": "mrk2"},
                        {"data": "mrk3"},
                        {"data": "mrk4"},
                        {"data": "total_mark"},
                        {"data": "average"},
                        {"data": "percentage"},
                        {
                            "orderable": false,
                            "data": null,
                            "defaultContent" : btn_admit,
                        },
                        {
                            "orderable": false,
                            "data": null,
                            "defaultContent" : btn_marksheet,
                        },{
                            "orderable": false,
                            "data": null,
                            "render" : function (data, type, row) {
                                if(data['certificate_number'] || data['category'] =='Sport'){
                                    return btn_certificate;
                                }else {
                                    return "--";
                                }
                            }
                        }
                    ];
            }
            else
            {*/
                tab = "<thead><tr><th>Rank</th><th>Application No.</th><th>Name</th><th>Guardian Name</th><th>Category</th><th>Sport</th><th>DOB</th><th>English_FM</th><th>Physics_FM</th><th>Chemistry_FM</th><th>Biology_FM</th><th>English</th><th>Physics</th><th>Chemistry</th><th>Biology</th><th>% in HS</th><th>PCB </th><th>HS Total</th><th>10th Total</th><th>Obtained in Secondary</th><th>% in Secondary</th><th>10th Weightage</th><th>12th Weightage</th><th>Marks Index</th><th class='text-center no-export'>Birth Certificate</th><th class='text-center no-export'>10th Marksheet</th><th class='text-center no-export'>Marksheet</th><th class='text-center no-export'>Certificate</th><th class='text-center no-export'>Sport Certificate</th></tr></thead>";
                data_user = data['user'];
                data_value = [
                        {"data": "rank"},
                        {"data": "application_no"},
                        {"data": "name"},
                        {"data": "guardian_name"},
                        {"data": "category"},
                        {"data": "sport_quota"},
                        {"data": "date_birth"},
                        
                        {"data": "fm4"},
                        {"data": "fm1"},
                        {"data": "fm2"},
                        {"data": "fm3"},
                        {"data": "mrk4"},
                        {"data": "mrk1"},
                        {"data": "mrk2"},
                        {"data": "mrk3"},
                        {"data": "percentage"},
                        {"data": "average"},
                        {"data": "total_mark"},
                        {"data": "class10fm"},
                        {"data": "class10mrk"},
                        {"data": "classten_percentage"},
                        {"data": "classten_weightage_final"},
                        {"data": "classtwelve_weightage_final"},
                        {"data": "mark_index"},
                        {
                            "orderable": false,
                            "data": null,
                            "defaultContent" : btn_admit,
                        },
                        {
                        "orderable": false,
                        "data": null,
                        "defaultContent" : btn_marksheet_ten,
                        },
                        {
                            "orderable": false,
                            "data": null,
                            "defaultContent" : btn_marksheet,
                        },{
                            "orderable": false,
                            "data": null,
                            "render" : function (data, type, row) {
                                if(data['category'] =='SC' || data['category'] =='ST' || data['category'] =='OBC-A' || data['category'] =='OBC-B' || data['category'] =='Physically Challange'){
                                    return btn_certificate;
                                }else {
                                    return "--";
                                }
                            }
                        },{
                            "orderable": false,
                            "data": null,
                            "render" : function (data_user, type, row) {
                                if(data_user['sport_quota'] == "Yes"){
                                    return btn_sport_quota;
                                }else {
                                    return "--";
                                }
                            }
                        }
                    ];
            //}
            
        } 
    }
    else
    {
        if(category == 'SC' || category == 'ST' || category == 'OBC-A' || category == 'OBC-B') {
             tab = "<thead><tr><th>Rank</th><th>Application No. </th><th>Name</th><th>Guardian Name</th><th>General Rank</th><th>Category</th><th>Sport</th><th>DOB</th><th>ENG_FM</th><th>BIO_FM</th><th>PHY/CHEM_FM</th><th>VOC. PAPER-I_FM</th><th>VOC. PAPER-II_FM</th><th>ENG</th><th>BIO</th><th>PHY/CHEM</th><th>VOC. PAPER-I</th><th>VOC. PAPER-II</th><th>% in HS</th><th>(BIO+PHY/CHEM)</th><th>P-I+P-II</th><th>HS Total</th><th>10th Total</th><th>Obtained in Secondary</th><th>% in Secondary</th><th>10th Weightage</th><th>12th Weightage</th><th>Marks Index</th><th class='text-center no-export'>Birth Certificate</th><th class='text-center no-export'>10th Marksheet</th><th class='text-center no-export'>Marksheet</th><th class='text-center no-export'>Certificate</th><th class='text-center no-export'>Sport Certificate</th></tr></thead>";

data_user = data['user'];
data_value = [
        {"data": "rank"},
        {"data": "application_no"},
        {"data": "name"},
        {"data": "guardian_name"},
        {"data": "general_rank"},
        {"data": "category"},
        {"data": "sport_quota"},
        {"data": "date_birth"},
        
        {"data": "fm1"},
        {"data": "fm2"},
        {"data": "fm3"},
        {"data": "fm4"},
        {"data": "fm5"},
        {"data": "mrk1"},
        {"data": "mrk2"},
        {"data": "mrk3"},
        {"data": "mrk4"},
        {"data": "mrk5"},
        {"data": "percentage"},
        {"data": "average"},
        {"data": "average1"},
        {"data": "total_mark"},
        {"data": "class10fm"},
        {"data": "class10mrk"},
        {"data": "classten_percentage"},
        {"data": "classten_weightage_final"},
        {"data": "classtwelve_weightage_final"},
        {"data": "mark_index"},
        {
            "orderable": false,
            "data": null,
            "defaultContent" : btn_admit,
        },
         {
            "orderable": false,
            "data": null,
            "defaultContent" : btn_marksheet_ten,
        },
        {
            "orderable": false,
            "data": null,
            "defaultContent" : btn_marksheet,
        },
        {
            "orderable": false,
            "data": null,
            "render" : function (data, type, row) {
                if(data['category'] =='SC' || data['category'] =='ST' || data['category'] =='OBC-A' || data['category'] =='OBC-B' || data['category'] =='Physically Challange'){
                    return btn_certificate;
                }else {
                    return "--";
                }
            }
        },{
            "orderable": false,
            "data": null,
            "render" : function (data_user, type, row) {
                if(data_user['sport_quota'] == "Yes"){
                    return btn_sport_quota;
                }else {
                    return "--";
                }
            }
        }
];

        }else{
        tab = "<thead><tr><th>Rank</th><th>Name</th><th>Guardian Name</th><th>Application No. </th><th>Category</th><th>Sport</th><th>DOB</th><th>ENG_FM</th><th>BIO_FM</th><th>PHY/CHEM_FM</th><th>VOC. PAPER-I_FM</th><th>VOC. PAPER-II_FM</th><th>ENG</th><th>BIO</th><th>PHY/CHEM</th><th>VOC. PAPER-I</th><th>VOC. PAPER-II</th><th>% in HS</th><th>(BIO+PHY/CHEM)</th><th>P-I+P-II</th><th>HS Total</th><th>10th Total</th><th>Obtained in Secondary</th><th>% in Secondary</th><th>10th Weightage</th><th>12th Weightage</th><th>Marks Index</th><th class='text-center no-export'>Birth Certificate</th><th class='text-center no-export'>10th Marksheet</th><th class='text-center no-export'>Marksheet</th><th class='text-center no-export'>Certificate</th><th class='text-center no-export'>Sport Certificate</th></tr></thead>";

        data_user = data['user'];
        data_value = [
                {"data": "rank"},
                {"data": "name"},
                {"data": "guardian_name"},
                {"data": "application_no"},
                {"data": "category"},
                {"data": "sport_quota"},
                {"data": "date_birth"},
                
                {"data": "fm1"},
                {"data": "fm2"},
                {"data": "fm3"},
                {"data": "fm4"},
                {"data": "fm5"},
                {"data": "mrk1"},
                {"data": "mrk2"},
                {"data": "mrk3"},
                {"data": "mrk4"},
                {"data": "mrk5"},
                {"data": "percentage"},
                {"data": "average"},
                {"data": "average1"},
                {"data": "total_mark"},
                {"data": "class10fm"},
                {"data": "class10mrk"},
                {"data": "classten_percentage"},
                {"data": "classten_weightage_final"},
                {"data": "classtwelve_weightage_final"},
                {"data": "mark_index"},
                {
                    "orderable": false,
                    "data": null,
                    "defaultContent" : btn_admit,
                },
                 {
                        "orderable": false,
                        "data": null,
                        "defaultContent" : btn_marksheet_ten,
                    },
                {
                    "orderable": false,
                    "data": null,
                    "defaultContent" : btn_marksheet,
                },
                {
                    "orderable": false,
                    "data": null,
                    "render" : function (data, type, row) {
                        if(data['category'] =='SC' || data['category'] =='ST' || data['category'] =='OBC-A' || data['category'] =='OBC-B' || data['category'] =='Physically Challange'){
                            return btn_certificate;
                        }else {
                            return "--";
                        }
                    }
                },{
                    "orderable": false,
                    "data": null,
                    "render" : function (data_user, type, row) {
                        if(data_user['sport_quota'] == "Yes"){
                            return btn_sport_quota;
                        }else {
                            return "--";
                        }
                    }
                }
        ];
        }
    }

    $("#box_table").html(tab);
    
    box_table_all = $("#box_table").DataTable({

        "dom" : "<'row'<'col-sm-4' l><'col-sm-4' B> <'col-sm-4 search' f>>t<'row' <'col-sm-6' i><'col-sm-6' p>>",
        "data" : data['user'],
        "pageLength": 50,
        "lengthMenu": [ 10, 50 ,100, 500, 1000, 5000],
        fixedHeader: true,
        "scrollY": "500px",
        "autoWidth":true,

        "columns": data_value,

        // "order": [[ 1, "asc" ]],
        "buttons" : [
                "copy", 
                {
                    "extend" : 'excel',
                    "title" : 'Merit-List',
                    "exportOptions": {
                        "columns": "thead th:not(.no-export)",
                    },
                },
                {
                    "extend" : 'pdf',
                    "title" : 'Merit-List',
                    "exportOptions": {
                        "columns": "thead th:not(.no-export)",
                    },
                },
                {
                    "extend" : 'print',
                    // "text": 'Print current page',
                    "title" : 'Merit-List',
                    "exportOptions": {
                        // "columns": [0,1,2,3,4,5,6,7,8],
                        "columns": "thead th:not(.no-export)",
                    },
                },
                'colvis',
        ],
        "language" : {
            "lengthMenu": 'Entries _MENU_  per page',
            "search" : '_INPUT_  ',
            "searchPlaceholder": "Search any field",
            "zeroRecords": "No matching List record found",
        },
        "ordering": false,
    });
}

$("#box_table").on('click', '.btn', function() {
    $("#box_table tr").removeClass("selected");
    $(this.closest('tr')).addClass("selected");
})


$("#box_table").on('click', '.btn-success', function() {

    var data = box_table_all.row( this.closest('tr')).data();

    var div = `<img src=" {{ url('management/image/') }}/${data['app_no']}/birth_certificate.jpg" width="100%">`;
    $("#modal_img").html(div);
    $("#image_Modal").modal('show');

});

$("#box_table").on('click', '.btn-danger', function() {

    var data = box_table_all.row( this.closest('tr')).data();

    var div = `<img src=" {{ url('management/image/') }}/${data['app_no']}/10th_marksheet_font.jpg" width="100%">`;
    $("#modal_img").html(div);
    $("#image_Modal").modal('show');

});

$("#box_table").on('click', '.btn-primary', function() {

    var data = box_table_all.row( this.closest('tr')).data();

    var div = `<img src=" {{ url('management/image/') }}/${data['app_no']}/marksheet.jpg" width="100%">`;
    $("#modal_img").html(div);
    $("#image_Modal").modal('show');

});

$("#box_table").on('click', '.btn-default', function() {

var data = box_table_all.row( this.closest('tr')).data();
if(data['category']=='General'){
    return false;
}
var div = `<img src=" {{ url('management/image/') }}/${data['app_no']}/certificate.jpg" width="100%">`;
$("#modal_img").html(div);
$("#image_Modal").modal('show');

});

$("#box_table").on('click', '.btn-warning', function() {

var data = box_table_all.row( this.closest('tr')).data();
// if(data['category']=='General'){
//     return false;
// }
var div = `<img src=" {{ url('management/image/') }}/${data['app_no']}/sport_certificate.jpg" width="100%">`;
$("#modal_img").html(div);
$("#image_Modal").modal('show');

});

</script>

<div class="modal fade" id="image_Modal" role="dialog" tabindex='-1'>
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header hidden-print">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="hidden-print" style="text-align: center; margin-bottom: 30px;">
            <button class="btn btn-primary" onclick="myPrint()">Print this page</button>
        </div>
        <div class="modal-body" id="modal_img">
         
        </div>
        <div class="modal-footer hidden-print">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>

<script>
    function myPrint() {
        window.print();
    }
</script>

@endsection
