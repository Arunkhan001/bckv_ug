<html>
    <title>{{ config('app.name', 'BCKV') }}</title>
    <body onload="document.frm1.submit()">
        <form action="{{ url('management/bulkprint') }}" name="frm1" method="POST">
            {{ csrf_field() }}
            <input id="students_id" type="hidden" name="students_id" value="{{ app('request')->input('a') }}">
        </form>
    </body>
</html>