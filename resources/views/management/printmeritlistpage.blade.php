@extends('management.layout.auth')

@section('content')

<hr/>
<div style="min-height: 400px;" align="center" class="row">
    
    <div class="col-md-2">
<div class="btn-group pull-right">
                    <a type="button" class="btn btn-success float-right" href="{{ url('management/home') }}">Back to Dashboard</a>
                </div>
    </div>
    <div class="col-md-6">
        <div class="list-group">
            <a href="{{ url('/management/meritlistWBCHSEOTHERS') }}" class="list-group-item">
            <h4>COMBINED , GENERAL & PH List for WBCHSE / OTHERS Board</h4></a>
        </div>

        <div class="list-group">
            <a href="{{ url('/management/meritlistWBCHSEOTHERScategory') }}" class="list-group-item">
            <h4>CATEGORY wise List for WBCHSE / OTHERS Board</h4></a>
        </div>

        <div class="list-group">
            <a href="{{ url('/management/meritlistvocational') }}" class="list-group-item">
            <h4>COMBINED , GENERAL & PH List for WBSCVE & T Board</h4></a>
        </div>

        <div class="list-group">
            <a href="{{ url('/management/meritlistvocationalcategory') }}" class="list-group-item">
            <h4>CATEGORY wise List for WBSCVE & T Board</h4></a>
        </div>
        

        
    </div>
    <div class="col-md-4">


    </div>
    
</div>

@endsection