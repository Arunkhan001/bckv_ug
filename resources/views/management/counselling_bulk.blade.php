 @extends('management.layout.auth')
@section('content')


 <form id="bulkprint" action="{{ url('management/counselling_bulkprint') }}" method="POST" target="_blank">
                {{ csrf_field() }}


                <div class="row">
      <div class="col-sm-2">
      </div>
			<div class="col-sm-4">
      <div class="form-group">
            <label>Select Board</label>
              <select name="board_name" id="board_name" class="form-control">
                <option value="-1">Select Board</option>
                <option value="WBCHSE">WBCHSE</option>
                <option value="Other Board">Other Board</option>
                <option value="WBSCVE & T">WBSCVE & T</option>
                 
              </select>
          </div>
			</div>

      <div class="col-sm-4">
      <div class="form-group">
            <label>Select Category</label>
              <select name="category" id="category" class="form-control">
                <option value="-1">Select Category</option>
                <option value="Unreserved">Unreserved</option>
                <option value="SC">SC</option>
                <option value="ST">ST</option>
                <option value="OBC-A">OBC-A</option>
                <option value="OBC-B">OBC-B</option>
                <option value="Physically Challange">Physically Challange</option>
                
                 
              </select>
          </div>
      </div>
      <div class="col-sm-2">

      </div>
    </div>



            <div class="col-md-2">
                <div class="form-group">
                    
                    <input id="submit" style="margin-left: 5px" type="submit" value="Print Application Form" class="btn btn-primary">
                </div>
            </div>
        </form>


        @endsection