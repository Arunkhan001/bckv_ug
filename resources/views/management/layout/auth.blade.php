<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:title" content="Online Admission"/>
    <meta property="og:site_name" content="Bidhan Chandra Krishi Viswavidyalaya Online Admission">
    <meta property="og:type" content="summary"/>
    <meta property="og:image" content="{{ url('images/bckv-logo.png') }}" />
    <meta property="og:url" content="{{ url('/') }}"/>
    <meta property="og:description" content="Bidhan Chandra Krishi Viswavidyalaya Online Admission for B.Sc.(Hons) Agril. & B.Sc(Hons) Hort. Courses for Mohanpur, Burdwan & Chanta(Susunia, Bankura) Campus for Academic session {{ env('ADMISSION_SESSION') }}. " />

    <meta name="keywords" content="online, BCKV, bckv,admission online,ug admission,agriculture, admission, bidhan chandra,krishi viswavidyalaya,agriculture admission, B.Sc.(Hons), mohanpur,Hort admission,Hort, mohanpur campus, burdwan, online admission,bidhan chandra krishi viswavidyalaya, Dos,dos, dos product, dos infotech product ">
    <meta name="robots" content="index, follow">
    <meta name="googlebot" content="index, follow">
    <meta name="msnbot" content="index, follow">

    <meta name="description" content="Bidhan Chandra Krishi Viswavidyalaya Online Admission for B.Sc.(Hons) Agril. & B.Sc(Hons) Hort. Courses for Mohanpur, Burdwan & Chanta(Susunia, Bankura) Campus for Academic session {{ env('ADMISSION_SESSION') }}">
    <meta name="author" content="Dos Infotech">
    <meta name="copyright" content="Dos Infotech">
    <meta name="document-distribution" content="Global">

    <meta name="theme-color" content="#d9edf7">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'BCKV') }}</title>
    <link rel="icon" href="{{ asset('images/bckv-logo.png') }}" type="image/png" sizes="16x16">
    <link rel="icon" href="{{ asset('images/bckv-logo.png') }}" type="image/png" sizes="32x32">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datatables.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">
    @yield('css')
    <style>
        * , body {
            font-family: 'Open Sans' !important;
        }
        @media only screen and (max-width:480px){
            .navbar-brand {
                font-size: 12px !important;
            }
        }
        textarea {
            resize:none
        }
        table tbody tr.selected {
			background-color: #0aa89e !important;
			color: #ffffff;
		}
    </style>
    <script src="{{ asset('js/app.js') }}"></script>

    <script>
        function inIframe () {
            try {
                return window.self !== window.top;
            } catch (e) {
                return true;
            }
        }
    </script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116983977-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-116983977-1');
    </script>
</head>
<body>
<div style="display: none; text-align: center; margin-top: 150px;" id="loading">
    <img src="{{ asset('images/loading.gif') }}">
</div>
<div class="app">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/management') }}">
                    {{ config('app.name', 'Laravel Multi Auth Guard') }}: Management
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/management/login') }}">Login</a></li>
                        {{-- <li><a href="{{ url('/management/register') }}">Register</a></li> --}}
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ url('/management/logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/management/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')
    <script src="{{ asset('js/jquery-ui.min.js') }}"></script>

    <script src="{{ asset('js/datatables.min.js') }}"></script>
    <script src="{{ asset('js/jszip.min.js') }}"></script>
    <script src="{{ asset('js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/buttons.html5.min.js') }}"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    @yield('js')
</div>
</body>
<script>
    if(inIframe ()) {
        document.body.innerHTML = '<h1>Please Open a new tab and past this url : <a href="https://bckvadmn.in">https://bckvadmn.in</a> </h1>';
    }
</script>
</html>
