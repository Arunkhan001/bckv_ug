@extends('management.layout.auth')
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title pull-left">Get Student Information<h4>
                    {{-- <div class="btn-group pull-right">
                        <a type="button" class="btn btn-success float-right" href="{{ url('management/home') }}">Back to Dashboard</a>
                    </div> --}}
                </div>
                <div class="panel-body">
                    <form>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="app_no" id="app_no" value="@if(!empty($student)){{$student->id}}@endif" placeholder="Application No. i.e. 12345" />
                        </div>
                        <div class="col-sm-7">
                            <input id="submit" type="submit" class="btn btn-primary" value="Get Information">
                            @if(!empty ( $student ) && $student->status>3)
                            <input type="button" id="print" class="btn btn-success" onclick="printed()" value="View Application">
                            <script>
                                function printed(){
                                    window.open("{{ url('management/singleprint') }}?a={{$student->id}}", "Application Preview", "toolbar=no,scrollbars=yes,resizable=yes,top=10,left=10,width=1300,height=600");
                                }  
                            </script>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-12" style="min-height: 300px">
            @if(!empty ( $student ))
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title pull-left">Student Personal Details [ U{{ $student->id }} ] <h4>
                </div>
                <div class="panel-body">
                    <div class="tabtable-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Father Name</th>
                                <th>Mother Name</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Category</th>
                                <th>DOB</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ $student->name}}</td>
                                <td>{{ $student->father_name}}</td>
                                <td>{{ $student->mother_name}}</td>
                                <td>{{ $student->email}}</td>
                                <td>{{ $student->mobile_number}}</td>
                                <td>{{ $student->category}}</td>
                                <td>{{ $student->date_of_birth}}</td>
                                @if(empty($student->status))
                                    <td> Only Registered, {{ $student->created_at }}</td> 
                                @elseif($student->status==1)
                                    <td class="warning">Personal Info</td>
                                @elseif($student->status==2)
                                    <td class="warning">Personal, Academic</td>
                                @elseif($student->status==3)
                                    <td class="danger">Waiting for Final submit</td>
                                @elseif($student->status==4)
                                    <td class="danger">Payment Due</td>
                                @elseif($student->status==5)
                                    <td class="warning">Payment Due [ {{$payment->t_type}} ] </td>
                                @elseif($student->status==6)
                                    <td class="success"> Successfully Submitted</td>
                                @endif
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @endif

            @if(!empty ( $payment ))
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title pull-left">Student Payment Details<h4>
                </div>
                <div class="panel-body">
                    <div class="tabtable-responsive">
                        <table class="table table-bordered text-center">
                            <thead>
                            <tr>
                                <th>Transaction Type</th>
                                <th>Ammount</th>
                                <th>Transaction Status</th>
                                <th>Invoice ID</th>
                                <th>Transaction ID</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ strtoupper($payment->t_type) }}</td>
                                <td>{{ $payment->t_amt}}</td>
                                <td><b>{{ $payment->t_status}}</b></td>
                                <td>{{ $payment->id}}</td>
                                <td>{{ $payment->t_id}}</td>
                                <td>{{ $payment->updated_at}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @endif

            @if(!$history->isEmpty())
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title pull-left">Transcation History<h4>
                </div>
                <div class="panel-body">
                    <div class="tabtable-responsive">
                        <table class="table table-bordered text-center">
                            <thead>
                            <tr>
                                <th>Transaction Type</th>
                                <th>Invoice ID</th>
                                <th>Transaction ID</th>
                                <th>Ammount</th>
                                <th>Status</th>
                                <th>Date</th>
                                <th>Auth Status</th>
                                <th>Auth ERROR</th>
                                <th>Bank ID</th>
                                <th>Bank Ref. No.</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($history as $h)
                            <tr>
                                <td>{{ $h['t_type']}}</td>
                                <td>{{ $h->invoice_no}}</td>
                                <td>{{ $h->t_id}}</td>
                                <td>{{ $h->t_amt}}</td>
                                <td><b>{{ $h->t_status}}</b></td>
                                <td>{{ $h->updated_at}}</td>
                                <td>{{ $h->auth_status}}</td>
                                <td>{{ $h->error_descrip}}</td>
                                <td>{{ $h->bank_id}}</td>
                                <td>{{ $h->bank_ref_no}}</td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>

    <div class="row" style="margin-bottom: 20px">
        <div class="col-sm-11">
            <div class="btn-group pull-right">
                <a type="button" class="btn btn-success float-right" href="{{ url('management/home') }}">Back to Dashboard</a>
            </div>
        </div>
    </div>
</div>

@endsection

