
var _URL = window.URL || window.webkitURL;
/*start of photo*/
function displayPhoto(files) {
    var img = new Image(),
    fileSize=Math.round(files.size / 1024);
    img.onload = function () {
            var width=this.width,
                height=this.height,
                imgsrc=this.src;  
      
        doPhoto(fileSize,width,height,imgsrc); //call function
        
        };   
img.src = _URL.createObjectURL(files);
}

// Do what you want in this function
function doPhoto(size,width,height,imgsrc)
{
    $('#preview_photo').empty();
    if(size>=150 || size<=10)
    {
        alert('Photo Size must be between 10-150Kb');
        $('#photo').val("");
    }
    else
    {
        $('#preview_photo').html('<img height="150px" width="150px" src="'+imgsrc+'">'); 
    }
  
    
}

$("#photo").change(function () {
    var photo = this.files[0];

    displayPhoto(photo);


});
/*end of photo*/
/*start of signature*/
function displaySignature(files) {
    var img = new Image(),
        fileSize=Math.round(files.size / 1024);
    
    img.onload = function () {
            var width=this.width,
                height=this.height,
                imgsrc=this.src;  
      
        doSignature(fileSize,width,height,imgsrc); //call function
        
        };   
img.src = _URL.createObjectURL(files);
}

// Do what you want in this function
function doSignature(size,width,height,imgsrc)
{
    $('#preview_signature').empty();
    if(size>=70 || size<=5)
    {
        alert('Signature Size must be between 5-70Kb');
        $('#signature').val("");
    }
    else
    {
         $('#preview_signature').html('<img height="50px" width="150px" src="'+imgsrc+'">'); 
    }
  
    
}

$("#signature").change(function () {
    var photo = this.files[0];

    displaySignature(photo);


});
/*end of signature*/
/*start of birth_certificate*/
function displaybirthcertificate(files) {
    var img = new Image(),
        fileSize=Math.round(files.size / 1024);
    
    img.onload = function () {
            var width=this.width,
                height=this.height,
                imgsrc=this.src;  
      
        dobirthcertificate(fileSize,width,height,imgsrc); //call function
        
        };   
img.src = _URL.createObjectURL(files);
}

// Do what you want in this function
function dobirthcertificate(size,width,height,imgsrc)
{
    $('#preview_birth_certificate').empty();
        
    if(size>300)
    {
        alert('Birth Certificate Size must be less than 300kb');
        $('#birth_certificate').val("");
    }
    else
    {
         $('#preview_birth_certificate').html('<img height="300px" width="200px" src="'+imgsrc+'">'); 
    }
  
    
}

$("#birth_certificate").change(function () {
    var photo = this.files[0];

    displaybirthcertificate(photo);


});
/*end of birth_certificate*/
/*start of marksheet*/
function displayMarksheet(files) {
    var img = new Image(),
        fileSize=Math.round(files.size / 1024);
    
    img.onload = function () {
            var width=this.width,
                height=this.height,
                imgsrc=this.src;  
      
        doMarksheet(fileSize,width,height,imgsrc); //call function
        
        };   
img.src = _URL.createObjectURL(files);
}

// Do what you want in this function
function doMarksheet(size,width,height,imgsrc)
{
    $('#preview_marksheet').empty();
        
    if(size>300)
    {
        alert('Marksheet Size must be less than 300kb');
        $('#marksheet').val("");
    }
    else
    {
         $('#preview_marksheet').html('<img height="300px" width="200px" src="'+imgsrc+'">'); 
    }
  
    
}

$("#marksheet").change(function () {
    var photo = this.files[0];

    displayMarksheet(photo);


});
/*end of marksheet*/

/*start of marksheet Back*/
function displayMarksheetback(files) {
    var img = new Image(),
        fileSize=Math.round(files.size / 1024);
    
    img.onload = function () {
            var width=this.width,
                height=this.height,
                imgsrc=this.src;  
      
        doMarksheetback(fileSize,width,height,imgsrc); //call function
        
        };   
img.src = _URL.createObjectURL(files);
}

// Do what you want in this function
function doMarksheetback(size,width,height,imgsrc)
{
    $('#preview_marksheet_back').empty();
        
    if(size>300)
    {
        alert('Marksheet Size must be less than 300kb');
        $('#marksheet_back').val("");
    }
    else
    {
         $('#preview_marksheet_back').html('<img height="300px" width="200px" src="'+imgsrc+'">'); 
    }
  
    
}

$("#marksheet_back").change(function () {
    var photo = this.files[0];

    displayMarksheetback(photo);


});
/*end of marksheet back*/

/*start of class10 marksheet font*/
function displaytenMarksheetfont(files) {
    var img = new Image(),
        fileSize=Math.round(files.size / 1024);
    
    img.onload = function () {
            var width=this.width,
                height=this.height,
                imgsrc=this.src;  
      
        dotenMarksheetfont(fileSize,width,height,imgsrc); //call function
        
        };   
img.src = _URL.createObjectURL(files);
}

// Do what you want in this function
function dotenMarksheetfont(size,width,height,imgsrc)
{
    $('#preview_marksheet_ten_font').empty();
        
    if(size>300)
    {
        alert('Marksheet Size must be less than 300kb');
        $('#marksheetclasstenfont').val("");
    }
    else
    {
         $('#preview_marksheet_ten_font').html('<img height="300px" width="200px" src="'+imgsrc+'">'); 
    }
  
    
}

$("#marksheetclasstenfont").change(function () {
    var photo = this.files[0];

    displaytenMarksheetfont(photo);


});
/*end of class 10 marksheet font*/




/*start of class10 marksheet back*/
function displaytenMarksheetback(files) {
    var img = new Image(),
        fileSize=Math.round(files.size / 1024);
    
    img.onload = function () {
            var width=this.width,
                height=this.height,
                imgsrc=this.src;  
      
        dotenMarksheetback(fileSize,width,height,imgsrc); //call function
        
        };   
img.src = _URL.createObjectURL(files);
}

// Do what you want in this function
function dotenMarksheetback(size,width,height,imgsrc)
{
    $('#preview_marksheet_ten_back').empty();
        
    if(size>300)
    {
        alert('Marksheet Size must be less than 300kb');
        $('#marksheetclasstenback').val("");
    }
    else
    {
         $('#preview_marksheet_ten_back').html('<img height="300px" width="200px" src="'+imgsrc+'">'); 
    }
  
    
}

$("#marksheetclasstenback").change(function () {
    var photo = this.files[0];

    displaytenMarksheetback(photo);


});
/*end of class 10 marksheet back*/



/*start of Aadhaar*/
function displayaadhaar(files) {
    var img = new Image(),
        fileSize=Math.round(files.size / 1024);
    
    img.onload = function () {
            var width=this.width,
                height=this.height,
                imgsrc=this.src;  
      
        doaadhaar(fileSize,width,height,imgsrc); //call function
        
        };   
img.src = _URL.createObjectURL(files);
}

// Do what you want in this function
function doaadhaar(size,width,height,imgsrc)
{
    $('#preview_aadhaar').empty();
        
    if(size>300)
    {
        alert('Aadhaar Card Size must be less than 300kb');
        $('#aadhaar').val("");
    }
    else
    {
         $('#preview_aadhaar').html('<img height="300px" width="200px" src="'+imgsrc+'">'); 
    }
  
    
}

$("#aadhaar").change(function () {
    var photo = this.files[0];

    displayaadhaar(photo);


});
/*end of class 10 marksheet back*/





/*start of photo*/
function displaySport(files) {
    var img = new Image(),
    fileSize=Math.round(files.size / 1024);
    img.onload = function () {
            var width=this.width,
                height=this.height,
                imgsrc=this.src;  
      
        doSport(fileSize,width,height,imgsrc); //call function
        
        };   
img.src = _URL.createObjectURL(files);
}

// Do what you want in this function
function doSport(size,width,height,imgsrc)
{
    $('#preview_certificate').empty();
    if(size>300)
    {
        alert('Certificate Size must be less than 300 KB!');
        $('#certificate').val("");
    }
    else
    {
        $('#preview_certificate').html('<img height="150px" width="150px" src="'+imgsrc+'">'); 
    }  
}

$("#certificate").change(function () {
    var certificate = this.files[0];

    displaySport(certificate);
});
/*end of photo*/