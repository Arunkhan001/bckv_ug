<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class transaction_history extends Model
{
	protected $table = 'transaction_history';
  protected $fillable = [
      'app_no',
      'invoice_no',
      't_amt',
      't_status',
      't_id',
      'branch_sol_id',
    ];

  /**
     * Get the transaction date in 01/Jan/2017 7:18 am format.
     *
     * @param  date  $value[created_at]
     * @return carbon date
     */
    public function getUpdatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-M h:i:s a');
    }
}
