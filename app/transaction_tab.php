<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class transaction_tab extends Model
{
	protected $table = 'transaction_tab';
    protected $fillable = array('app_no','t_type','t_amt','t_status');

     /**
     * Get the Complain Id U infront of 
     *
     * @param  int  $value[application_no]
     * @return U.$value
     */
    public function getApplicationNoAttribute($value){
        
        return 'U'.$value;
    }

    /**
     * Get the transaction date in 01/Jan/2017 7:18 am format.
     *
     * @param  date  $value[created_at]
     * @return carbon date
     */
    public function getUpdatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-M-Y');
    }

    /**
     * Get the transaction date in 01/Jan/2017 7:18 am format.
     *
     * @param  date  $value[updated_at]
     * @return carbon date
     */
    public function getDateAttribute($value)
    {
        return Carbon::parse($value)->format('d-M-Y h:i A');
    }
}
