<?php

namespace App\Http\Controllers\StudentAuth;

use App\Student;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;

use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/student/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('student.guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:students',
            'mobile_number' => 'required|unique:students|numeric|digits:10',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Student
     */
    protected function create(array $data)
    {
        $student =  Student::create([
            'name' => $data['name'],
            'email' => strtolower($data['email']),
            'mobile_number' => $data['mobile_number'],
            'password' => bcrypt($data['password']),
            'password_original' => $data['password'],
        ]);
        $password = $data['password'];
        $email = strtolower($data['email']);
        $message = 'Your Application Details for BCKV UG ADMISSION, Application No. U'. $student->id . ' Email-Id : '.$email.'  Password : '.$password;
        
        $data = array(
            'authkey' => "113601AINMyMWBSkG857400860",
            'mobiles' =>  $data['mobile_number'],
            'message' => $message,
            'sender' => "BCKVUG",
            'route' => "4",
            'country' => '91'
        );
        
        $url = "https://control.msg91.com/api/sendhttp.php";

        $ch = curl_init();

        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data
                //,CURLOPT_FOLLOWLOCATION => true
        ));

        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        //get response
        $output = curl_exec($ch);
        curl_close($ch);
        
        $mail = [
            'appno' =>$student->id,
            'name' => $student->name,
            'email' => $student->email,
            'password' => $password,
        ];
        Mail::send('emails.welcome',$mail, function($message) use ($student){
            $message->from('admission@bckvadmn.in', "BCKV ADMISSION");
            $message->subject("Welcome ".$student->name." to BCKV Online Admission ");
            $message->to($student->email);
        });

        // $to      = 'khanarun44@gmail.com';
        // $subject = 'Welcome to BCKV Online Admission';
        // $message = 'hello';
        // $headers = 'From: admission@bckvadmn.in' . "\r\n" .
        //     'Reply-To: admission@bckvadmn.in' . "\r\n" .
        //     'X-Mailer: PHP/' . phpversion();

        // mail($to, $subject, $message, $headers);

        return $student;
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('student.auth.register');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('student');
    }
}
