<?php

namespace App\Http\Controllers\Student;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Student;
use App\StudentDetail;
use Auth;

class PersonalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('student');
    }

    /**
     * Show the application Personal information update portal.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = StudentDetail::rightjoin('students','student_details.app_no','=','students.id')
                ->where('students.id',Auth::user()->id)
                ->first();
        //return $user;
        if($user->status>=4 && Auth::user()->isRectification()){
            return view('student.personal')->with(compact('user'));
        }else if($user && $user->status > 3) {
            return redirect('student/home');
        }

        if(Auth::user()->isActive())
            return view('student.personal')->with(compact('user'));
        else
            return redirect('student/home');
    }

    /**
     * Save Personal information details.
     *
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        // return $request->all();
        $category = $request->category;
        $sport_quota = $request->sport_quota;
        // $validator = \Validator::make($request->all(),[
        $this->validate($request, [
            'name'=>'required|regex:/^[a-zA-Z ]+$/|min:3|max:90',
            'father_name'=>'nullable|regex:/^[a-zA-Z ]+$/|max:90',
            'mother_name'=>'required|regex:/^[a-zA-Z ]+$/|min:3|max:90',
            'guardian_mobile_number'=>'required|numeric|digits:10',
            'guardian_email'=>'nullable|email|max:255',
            'gender'=>'required|in:Male,Female,Other',
            'category'=>'required|in:General,SC,ST,OBC-A,OBC-B,Physically Challange,Sport',
            'certificate_number'=>($category!='General' && $category!='Sport')?'required':'',
            'issuing_date'=>($category!='General'  && $category!='Sport' )?'required|date|date_format:Y-m-d|before:2020-09-04':'nullable',
            'sport_quota'=>'required|in:Yes,No',
            'sport_certificate_number'=>($sport_quota=='Yes')?'required':'',
            'sport_issuing_date'=>($sport_quota=='Yes')?'required|date|date_format:Y-m-d|before:2020-09-04':'nullable',

            'domicile'=>'required|in:west bengal,other',
            'nationality'=>'required|min:3|max:20',
            // 'religion'=>'required|in:Hinduism,Muslim,Christian,Buddhism,other',
            'religion'=>'nullable|min:3',
            'blood_group'=>'nullable|in:A+,A-,B+,B-,AB+,AB-,O+,O-',
            'residential_area_type' =>'required|in:Rural,Urban',
            'correspondence_street_no'=>'nullable|string|min:3|max:191',
            'correspondence_village'=>'required|string|min:3|max:191',
            'correspondence_area_type'=>'required|string|min:3|max:191',
            'correspondence_police_station'=>'required|string|min:3|max:191',
            'correspondence_post_office'=>'required|string|min:3|max:191',
            'correspondence_pincode'=>'required|string|min:2|max:10',
            'correspondence_state' => 'required|string|min:3|max:191',
            'correspondence_district' => 'required|string|min:3|max:191',

            'street_no'=>'nullable|string|min:3|max:191',
            'village'=>'required|string|min:3|max:191',
            'area_type'=>'required|string|min:3|max:191',
            'police_station'=>'required|string|min:3|max:191',
            'post_office'=>'required|string|min:3|max:191',
            'pincode'=>'required|string|min:2|max:10',
            'state' => 'required|string|min:3|max:191',
            'district' => 'required|string|min:3|max:191',
            
            'date_of_birth'=>'required|date|date_format:Y-m-d|before:2004-09-01',
            'guardian_name'=>'required|regex:/^[a-zA-Z ]+$/|min:3|max:90',
            'guardian_relationship'=>'required|regex:/^[a-zA-Z ]+$/|min:2|max:30',

            // 'aadhaar' => 'required',
            'student_aadhaar_number' =>(request()->student_aadhaar_number!="")?'numeric|digits:12':'',
            // 'guardian_aadhaar_number' => (request()->aadhaar=="Guardian Aadhaar")?'required|numeric|digits:12':'',
            // 'guardian_aadhaar_name' => (request()->aadhaar=="Guardian Aadhaar")?'required|regex:/^[a-zA-Z ]+$/|min:3|max:90':'',
            // 'guardian_relation' => (request()->aadhaar=="Guardian Aadhaar")?'required|regex:/^[a-zA-Z ]+$/|min:3|max:90':'',
        ]);
/*
 *      return response()->json([
            "module_name" => "error",
            "module_message" => $validator->errors(),
        ]);*/

        // DB::beginTransaction();
        // try {

            StudentDetail::updateOrCreate(['app_no' => Auth::user()->id ], $request->all());
            Student::where('id', Auth::user()->id )
                    ->update(['name'=>$request->name]);

            // DB::commit();

            return  redirect('student/academic')->with('alert', 'Your Personal Details has been successfully saved!');

            DB::table('student_details')->first();
            return User::get();

    //    } catch(\Exception $e){
            // DB::rollback();
            // return redirect()->back($e);
        // } 
    }
}
