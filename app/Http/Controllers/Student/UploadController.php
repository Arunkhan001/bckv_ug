<?php

namespace App\Http\Controllers\Student;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\StudentDetail;
use Auth;
use DB;

class UploadController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('student');
    }

    /**
     * Show the application image update portal.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = StudentDetail::where('app_no', Auth::user()->id)->first();
        //return $data;
        $img_flag = 0;
        if(empty($data)) {
            return redirect('student/home');
        }
        if(($data && $data->status > 1 && $data->status < 4 && Auth::user()->isActive()) ||( $data->status >= 4 && Auth::user()->isRectification() ))
        {
            $img_flag = $data->img_flag;
            $app_no =  Auth::user()->id;
            /*return $data;*/
            return view('student.upload')->with(compact('img_flag', 'app_no', 'data'));
        }
        return redirect('student/home');
        
    }

    /**
     * Upload Image.
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadimage(Request $request)
    {
        $data = StudentDetail::where('app_no', Auth::user()->id)->first();
        $category = $data->category;
        $sport_quota = $data->sport_quota;
        $student_aadhaar_number = $data->student_aadhaar_number;
        /*return $category;*/
        $this->validate($request, [
             'photo' => 'required|image|mimes:jpeg,png,jpg|max:150|min:10',
             'signature' => 'required|image|mimes:jpeg,png,jpg|max:70|min:5',
             'birth_certificate' => 'required|image|mimes:jpeg,png,jpg|max:300',
             'marksheet' => 'required|image|mimes:jpeg,png,jpg|max:300',
             'marksheet_back' => 'required|image|mimes:jpeg,png,jpg|max:300',
             'marksheetclasstenfont' => 'required|image|mimes:jpeg,png,jpg|max:300',
             'marksheetclasstenback' => 'required|image|mimes:jpeg,png,jpg|max:300',
             'aadhaar' => ($student_aadhaar_number!=NULL)?'required|image|mimes:jpeg,png,jpg|max:300':'',
             'certificate' => ($category!="General")?'required|image|mimes:jpeg,png,jpg|max:300':'',
             'sport_certificate' => ($sport_quota=="Yes")?'required|image|mimes:jpeg,png,jpg|max:300':'',
        ]);
        DB::beginTransaction();
        try 
        {
            $app_no =  Auth::user()->id;

            $request->file('photo')->storeAs($app_no,'photo.jpg');
            $request->file('signature')->storeAs($app_no,'signature.jpg');
            $request->file('birth_certificate')->storeAs($app_no, 'birth_certificate.jpg');
            $request->file('marksheet')->storeAs($app_no, 'marksheet.jpg');
            $request->file('marksheet_back')->storeAs($app_no, 'marksheet_back.jpg');
            $request->file('marksheetclasstenfont')->storeAs($app_no, '10th_marksheet_font.jpg');
            $request->file('marksheetclasstenback')->storeAs($app_no, '10th_marksheet_back.jpg');
            if ($student_aadhaar_number!=NULL) {
                $request->file('aadhaar')->storeAs($app_no, 'aadhaar.jpg');
            }
           
            if($category != 'General')
            {
                $request->file('certificate')->storeAs($app_no, 'certificate.jpg');
            }

            if($sport_quota == 'Yes')
            {
                $request->file('sport_certificate')->storeAs($app_no, 'sport_certificate.jpg');
            }
            
             /*check status of previous database entry*/
            $users = StudentDetail::where('app_no', $app_no)->first();
            $status = $users->status;
            if($status > 3)
            {$user_status = $status;}
            else
            {$user_status = 3;}
            /*check status of previous database entry*/
            
            /*Status and image flag update*/
            $update = StudentDetail::where ('app_no',$app_no)
                    ->update(['img_flag'=>'1', 'status'=>$user_status]);
            DB::commit();
            if($update)
            {
                return redirect('student/preview')->with('alert', 'Your All Scan Copy has been successfully uploaded!');
            }  
            
        }
        catch(\Exception $e){
            DB::rollback();
            return redirect('student/preview')->with('alert', 'There is some Internal Problem! Kindly Try again!');
        }
    }
}
