<?php

namespace App\Http\Controllers\Student;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\StudentDetail;
use App\AcademicDetails;
use Auth;

class AcademicController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('student');
    }

    /**
     * Show the application academic information.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = AcademicDetails::where('app_no', Auth::user()->id)->first();
        $data = StudentDetail::where('app_no', Auth::user()->id)->first();
        if(empty($data)) {
            return redirect('student/home');
        }
        if(($data && $data->status < 4 && Auth::user()->isActive()) ||( $data->status >= 4 && Auth::user()->isRectification() )) {
            return view('student.academic')->with(compact('user'));
        }
        return redirect('student/home');
        
    }

    /**
     * Save academic information details.
     *
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $studentdetail = StudentDetail::where('app_no', Auth::user()->id)->first();
        //return $studentdetail;
        $category = $studentdetail->category;
        // $validator = \Validator::make($request->all(),[
        $this->validate($request, [
            'class10fm'=>'required|integer',
            'class10mrk'=>'required|integer|max:'.request()->class10fm,
            // 'classtenpercantage'=>($category=='General' ||$category=='OBC-A' ||$category=='OBC-B')?'min:50|required|numeric|max:100':'min:40|required|numeric|max:100',
            'board_name_classten' =>'required|in:WBBSE,CBSE,ICSE,Other Board',
            'specify_other_board' =>(request()->board_name_classten=="Other Board")?'required':'',
            'passing_year_10' => 'required|numeric|digits:4|before:2019',
            'vocational' => 'required|in:No,Yes',
            // 'syllabus' => 'required_if:vocational,Yes',
            'board_name' =>'required|in:WBCHSE,WBSCVE & T,Other Board',
            'passing_year' => 'required|numeric|digits:4|before:2021',
            'science'=>(request()->vocational=="Yes")?'required':'',
            'paper1'=>(request()->vocational=="Yes")?'required':'',
            'paper2'=>(request()->vocational=="Yes")?'required':'',
            'fm1'=>'required|integer',
            'fm2'=>'required|integer',
            'fm3'=>'required|integer',
            'fm4'=>'required|integer',
            'mrk1'=>'required|integer|max:'.request()->fm1,
            'mrk2'=>'required|integer|max:'.request()->fm2,
            'mrk3'=>'required|integer|max:'.request()->fm3,
            'mrk4'=>'required|integer|max:'.request()->fm4,
            'fm5' =>(request()->vocational=="Yes")?'required|integer':'',
            'mrk5' =>(request()->vocational=="Yes")?'required|integer':'',
            'total_fullmarks'=>'required|integer',
            'total_mark'=>'required|integer|max:'.request()->total_fullmarks,
            'percentage'=>($category=='General' ||$category=='OBC-A' ||$category=='OBC-B')?'min:50|required|numeric|max:100':'min:40|required|numeric|max:100',
            ],
            [
                'science.required' => 'Physics/Chemistry is required',
                'paper1.required' => 'Subject Code 1 is required',
                'paper2.required' => 'Subject Code 2 is required',
            ]
        );
        /*return response()->json([
            "module_name" => "error",
            "module_message" => $validator->errors(),
        ]);*/

        if($studentdetail->sport_quota == "Yes"){
            if($request->vocational == "Yes" || $request->board_name == "Other Board" || $request->board_name == "WBSCVE & T"){
                return redirect()->back()->with('error', 'Error : Sport Quota is only for WBCHSE Board! Not for Vocational or Other Board!');
            }
        }

        
        
        DB::beginTransaction();
        try {
            $id = Auth::user()->id;


            // $classten_full_marks = $request->class10fm;
            // $classten_obtain_marks = $request->class10mrk;

            // $classten_obtain_marks_percentage = (($classten_obtain_marks * 100)/$classten_full_marks);


            // $arr=explode('.', $classten_obtain_marks_percentage);
           
            // list($a, $b) = $arr;
            
            // $fir=$a;
            // $sec = substr($b,0,2);
            // $classten_obtain_marks_percentage=$fir.'.'.$sec;
            $classten_obtain_marks_percentage = $request->classtenpercantage;


            $classten_weightage = (($classten_obtain_marks_percentage * 20)/100);
            $classten_weightage = number_format($classten_weightage,3);


            $classtwelve_weightage = (($request->percentage * 80)/100);

            $classtwelve_weightage = number_format($classtwelve_weightage,3);





            
            $arr1=explode('.', $classten_weightage);
            //print_r($arr);
            /*$firsr=$arr[0];
            echo $firsr;
            $sec=$arr[1];*/
            list($a, $b) = $arr1;
            //echo $a;
            $fir1=$a;
            $sec1 = substr($b,0,2);
            $classten_weightage_final=$fir1.'.'.$sec1;
           


            //$str = "123.23444";
            $arr2=explode('.', $classtwelve_weightage);
            //print_r($arr);
            /*$firsr=$arr[0];
            echo $firsr;
            $sec=$arr[1];*/
            list($a, $b) = $arr2;
            //echo $a;
            $fir2=$a;
            $sec2 = substr($b,0,2);
            $classtwelve_weightage_final=$fir2.'.'.$sec2;
            //echo $final;

            $mark_index = ($classten_weightage_final + $classtwelve_weightage_final);

            AcademicDetails::updateOrCreate(['app_no' => $id], 
                [
                    'class10fm' => $request->class10fm,
                    'class10mrk'    => $request->class10mrk,
                    'board_name_classten'    => $request->board_name_classten,
                    'specify_other_board'    => $request->specify_other_board,
                    'passing_year_10'    => $request->passing_year_10,
                    'vocational'   => $request->vocational,
                    'syllabus'       => $request->syllabus,
                    'board_name'   => $request->board_name,
                    'passing_year'    => $request->passing_year,
                    'science'    => $request->science,
                    'paper1'    => $request->paper1,
                    'paper2'    => $request->paper2,
                    'fm1'    => $request->fm1,
                    'fm2'    => $request->fm2,
                    'fm3'    => $request->fm3,
                    'fm4'    => $request->fm4,
                    'fm5'    => $request->fm5,
                    'mrk1'    => $request->mrk1,
                    'mrk2'    => $request->mrk2,
                    'mrk3'    => $request->mrk3,
                    'mrk4'    => $request->mrk4,
                    'mrk5'    => $request->mrk5,
                    'total_fullmarks'    => $request->total_fullmarks,
                    'total_mark'    => $request->total_mark,
                    'percentage'    => $request->percentage,
                    'classten_percentage'    => $classten_obtain_marks_percentage,
                    'classten_weightage_final'    => $classten_weightage_final,
                    'classtwelve_weightage_final'    => $classtwelve_weightage_final,
                    'mark_index'    => $mark_index,
                ]
             );

            $data = StudentDetail::select('status')
                                    ->where('app_no', $id)->first();
            if($data->status < 2 ) {
                StudentDetail::where('app_no', $id)
                        ->update(['status' => '2']);
            }

            DB::commit();

            return  redirect('student/upload')->with('alert', 'Your Academic Details has been successfully saved!');
        }catch(\Exception $e){
            DB::rollback();
            return redirect()->back()->with($e);
        }

    }




   
}
