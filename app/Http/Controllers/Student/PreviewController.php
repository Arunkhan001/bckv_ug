<?php

namespace App\Http\Controllers\Student;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\StudentDetail;
use App\AcademicDetails;
use App\transaction_tab;
use Auth;
use Mail;

class PreviewController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('student');
    }

    /**
     * Show the application Preview form portal before final submit.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = StudentDetail::join('students','student_details.app_no','=','students.id')
                ->where('students.id',Auth::user()->id)
                ->first();
        if(empty($user)) {
            return redirect('student/home');
        }
        $academic = AcademicDetails::where('app_no', Auth::user()->id)->first();
        if($user && $user->status == 3 && Auth::user()->isActive()){
        	return view('student.preview')->with(compact('user','academic'));
            
        }
        return redirect('student/home');
    }
    /**
     * Show the application Print page.
     *
     * @return \Illuminate\Http\Response
     */
    public function confirm(Request $request)
    {
        $app_no = Auth::user()->id;
        $user = StudentDetail::join('students','student_details.app_no','=','students.id')
                ->where('students.id',$app_no)
                ->first();
        //return $user;
        // if($user && $user->category == "Physically Challange"){
        //     StudentDetail::where('app_no',$app_no)->update(['status' => 6]);
        //     return redirect('student/home');
        // }

        if($user && $user->status == 3) {
            StudentDetail::where('app_no',$app_no)->update(['status' => 4]);
            $charge = StudentDetail::join('app_charges', 'student_details.category', '=', 'app_charges.category')
                        ->where('app_no',$app_no)
                        ->first();
            $mail = [
                'appno' =>$user->id,
                'name' => $user->name,
                'ammount' =>$charge->amount,
                'in_word' =>$charge->in_word
            ];
            Mail::send('emails.payment',$mail, function($message) use ($user){
                $message->from('admission@bckvadmn.in', "BCKV Admission");
                $message->subject("Application Submitted Successfully. ".$user->id);
                $message->to($user->email);
            });
            return redirect('student/payment')->with('alert', 'You have successfully submitted All required Information. Kindly Make Payment to complete your Application!');
        }

        return redirect('student/home');
    }

    /**
     * Show the application Print page.
     *
     * @return \Illuminate\Http\Response
     */
    public function print()
    {
        $user = StudentDetail::join('students','student_details.app_no','=','students.id')
                ->join('academic_details','student_details.app_no','=','academic_details.app_no')
                ->select('*','student_details.app_no as application_no')
                ->where('students.id',Auth::user()->id)
                /*->whereIn('students.id',[1,2,3,4,5])*/
                ->get();
        $status = transaction_tab::where('app_no',Auth::user()->id)->first();
        if($user[0] && $user[0]->status > 5){
           return view('student.print')->with(compact('user'));
        }
        return redirect('student/home');
    }

    /**
     * Show the application Preview only form portal before final submit.
     *
     * @return \Illuminate\Http\Response
     */
    public function previewOnly()
    {
        $user = StudentDetail::join('students','student_details.app_no','=','students.id')
                ->where('students.id',Auth::user()->id)
                ->first();
        $academic = AcademicDetails::where('app_no', Auth::user()->id)->first();
        if($user && $user->status > 3 && $user->status <= 6){
            return view('student.previewonly')->with(compact('user','academic'));
            
        }
        return redirect('student/home');
    }
}
