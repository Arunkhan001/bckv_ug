<?php

namespace App\Http\Controllers\Student;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\StudentDetail;
use App\transaction_tab;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('student');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status = StudentDetail::where('app_no',\Auth::user()->id)->value('status');
        $category = StudentDetail::where('app_no',\Auth::user()->id)->value('category');
        $transaction_tab = transaction_tab::where('app_no',\Auth::user()->id)->first();
        //return $status;
        return view('student.home')->with(compact('status','transaction_tab','category'));
    }
}
