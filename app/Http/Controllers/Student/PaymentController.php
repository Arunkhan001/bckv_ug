<?php

namespace App\Http\Controllers\Student;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\StudentDetail;
use App\transaction_tab;
use App\transaction_history;
use App\BankAccountDetail;
use Auth;
use URL;
use DB;
use Mail;

class PaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('student');
    }
    
    public function index() {
        $app_no = Auth::user()->id;
        $user = StudentDetail::join('app_charges', 'student_details.category', '=', 'app_charges.category')
            ->where('app_no',$app_no)
            ->first();
        if(empty($user)) {
            return redirect('student/home');
        }
        if($user && $user->status > 3 && $user->status < 6 && Auth::user()->isPayment() )
        {
            return view('student/payment')->with(compact('user'));
        }
        return redirect('student/home');
    }

    public function challan() {
        $app_no = Auth::user()->id;
        $user = StudentDetail::join('app_charges', 'student_details.category', '=', 'app_charges.category')
            ->join('students','student_details.app_no','=','students.id')
            ->select('*','student_details.app_no as application_no')
            ->where('app_no',$app_no)
            ->first();
        $invoice = transaction_tab::join('students','transaction_tab.app_no','=','students.id')
            ->select('*','transaction_tab.id as tid')
            ->where('app_no',$app_no)
            ->first();
        $bank_details = BankAccountDetail::first();

        if($user && $user->status == 5 && Auth::user()->isPayment()) {
            if($invoice->t_type=='Online'){
                transaction_tab::where('app_no', $app_no)->update(['t_type'=>'Offline']);
            }
            return view('student.challan')->with(compact('user', 'bank_details', 'invoice'));
        }
        return redirect('student/home');
    }

    public function payment(Request $request) {
        $this->validate($request, [
            'payment_mode' =>'required|in:Online'
        ]);
        $app_no = Auth::user()->id;
        $payment_mode = $request->input('payment_mode');
        $student = StudentDetail::join('app_charges', 'student_details.category', '=', 'app_charges.category')
                   ->where('student_details.app_no',$app_no)
                   ->first();

        $t_amt = $student->amount;
        $t_category = $student->category;

        transaction_tab::updateOrCreate(
            ['app_no'=>$app_no],[
            't_type' => $payment_mode,
            't_amt' => $t_amt,
            't_status' => 'PENDING',
        ]);

        StudentDetail::where('app_no', $app_no) ->update(['status' => 5]);

        if($payment_mode == 'Online' && Auth::user()->isActive() ) {
            $transaction = transaction_tab::select('id')->where('app_no', $app_no)->first();
            $invoice_id = $transaction->id;
            $ru = URL::to('student/ksDfsHbfjOdbSf'); 
            $onPaymentData = "BCKV|" . $app_no . "|NA|" . $t_amt . "|NA|NA|NA|INR|NA|R|bckv|NA|NA|F|U" . $app_no . "|" . $invoice_id . "|ONLINE|".$t_category."|NA|NA|NA|" . $ru;
            $onPaymentData = $onPaymentData."|".strtoupper(hash_hmac('sha256', $onPaymentData, 'qYT0fMt4vwAe', false));
            //return $onPaymentData;
            return view('student.paycaller')->with('msg',$onPaymentData);
        }
        // elseif($payment_mode == 'Offline') {
        //     return redirect('student/challan');
        // }
        return redirect('student/home');
    }

    public function returnurl(Request $request) {
        $msg = $request->input('msg');
        $msgArray = explode("|", $msg);

        $checksum_flag = false;
        $t_flag = false; //transaction flag
        $i = strripos($msg, "|");
        $receivedChecksum = substr($msg, $i + 1);
        $main_msg = substr($msg, 0, strlen($msg) - strlen($receivedChecksum) - 1);

        $checksum = strtoupper(hash_hmac('sha256', $main_msg, 'qYT0fMt4vwAe', false));
        // return $checksum . " \n" . $receivedChecksum;
        if ($receivedChecksum == $checksum) {
            $checksum_flag = true;
            if ($msgArray[14] == "0300") {
                $t_flag = true;
            } else {
                $t_flag = false;
            }
        } else {
            return redirect('student/payment')->with('error', 'Error Code : '.$msgArray[14].' | '.$msgArray[24]);
        }
        if ($checksum_flag) {
            // $app_no = Auth::user()->id;
            // $transaction = transaction_tab::where('app_no', $app_no)->first();

            // $invoice_id = $transaction->id;
            // $t_mode = $transaction->t_type;
            $app_no = $msgArray[1];
            $t_id = $msgArray[2];
            $bank_ref_no = $msgArray[3];
            $t_amt = $msgArray[4];
            $bank_id = $msgArray[5];
            $bank_marchant_id = $msgArray[6];
            $item_code = $msgArray[9]; // debit/credit/net banking indentification no
            $auth_status = $msgArray[14]; //0300 for success
            $t_status = ($t_flag == true) ? 'SUCCESS' : 'FAILED';
            $invoice_id = $msgArray[17];
            $t_mode = $msgArray[18];
            $error_desc = $msgArray[24];

            transaction_history::insert(
                ['app_no' => $app_no,
                 'invoice_no' => $invoice_id,
                 't_id' => $t_id,
                 't_type' => $t_mode,
                 't_amt' => $t_amt,
                 't_status' => $t_status,
                 'auth_status' => $auth_status,
                 'bank_ref_no' => $bank_ref_no,
                 'bank_id' => $bank_id,
                 'bank_marchant_id' => $bank_marchant_id,
                 'error_descrip' => $error_desc,
                 'item_code' => $item_code,
                 'tot_msg' => $msg,
                ]
            );

            if($t_flag) {
                $t_date_time = $msgArray[13];
                $t_date = explode(" ", $t_date_time)[0];
                $t_time = explode(" ", $t_date_time)[1];
                transaction_tab::where('app_no', $app_no)
                ->update([
                    't_id' => $t_id,
                    'received_amt' => $t_amt,
                    't_status' => $t_status,
                    't_type' => $t_mode
                ]);

                StudentDetail::where('app_no', $app_no)
                ->update([
                    'status' => 6,
                ]);

                $user = StudentDetail::join('students','student_details.app_no','=','students.id')
                        ->where('students.id',$app_no)
                        ->first();
                
                $mail = [
                    'appno' =>$user->id,
                    'name' => $user->name,
                    'ammount' =>$t_amt,
                    'transaction_id' =>$t_id,
                    'invoice_id' =>$invoice_id,
                    't_status' => $t_status,
                    'mode'  =>"ONLINE",
                    't_date' => $t_date
                ];
                Mail::send('emails.confirm',$mail, function($message) use ($user){
                    $message->from('admission@bckvadmn.in', "BCKV ADMISSION");
                    $message->subject("Payment Confirmed Successfully.");
                    $message->to($user->email);
                });
                return redirect('student/transaction');
            }
            return redirect('student/payment')->with('error', 'Error Code - '.$msgArray[14].' | '.$msgArray[24]);
        } else  {
            return redirect('student/payment')->with('error', 'Error Code - '.$msgArray[14].' | '.$msgArray[24]);
        }
    }


    public function returnurlserver(Request $request) {
        $msg = $request->input('msg');
        $msgArray = explode("|", $msg);

        $checksum_flag = false;
        $t_flag = false; //transaction flag
        $i = strripos($msg, "|");
        $receivedChecksum = substr($msg, $i + 1);
        $main_msg = substr($msg, 0, strlen($msg) - strlen($receivedChecksum) - 1);

        $checksum = strtoupper(hash_hmac('sha256', $main_msg, 'qYT0fMt4vwAe', false));
        // return $checksum . " \n" . $receivedChecksum;
        if ($receivedChecksum == $checksum) {
            $checksum_flag = true;
            if ($msgArray[14] == "0300") {
                $t_flag = true;
            } else {
                $t_flag = false;
            }
        } 
        if ($checksum_flag) {
            // $app_no = Auth::user()->id;
            // $transaction = transaction_tab::where('app_no', $app_no)->first();

            // $invoice_id = $transaction->id;
            // $t_mode = $transaction->t_type;
            $app_no = $msgArray[1];
            $t_id = $msgArray[2];
            $bank_ref_no = $msgArray[3];
            $t_amt = $msgArray[4];
            $bank_id = $msgArray[5];
            $bank_marchant_id = $msgArray[6];
            $item_code = $msgArray[9]; // debit/credit/net banking indentification no
            $auth_status = $msgArray[14]; //0300 for success
            $t_status = ($t_flag == true) ? 'SUCCESS' : 'FAILED';
            $invoice_id = $msgArray[17];
            $t_mode = $msgArray[18];
            $error_desc = $msgArray[24];

            DB::table('server_return')->insert([
                        'app_no' => $app_no,
                        't_status' => $t_status,
                        'auth_status' => $auth_status,
               
                    ]);

            // transaction_history::insert(
            //     ['app_no' => $app_no,
            //      'invoice_no' => $invoice_id,
            //      't_id' => $t_id,
            //      't_type' => $t_mode,
            //      't_amt' => $t_amt,
            //      't_status' => $t_status,
            //      'auth_status' => $auth_status,
            //      'bank_ref_no' => $bank_ref_no,
            //      'bank_id' => $bank_id,
            //      'bank_marchant_id' => $bank_marchant_id,
            //      'error_descrip' => $error_desc,
            //      'item_code' => $item_code,
            //      'tot_msg' => $msg,
            //     ]
            // );

            
        } 
    }

    public function transaction()
    {
        $app_no = Auth::user()->id;
        
        $invoice = transaction_tab::join('students','transaction_tab.app_no','=','students.id')
                   ->join('student_details','transaction_tab.app_no','student_details.app_no')
                   ->select('students.*','transaction_tab.*','student_details.category','transaction_tab.id as invoice','students.id as application_no','transaction_tab.updated_at as date')
                   ->where('transaction_tab.app_no',$app_no)
                   ->first();
        if($invoice && $invoice->t_status == 'SUCCESS') {
            return view('student.transaction')->with(compact('invoice'));
        }
        return redirect('student/home');
    }
}
