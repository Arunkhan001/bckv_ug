<?php

namespace App\Http\Controllers\Management;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\StudentDetail;
use App\transaction_tab;

class MeritlistController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('management');
    }

    public function index()
    {
        return view('management.meritlist');
    }

    public function meritlistWBCHSEOTHERSs()
    {
        return view('management.wbchse_combined_list');
    }

    public function generate(Request $request)
    {
        
        $category = $request->input('category');
        $board = $request->input('board');
        $sport = $request->input('sport');
        
        $from_limit = $request->input('from_limit')-1; // because skip is not from_limit
        $to_limit = $request->input('to_limit') - $from_limit; // take the middle data

        if($board != "WBSCVE & T")
        {
            $users = StudentDetail::join('academic_details', 'student_details.app_no', '=', 'academic_details.app_no')
                ->join('students', 'student_details.app_no', '=', 'students.id')
                ->select('students.name','student_details.*','student_details.date_of_birth as date_birth','academic_details.*','student_details.app_no as application_no',DB::raw('academic_details.mrk1 + academic_details.mrk2 + academic_details.mrk3  as pcb') )
                // ->select('*','student_details.app_no as application_no')
                ->when($category == 'All', function($p) use($category) {
                    return $p->where('student_details.category','<>','Physically Challange');
                })
                ->when($category != 'All', function($p) use($category){
                    return $p->where('student_details.category', $category);
                })
                ->when($board != 'All', function($q) use($board){
                    return $q->where('academic_details.board_name', $board);
                })
                ->when($sport != 'All', function($q) use($sport){
                    return $q->where('student_details.sport_quota', $sport);
                })
                ->where('student_details.status', 6)
                ->orderBy('academic_details.mark_index','desc')
                ->orderBy('academic_details.total_mark','desc')
                ->orderBy('pcb','desc')
                ->orderBy('mrk3','desc')
                ->orderBy('mrk2','desc')
                ->orderBy('mrk1','desc')
                ->orderBy('mrk4','desc')
                ->orderBy('academic_details.classten_weightage_final','desc')
                ->orderBy('student_details.date_of_birth','asc')
                ->skip($from_limit)
                ->take($to_limit)
               // ->limit($to_limit)
                ->get();

            //return $users;

            $rank = $request->input('from_limit');//initialize rank
            if($category !='All' && $category !='General') {
            $users_general = StudentDetail::join('academic_details', 'student_details.app_no', '=', 'academic_details.app_no')
               ->join('students', 'student_details.app_no', '=', 'students.id')
                ->when($board != 'All', function($q) use($board){
                    return $q->where('academic_details.board_name', $board);
                })
                ->select('student_details.app_no',DB::raw('academic_details.mrk1 + academic_details.mrk2 + academic_details.mrk3  as pcb'))
                ->where('student_details.status', 6)
				->where('student_details.category','<>','Physically Challange')
                ->orderBy('mark_index','desc')
                ->orderBy('total_mark', 'desc')
                ->orderBy('pcb','desc')
                ->orderBy('mrk3','desc')
                ->orderBy('mrk2','desc')
                ->orderBy('mrk1','desc')
                ->orderBy('mrk4','desc')
                ->orderBy('classten_weightage_final','desc')
                ->orderBy('student_details.date_of_birth','asc')
                //->skip($rank)
                //->limit($rank + $to_limit)
                ->get();
            }
            $students_id = array();
            foreach($users as $user) {
                /*add new attribute as pcb*/
                $number = ($user->mrk1 + $user->mrk2 + $user->mrk3);
                $user->average = number_format($number, 2, '.', ',');
                $user->percentage = number_format($user->percentage, 2, '.', ',');

                /*add new attribute as DOB*/
                // $dob = date("d/m/Y", strtotime($user->date_of_birth));
                // $user->DOB = $dob;
                $user->rank = $rank++; //for rank
                if($category !='All' && $category !='General') {
                    $user->general_rank = $this->getRanking($user->app_no,$users_general);
                }

                /*for admit and marksheet*/
               // $app_no = $user->app_no;
                $students_id[] = $user->app_no; 
                /*to get App_no from searched Students & use for bulk print*/
            }
        }
        else
        {
            $users = StudentDetail::join('academic_details', 'student_details.app_no', '=', 'academic_details.app_no')
                ->join('students', 'student_details.app_no', '=', 'students.id')
                ->select('students.name','student_details.*','student_details.date_of_birth as date_birth','academic_details.*','student_details.app_no as application_no',DB::raw('academic_details.mrk2 + academic_details.mrk3  as pcb'),DB::raw('academic_details.mrk4 + academic_details.mrk5  as vpaper') )
                // ->select('*','student_details.app_no as application_no')
                ->when($category == 'All', function($p) use($category) {
                    return $p->where('student_details.category','<>','Physically Challange');
                })
                ->when($category != 'All', function($p) use($category){
                    return $p->where('student_details.category', $category);
                })
                ->when($board != 'All', function($q) use($board){
                    return $q->where('academic_details.board_name', $board);
                })
                ->when($sport != 'All', function($q) use($sport){
                    return $q->where('student_details.sport_quota', $sport);
                })
                ->where('student_details.status', 6)
                ->orderBy('academic_details.mark_index','desc')
                ->orderBy('academic_details.total_mark','desc')
                ->orderBy('pcb','desc')
                ->orderBy('mrk2','desc')
                ->orderBy('mrk3','desc')
                ->orderBy('mrk1','desc')
                ->orderBy('vpaper','desc')
                ->orderBy('academic_details.classten_weightage_final','desc')
                ->orderBy('student_details.date_of_birth','asc')
                ->skip($from_limit)
                ->take($to_limit)
               // ->limit($to_limit)
                ->get();

            //return $users;

            $rank = $request->input('from_limit');//initialize rank
            if($category !='All' && $category !='General') {
            $users_general = StudentDetail::join('academic_details', 'student_details.app_no', '=', 'academic_details.app_no')
               ->join('students', 'student_details.app_no', '=', 'students.id')
                ->when($board != 'All', function($q) use($board){
                    return $q->where('academic_details.board_name', $board);
                })
                ->select('student_details.app_no',DB::raw('academic_details.mrk2 + academic_details.mrk3  as pcb'),DB::raw('academic_details.mrk4 + academic_details.mrk5  as vpaper'))
                ->where('student_details.status', 6)
                ->where('student_details.category','<>','Physically Challange')
                ->orderBy('mark_index', 'desc')
                ->orderBy('total_mark', 'desc')
                ->orderBy('pcb','desc')
                ->orderBy('mrk2','desc')
                ->orderBy('mrk3','desc')
                ->orderBy('mrk1','desc')
                ->orderBy('vpaper','desc')
                ->orderBy('classten_weightage_final','desc')
                ->orderBy('date_of_birth','asc')
                //->skip($rank)
                //->limit($rank + $to_limit)
                ->get();
            }
            $students_id = array();
            foreach($users as $user) {
                /*add new attribute as pcb*/
                $number = ($user->mrk2 + $user->mrk3);
                $user->average = number_format($number, 2, '.', ',');

                $number1 = ($user->mrk4 + $user->mrk5);
                $user->average1 = number_format($number1, 2, '.', ',');

                $user->percentage = number_format($user->percentage, 2, '.', ',');

                /*add new attribute as DOB*/
                // $dob = date("d/m/Y", strtotime($user->date_of_birth));
                // $user->DOB = $dob;
                $user->rank = $rank++; //for rank
                if($category !='All' && $category !='General') {
                    $user->general_rank = $this->getRanking($user->app_no,$users_general);
                }

                /*for admit and marksheet*/
               // $app_no = $user->app_no;
                $students_id[] = $user->app_no; 
                /*to get App_no from searched Students & use for bulk print*/
            }
        }
           
        return response()->json([
            "user" => $users,
            "board" => $board,
            "category" => $category,
            "students_id" => $students_id /* student IDs array*/
        ]);
    }

    public function getRanking($id,$users_general) {
       $rank =  1;
       foreach($users_general as $g) {
        if($g->app_no == $id) {
            return $rank;
        }
        $rank ++;
      }
       return $rank;
    }

    public function bulkprint(Request $request)
    {
        // return $request->all();
        $students_id = explode(',',$request->students_id);
        /*return $students_id;*/
        $placeholders = implode(',',array_fill(0, count($students_id), '?')); // string for the query

        $user = StudentDetail::join('students','student_details.app_no','=','students.id')
                ->join('academic_details','student_details.app_no','=','academic_details.app_no')
                ->select('*','student_details.app_no as application_no')
                /*->where('students.id',Auth::user()->id)*/
                ->whereIn('student_details.app_no', $students_id)
                ->orderByRaw("field(student_details.app_no,{$placeholders})", $students_id)
                ->get();
        return view('management.print')->with(compact('user'));
    }

    public function Certificate(Request $request)
    {
        if($request->case == "caste"){

            $case = "caste";
            $students_id = explode(',',$request->students_id);
            foreach ($students_id as $key => $students_id) {
                $notgeneral_count = StudentDetail::where('category','!=', 'General')
                                ->where('app_no', $students_id)
                                ->count();
                if($notgeneral_count != 0){
                    $students_array[] = $students_id;
                }
            }
            //return $students_array;
            return view('management.certificate')->with(compact('students_array','case'));

        }else if($request->case == "sport"){

            $case = "sport";
            $students_id = explode(',',$request->students_id);
            foreach ($students_id as $key => $students_id) {
                $sport_count = StudentDetail::where('sport_quota','=', 'Yes')
                                ->where('app_no', $students_id)
                                ->count();
                if($sport_count != 0){
                    $students_array[] = $students_id;
                }else{
                    $students_array = array();
                }
            }
            //return $students_array;
            return view('management.certificate')->with(compact('students_array','case'));

        }else if($request->case == "marksheet"){

            $case = "marksheet";
            $students_array = explode(',',$request->students_id);
            return view('management.certificate')->with(compact('students_array','case'));

        }else if($request->case == "admitcard"){

            $case = "admitcard";
            $students_array = explode(',',$request->students_id);
            return view('management.certificate')->with(compact('students_array','case'));
        }
        
    }
}