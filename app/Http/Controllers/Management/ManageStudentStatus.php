<?php

namespace App\Http\Controllers\Management;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Student;
use App\StudentDetail;
use App\transaction_tab;
use App\transaction_history;
class ManageStudentStatus extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('management');
    }

    /**
     * Show the application student management.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $student = StudentDetail::rightjoin('students','student_details.app_no','students.id')
                ->select('students.id','name','father_name','mother_name','email','mobile_number','category','date_of_birth','status','students.created_at')
                ->where('students.id', $request->input('app_no'))
                ->orWhere('students.email',$request->input('app_no'))
                ->first();
        if(empty($student)) {
            $id = $request->input('app_no');
        }else {
            $id = $student->id;
        }
        $payment = transaction_tab::where('app_no',$request->input('app_no'))
                ->orWhere('app_no',$id)
                ->first();
        $history = transaction_history::where('app_no',$request->input('app_no'))
                ->orWhere('app_no',$id)
                ->get();

        return view('management.management')->with(compact('student','payment','history'));
    }
}
