<?php

namespace App\Http\Controllers\Management;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Student;
use App\StudentDetail;
use App\AcademicDetails;
use DB;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('management');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $register = Student::count();
        $step1 = StudentDetail::where('status', 1)->count();
        $step2 = StudentDetail::where('status', 2)->count();
        $step3 = StudentDetail::where('status', 3)->count();
        $step4 = StudentDetail::where('status', 4)->count();
       // $step5 = StudentDetail::where('status', 5)->count();
        // $ofline = StudentDetail::join('transaction_tab','student_details.app_no','=','transaction_tab.app_no')
        //         ->where('student_details.status',5)
        //         ->where('transaction_tab.t_type','Offline')
        //         ->count();
		$onlinepayment = StudentDetail::join('transaction_tab','student_details.app_no','=','transaction_tab.app_no')
				->where('student_details.status',6)
                ->where('transaction_tab.t_type','online')
                ->sum('transaction_tab.t_amt');
        $step6 = StudentDetail::where('status', 6)->count();
        return view('management.adminboard')->with(compact('register','step1','step2','step3','step4','step6','onlinepayment'));;
    }


    public function counselling_bulk(){



        return view('management.counselling_bulk');
    }

    public function counselling_bulkprint(Request $request){

         // string for the query

        // $user = StudentDetail::join('students','student_details.app_no','=','students.id')
        //         ->join('academic_details','student_details.app_no','=','academic_details.app_no')
        //         ->join('bulk_form','student_details.app_no','=','bulk_form.app_no')
        //         ->select('*','student_details.app_no as application_no')
        //         ->where('bulk_form.board',$request->board_name)
        //         ->where('bulk_form.category',$request->category)
                
        //         ->get();


        $user = DB::table('bulk_form')
                ->join('students','bulk_form.app_no','=','students.id')
                ->join('academic_details','bulk_form.app_no','=','academic_details.app_no')
                ->join('student_details','student_details.app_no','=','bulk_form.app_no')
                ->select('*','student_details.app_no as application_no')
                ->where('bulk_form.board',$request->board_name)
                ->where('bulk_form.category',$request->category)
                ->orderBy('bulk_form.id')
                ->get();

        return view('management.print')->with(compact('user'));
    }


    public function bulksms()
    {

        set_time_limit(0);
        $users = DB::table('sms')->get();



        //return $users;
          //$mobile = $users[0]->ph_no;

        foreach ($users as $user) {
            
            $mobile = $user->ph_no;
            $message = 'Your payment data has been updated from our end. please check your application:BCKV';

            $data = array(
            'authkey' => "113601AINMyMWBSkG857400860",
            'mobiles' =>  $mobile,
            'message' => $message,
            'sender' => "BCKVUG",
            'route' => "4",
            'country' => '91'
        );
        
        $url = "https://control.msg91.com/api/sendhttp.php";

        $ch = curl_init();

        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data
                //,CURLOPT_FOLLOWLOCATION => true
        ));

        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        //get response
        $output = curl_exec($ch);
        curl_close($ch);
        }

        return redirect()->back() ->with('alert', 'Done!');

     }



      public function calculationcorrection()

    {

        set_time_limit(0);
        $users = AcademicDetails::get();

        $count = 0;

        foreach ($users as $key => $user) {

            $app_no = $user->app_no;
            $board_name = $user->board_name;
            $total_mark = $user->total_mark;
            $total_fullmarks = $user->total_fullmarks;

            $class10mrk = $user->class10mrk;
            $class10fm = $user->class10fm;

            $classten_percentage = (($class10mrk * 100)/$class10fm);
            $classten_percentage = number_format($classten_percentage,3);


            $arr1=explode('.', $classten_percentage);
            //print_r($arr);
            /*$firsr=$arr[0];
            echo $firsr;
            $sec=$arr[1];*/
            list($a, $b) = $arr1;
            //echo $a;
            $fir1=$a;
            $sec1 = substr($b,0,2);

            $classten_percentage_final=$fir1.'.'.$sec1;  //value




            $classten_weightage = (($classten_percentage_final * 20)/100);
            $classten_weightage = number_format($classten_weightage,3); 

            $arr1=explode('.', $classten_weightage);
            //print_r($arr);
            /*$firsr=$arr[0];
            echo $firsr;
            $sec=$arr[1];*/
            list($a, $b) = $arr1;
            //echo $a;
            $fir1=$a;
            $sec1 = substr($b,0,2);

            $classten_weightage_final=$fir1.'.'.$sec1;   //value



            







            // if ( $board_name == "WBCHSE" || $board_name == "Other Board") {


            //     $classtwelve_percentage = (($total_mark * 100)/$total_fullmarks);

            //     } 

            // else {

                $classtwelve_percentage = (($total_mark * 100)/$total_fullmarks);
                
            // }
                $classtwelve_percentage = number_format($classtwelve_percentage,3);

                $arr1=explode('.', $classtwelve_percentage);
                //print_r($arr);
                /*$firsr=$arr[0];
                echo $firsr;
                $sec=$arr[1];*/
                list($a, $b) = $arr1;
                //echo $a;
                $fir1=$a;
                $sec1 = substr($b,0,2);

                $classtwelve_percentage_final=$fir1.'.'.$sec1;  //value

            


                $classtwelve_weightage = (($classtwelve_percentage_final * 80)/100);

                $classtwelve_weightage = number_format($classtwelve_weightage,3);


                $arr2=explode('.', $classtwelve_weightage);
                //print_r($arr);
                /*$firsr=$arr[0];
                echo $firsr;
                $sec=$arr[1];*/
                list($a, $b) = $arr2;
                //echo $a;
                $fir2=$a;
                $sec2 = substr($b,0,2);
                $classtwelve_weightage_final=$fir2.'.'.$sec2;



                $mark_index = ($classten_weightage_final + $classtwelve_weightage_final);




                AcademicDetails::updateOrCreate(['app_no' => $app_no], 
                [
                    
                    
                    'percentage'    => $classtwelve_percentage_final,
                    'classten_percentage'    => $classten_percentage_final,
                    'classten_weightage_final'    => $classten_weightage_final,
                    'classtwelve_weightage_final'    => $classtwelve_weightage_final,
                    'mark_index'    => $mark_index,
                ]
             );

                $student_array[] = $app_no;


            # code...
        }

         return view('management.calculationdone')->with(compact('student_array'));

    }
}
