<?php

namespace App\Http\Controllers\Management;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('management');
    }

    /**
     * Show Images.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($app_no, $name)
    {
    	$path = $app_no.'/'.$name;
    	$content = \Storage::exists($path);

    	if($content){
            return \Response::make(\Storage::disk('local')->get($path), 200,[
                'Content-Type' => 'image/jpg'
            ]);
    		// $url = \Storage::url($path);
    		// return response()->file($url);
    	}
    	return response()->file('images/bckv-logo.png');
    }
}
