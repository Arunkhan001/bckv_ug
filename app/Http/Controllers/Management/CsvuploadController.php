<?php

namespace App\Http\Controllers\Management;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\StudentDetail;
use App\transaction_history;
use App\transaction_tab;

class CsvuploadController extends Controller
{
	/**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('management');
    }

    public function index()
    {
        $error_array = NULL;
        $success_array = NULL;
    	return view('management.csvupload')->with(compact('error_array', 'success_array'));
    }

    public function upload(Request $request)
    {
        $this->validate($request,[

                'csv' => 'required|mimes:csv,txt',
            ]);

    	if($request->file('csv'))
        {
            $file = fopen($request->file('csv'),"r");

            $count = 0;
            $success_array = [];
            $error_array = [];
            while(($line = fgetcsv($file)) !== FALSE)
            {
                    /*$invoice_no = $line[0];
                    $app_no = $line[1];
                    $t_amt = $line[5];
                    $t_id = $line[6];*/
                    $offline_status = StudentDetail::join('transaction_tab','transaction_tab.app_no','student_details.app_no')
                                        ->where('student_details.app_no', '=', $line[1])
                                        ->first();
                    // return $offline_status->status;
                    if($offline_status ){
                        if($offline_status->status == 5 && $offline_status->t_amt == $line[5])
                        {
                            $line[] = $offline_status->status;
                            $line[] = $offline_status->t_amt;
                            $success_array[] = $line;
                        }
                        else
                        {
                            $line[] = $offline_status->status;
                            $line[] = $offline_status->t_amt;
                            $error_array[] = $line;
                        }
                    }else{
                        $line[] = NULL;
                        $line[] = NULL;
                        $error_array[] = $line;/*User not created payment*/
                    }
                }
            /*return $success_array;
            return $error_array;*/
            return view("management/csvupload")->with(compact('error_array', 'success_array'));
        } 
    }

    public function offlinedataupload(Request $request)
    {
        /*return $request->all();*/
        /*return count($request->invoice_no);*/
        try {

            DB::beginTransaction();
            $count = 0;
            for ($i=0; $i < count($request->invoice_no); $i++) {
                transaction_tab::where('app_no', $request->app_no[$i])
                                    ->update([
                                        't_id' => $request->app_no[$i],
                                        't_type' => 'Offline',
                                        'received_amt' => $request->amount[$i],
                                        't_id' => $request->t_id[$i],
                                        't_status' => 'SUCCESS',
                                    ]);

                transaction_history::insert(
                    [

                    'invoice_no'=>  $request->invoice_no[$i],
                    'app_no'=>  $request->app_no[$i],
                    't_amt'=>   $request->amount[$i],
                    't_type'=>'Offline',
                    't_status'=>'SUCCESS',
                    't_id'=> $request->t_id[$i],
                    'bank_ref_no'=> $request->invoice_no[$i],

                    ]);

                StudentDetail::where('app_no', $request->app_no[$i])
                    ->update([

                        'status'=>6

                        ]);
                $count++;
            }
            DB::commit();
            return \Redirect::back()->with('alert', $count.' No of Rows has been Inserted');
        }
        catch(\Exception $e) {

                DB::rollBack();
                /*return $e;*/
                return \Redirect::back()->with('alert', 'CSV file does not matched with required format at row no : '.($count+1) .' FOR : '.substr($e->getMessage(), 0, strpos($e->getMessage(), "for")));
            }


    }
}

        
