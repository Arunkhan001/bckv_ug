<?php

namespace App;

use App\Notifications\StudentResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;
class Student extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'mobile_number','password','password_original',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new StudentResetPassword($token));
    }

    public function isRectification() {
        if(Carbon::now()->between(Carbon::parse(env('RECTIFICATION_START_DATE')),Carbon::parse(env('RECTIFICATION_END_DATE'))) ){
            return true;
        }  
        return false; //need to modify check date
    }

    public function isActive() {
        if(Carbon::now()->between(Carbon::parse(env('ADMISSION_START_DATE')),Carbon::parse(env('ADMISSION_END_DATE'))) || Carbon::parse(env('ADMISSION_END_DATE'))->isToday()){
            return true;
        }           
        return false;
    }
    public function isPayment() {
        if(Carbon::now()->between(Carbon::parse(env('ADMISSION_START_DATE')),Carbon::parse(env('PAYMENT_END_DATE'))) || Carbon::parse(env('PAYMENT_END_DATE'))->isToday()){
            return true;
        }
        return false;
    }
}
