<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankAccountDetail extends Model
{
    protected $table = 'bank_account_details';
    protected $fillable = array('acc_no','acc_type','ifsc_code','bf_name','bank_name');
}
