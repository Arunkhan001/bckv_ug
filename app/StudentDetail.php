<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;

class StudentDetail extends Model
{
    protected  $table = 'student_details';

    protected $fillable = [
            'app_no','father_name','mother_name','guardian_name','guardian_relationship','guardian_mobile_number',
            'guardian_email','gender','category','date_of_birth','domicile','student_aadhaar_number','status', 'religion', 'blood_group', 'nationality','street_no', 'village', 'area_type','correspondence_area_type',
            'police_station', 'post_office','state', 'pincode', 'district', 'correspondence_street_no', 'correspondence_village', 
            'correspondence_police_station', 'correspondence_post_office', 'correspondence_pincode', 'correspondence_district',
            'correspondence_state', 'residential_area_type','certificate_number','issuing_date','sport_certificate_number','sport_issuing_date','sport_quota','bank_account_details','bank_ifsc_code'
            ];

    /**
     * Get the Complain Id U infront of 
     *
     * @param  int  $value[application_no]
     * @return U.$value
     */
    public function getApplicationNoAttribute($value){
        
        return 'U'.$value;
    }

    /**
     * Get the date of birth date in 01/Jan/2017 7:18 am format.
     *
     * @param  date  $value[date_of_brith]
     * @return carbon date
     */
    public function getDateBirthAttribute($value) {
        return Carbon::parse($value)->format('d/m/Y');
    }

    /**
     * Get the created date in 01/Jan/2017 7:18 am format.
     *
     * @param  date  $value[created_at]
     * @return carbon date
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-M h:i a');
    }
	
	public function getNameAttribute($value) {
        return strtoupper($value);
    }

    public function getGuardianNameAttribute($value) {
        return strtoupper($value);
    }
	
	// public function getCategoryAttribute($value) {
 //        return strtoupper($value);
 //    }

}
