<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcademicDetails extends Model
{
    protected  $table = 'academic_details';


    protected $fillable = ['app_no','class10fm','class10mrk','board_name_classten','specify_other_board','passing_year_10','classten_percentage','classten_weightage_final','vocational','syllabus','board_name',
    		'passing_year','science','paper1','paper2','fm1','fm2','fm3','fm4','fm5','mrk1','mrk2',
    		'mrk3','mrk4','mrk5','total_fullmarks','total_mark','percentage','classtwelve_weightage_final','mark_index'];

}
