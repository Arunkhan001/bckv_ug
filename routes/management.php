<?php

/*Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('management')->user();

    //dd($users);

    return view('management.home');
})->name('home');*/

Route::get('/home', 'Management\DashboardController@index');
Route::get('/bulksms', 'Management\DashboardController@bulksms');


Route::get('/counselling_bulk','Management\DashboardController@counselling_bulk');
Route::post('/counselling_bulkprint','Management\DashboardController@counselling_bulkprint');





Route::get('/calculationcorrection', 'Management\DashboardController@calculationcorrection');
Route::get('/meritlist','Management\MeritlistController@index')->name('merit');

Route::get('/printmeritlist','Management\MeritlistController@printmerit');
Route::get('/meritlistWBCHSEOTHERS','Management\MeritlistController@meritlistWBCHSEOTHERSs');
Route::get('/meritlistWBCHSEOTHERScategory','Management\MeritlistController@meritlistWBCHSEOTHERScategory');
Route::get('/meritlistvocational','Management\MeritlistController@meritlistvocational');
Route::get('/meritlistvocationalcategory','Management\MeritlistController@meritlistvocationalcategory');

Route::post('/meritlist', 'Management\MeritlistController@generate')->name('meritlist');
Route::get('/csvupload','Management\CsvuploadController@index');
Route::get('/bulkprint', function(){
	return redirect('management/meritlist');
});
Route::get('/singleprint', function(){return view('management.bulkprint');});
Route::post('/bulkprint','Management\MeritlistController@bulkprint');
Route::post('/csvupload', 'Management\CsvuploadController@upload');
Route::get('/image/{app_no}/{name}', 'Management\ImageController@show');
Route::get('/checkstatus', 'Management\ManageStudentStatus@index');
Route::post('/checkstatus', 'Management\ManageStudentStatus@info');
Route::post('/offlinedata', 'Management\CsvuploadController@offlinedataupload');

Route::post('/certificate','Management\MeritlistController@Certificate');


Route::get('/transaction','Management\DashboardController@transaction');