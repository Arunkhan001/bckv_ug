<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->middleware(['student.guest','management.guest']);
Route::get('UGAdmission', function() {
	return redirect('/');
});
Route::group(['prefix' => 'student'], function () {
      Route::get('/', function () {
          return redirect('/');
      });
      Route::get('/login', 'StudentAuth\LoginController@showLoginForm')->name('login');
      Route::post('/login', 'StudentAuth\LoginController@login');
      Route::post('/logout', 'StudentAuth\LoginController@logout')->name('logout');

      Route::get('/register', 'StudentAuth\RegisterController@showRegistrationForm')->name('register');
      Route::post('/register', 'StudentAuth\RegisterController@register');

      Route::post('/password/email', 'StudentAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
      Route::post('/password/reset', 'StudentAuth\ResetPasswordController@reset')->name('password.email');
      Route::get('/password/reset', 'StudentAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
      Route::get('/password/reset/{token}', 'StudentAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'management'], function () {
       Route::get('/', function () {
          return redirect('/');
      });
      Route::get('/login', 'ManagementAuth\LoginController@showLoginForm');
      Route::post('/login', 'ManagementAuth\LoginController@login');
      Route::post('/logout', 'ManagementAuth\LoginController@logout')->name('logout');

      Route::get('/register', 'ManagementAuth\RegisterController@showRegistrationForm')->name('register');
      Route::post('/register', 'ManagementAuth\RegisterController@register');

      Route::post('/password/email', 'ManagementAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
      Route::post('/password/reset', 'ManagementAuth\ResetPasswordController@reset')->name('password.email');
      Route::get('/password/reset', 'ManagementAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
      Route::get('/password/reset/{token}', 'ManagementAuth\ResetPasswordController@showResetForm');
});
