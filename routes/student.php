<?php

/*Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('student')->user();

    //dd($users);

    return view('student.home');
})->name('home');*/

Route::get('/home', 'Student\DashboardController@index')->name('home');
Route::get('/personal', 'Student\PersonalController@index');
Route::POST('/personal', 'Student\PersonalController@save');
Route::get('/academic', 'Student\AcademicController@index');
Route::POST('/academic','Student\AcademicController@save');
Route::get('/upload', 'Student\UploadController@index');
Route::POST('/upload', 'Student\UploadController@uploadimage');
Route::get('/preview', 'Student\PreviewController@index');
Route::post('/preview', 'Student\PreviewController@confirm');
Route::get('/payment', 'Student\PaymentController@index');
Route::post('/payment', 'Student\PaymentController@payment');
Route::post('/ksDfsHbfjOdbSf','Student\PaymentController@returnurl');
Route::post('/ksDfsHbfjOdbSfghfdt','Student\PaymentController@returnurlserver');
Route::get('/challan','Student\PaymentController@challan');
Route::get('/previewonly', 'Student\PreviewController@previewOnly');
Route::get('/transaction','Student\PaymentController@transaction');
Route::get('/print','Student\PreviewController@print');
Route::get('/image/{app_no}/{name}', 'Student\ImageController@show');